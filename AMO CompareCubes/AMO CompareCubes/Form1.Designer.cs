﻿namespace AMO_CompareCubes
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnRun = new System.Windows.Forms.Button();
            this.tvStatus = new System.Windows.Forms.TreeView();
            this.cmTreeView = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pgDimensjon = new System.Windows.Forms.ProgressBar();
            this.pgMålTall = new System.Windows.Forms.ProgressBar();
            this.lbStatus = new System.Windows.Forms.Label();
            this.cmTreeView.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnRun
            // 
            this.btnRun.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRun.Location = new System.Drawing.Point(613, 390);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(141, 23);
            this.btnRun.TabIndex = 0;
            this.btnRun.Text = "Kjør sammenligning";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // tvStatus
            // 
            this.tvStatus.ContextMenuStrip = this.cmTreeView;
            this.tvStatus.Location = new System.Drawing.Point(3, 12);
            this.tvStatus.Name = "tvStatus";
            this.tvStatus.Size = new System.Drawing.Size(751, 297);
            this.tvStatus.TabIndex = 1;
            this.tvStatus.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tvStatus_MouseUp);
            // 
            // cmTreeView
            // 
            this.cmTreeView.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.cmTreeView.Name = "cmTreeView";
            this.cmTreeView.Size = new System.Drawing.Size(129, 26);
            this.cmTreeView.Text = "Kopier MDX";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(128, 22);
            this.toolStripMenuItem1.Text = "Kopier MDX";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // pgDimensjon
            // 
            this.pgDimensjon.Location = new System.Drawing.Point(3, 315);
            this.pgDimensjon.Name = "pgDimensjon";
            this.pgDimensjon.Size = new System.Drawing.Size(751, 30);
            this.pgDimensjon.TabIndex = 3;
            // 
            // pgMålTall
            // 
            this.pgMålTall.Location = new System.Drawing.Point(3, 351);
            this.pgMålTall.Name = "pgMålTall";
            this.pgMålTall.Size = new System.Drawing.Size(751, 33);
            this.pgMålTall.TabIndex = 4;
            // 
            // lbStatus
            // 
            this.lbStatus.AutoSize = true;
            this.lbStatus.Location = new System.Drawing.Point(0, 387);
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(37, 13);
            this.lbStatus.TabIndex = 5;
            this.lbStatus.Text = "Status";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(757, 413);
            this.Controls.Add(this.lbStatus);
            this.Controls.Add(this.pgMålTall);
            this.Controls.Add(this.pgDimensjon);
            this.Controls.Add(this.tvStatus);
            this.Controls.Add(this.btnRun);
            this.Name = "Form1";
            this.Text = "Form1";
            this.cmTreeView.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.TreeView tvStatus;
        private System.Windows.Forms.ContextMenuStrip cmTreeView;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ProgressBar pgDimensjon;
        private System.Windows.Forms.ProgressBar pgMålTall;
        private System.Windows.Forms.Label lbStatus;
    }
}

