﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.AnalysisServices;
using Microsoft.AnalysisServices.Hosting;
using Microsoft.AnalysisServices.AdomdClient;
using System.Text.RegularExpressions;

namespace AMO_CompareCubes
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int numberOfTests = 0;
        bool errorFound = false;

        public string cubeName = " from RetailData ";
        //public string whereStatement = " where StrToMember(\"[Date].[Year - Month - Date].&[\" + Format(now()-1, \"yyyy-MM-dd\") + \"T00:00:00]\", CONSTRAINED)";

        public string whereStatement = " where [Date].[Year - Month - Date].[Date].&[2012-10-26T00:00:00]";
        
        public string measure = "select [Measures].[Net Amount Ex Tax] on 0, ";

        public string newServerConnection = "Data Source=epsqltest2;Catalog=RetailData_dev";
        public string newCubeName = "RetailData";

        public string oldServerConnection = "Data Source=epsql3;Catalog=RetailData";

        bool debug = true;
        public int jumpvalue = 1;

        int X = 0;
        int Y = 0;

        public bool compareContent(CellSet oldCell, CellSet newCell, out string ErrorText)
        {
            // Create the pos object to contain two ordinals 
            // for the coordinate.
            int numaxes = newCell.Axes.Count;
            object[] pos = new object[numaxes];
            int posval = 350;
            bool isEqual = true;

            ErrorText = "";

            // Traverse through the pages.
            for (int pages = 0; pages <= newCell.Axes.Count; pages++)
            {
                for (int ia = 0; ia <= newCell.Axes[0].Positions.Count - 1; ia++)
                {
                    // If a page exists, write the name of the page.
                    if (numaxes > 2)
                        if (newCell.Axes[2].Positions[pages].Members[0].Caption.ToString() != oldCell.Axes[2].Positions[pages].Members[0].Caption.ToString())
                        {
                            ErrorText = "Error 1... meta matcher ikke. " + newCell.Axes[2].Positions[pages].Members[0].Caption.ToString();
                            isEqual = false;
                            errorFound = true;
                            break;
                        }

                    // Write the name of the columns.
                    if (newCell.Axes[0].Positions[ia].Members[0].Caption.ToString() != newCell.Axes[0].Positions[ia].Members[0].Caption.ToString())
                    {
                        ErrorText = "Error 2... meta matcher ikke. " + newCell.Axes[0].Positions[ia].Members[0].Caption.ToString();
                        isEqual = false;
                        errorFound = true;
                        break;
                    }

                    numberOfTests++;
                }

                // Traverse through the rows.
                for (int j = 0; j <= newCell.Axes[1].Positions.Count - 1; j++)
                {
                    string dimNameOld = newCell.Axes[1].Positions[j].Members[0].Caption.ToString().Replace(" ","");
                    string dimNameNew = oldCell.Axes[1].Positions[j].Members[0].Caption.ToString().Replace(" ","");
                    // Write the dimension member name.
                    if( dimNameNew.ToUpper() != dimNameOld.ToUpper())
                    {
                        ErrorText = "Error 3... dimensjonsnavn matcher ikke. " + newCell.Axes[1].Positions[j].Members[0].Caption.ToString().Replace(" ", "");
                        isEqual = false;
                        errorFound = true;
                        break;
                    }


                    numberOfTests++;

                    // Traverse through the columns.
                    for (int i = 0; i <= newCell.Axes[0].Positions.Count - 1; i++)
                    {
                        try
                        {
                            // Load the cell coordinates to the pos object.
                            pos[0] = System.Convert.ToInt16(newCell.Axes[0].
                                            Positions[i].Ordinal.ToString());
                            pos[1] = System.Convert.ToInt16(newCell.Axes[1].
                                            Positions[j].Ordinal.ToString());

                            // Retrieve the value of a cell.
                            Cell cellNew = newCell.Cells[i];
                            Cell cellOld = oldCell.Cells[i];
                            
                            // Write the FormattedValue property.
                            if (Math.Round(Convert.ToDouble(cellNew.Value),3) != Math.Round(Convert.ToDouble(cellOld.Value),3))
                            {
                                ErrorText = "Error 4... verdier matcher ikke. " + newCell.Axes[1].Positions[j].Members[0].Caption.ToString();
                                isEqual = false;
                                errorFound = true;
                                break;
                            }
                            posval = posval + 25;

                            numberOfTests++;
                        }
                        catch(Exception err) 
                        {
                            ErrorText = "Error 5... manglende verdi. " + newCell.Axes[1].Positions[j].Members[0].Caption.ToString() + "." + err.Message;
                            isEqual = false;
                            errorFound = true;
                            break;
                        }
                    }
                }
            }

            return isEqual;
        }

        public void loopCube(string serverConn)
        {
            string mdx = "";
            int counter = 0;
            int selectCounter = 0;

            string ConnStr = serverConn;
            Server OLAPServer = new Server();
            OLAPServer.Connect(ConnStr);

            Database OLAPDatabase = OLAPServer.Databases[newCubeName];
            TreeNode dimNode;

            foreach (Cube OLAPCubex in OLAPDatabase.Cubes)
            {
                int numberOfMeasures = 0;
                
                foreach (Microsoft.AnalysisServices.Measure m in OLAPCubex.AllMeasures)
                {
                    numberOfMeasures++;
                }

                pgMålTall.Maximum = numberOfMeasures;

                tvStatus.BeginUpdate();
                tvStatus.Nodes.Add(new TreeNode("Testing cube: " + OLAPCubex.Name));

                pgDimensjon.Maximum = OLAPCubex.Dimensions.Count;
                pgDimensjon.Value = 0;

                foreach (CubeDimension OLAPDimension in OLAPCubex.Dimensions)
                {
                    pgDimensjon.Increment(1);

                    dimNode = new TreeNode("Dimensjon " + OLAPDimension.Name);

                    mdx = OLAPDimension.Name;

                    if (counter == jumpvalue && debug == true)
                        break;

                    foreach (CubeAttribute OLAPDimAttribute in OLAPDimension.Attributes)
                    {
                        if (OLAPDimAttribute.AttributeHierarchyVisible == true)
                        {
                            TreeNode selectNode;
                            TreeNode infoTextNode;
                            CellSet newCell;
                            CellSet oldCell;
                            int numberOfRecordsOld = 0;
                            int numberOfRecordsNew = 0;
                            string errorMessage = "";
                            TreeNode measureNode;

                            pgMålTall.Value = 0;

                            foreach (Microsoft.AnalysisServices.Measure m in OLAPCubex.AllMeasures)
                            {
                                measureNode = new TreeNode(m.Name);

                                pgMålTall.Increment(1);
                                Application.DoEvents();

                                measure = "select [Measures].[" + m.Name + "] on 0, ";

                                mdx = "";
                                mdx = measure;
                                mdx += " NON EMPTY [" + OLAPDimension.Name + "].[" + OLAPDimAttribute.Attribute.Name + "]." + "[" + OLAPDimAttribute.Attribute.Name + "] on 1";
                                mdx += cubeName;
                                mdx += whereStatement;

                                if (runMdx(mdx, newServerConnection, out newCell, out numberOfRecordsNew))
                                {
                                    selectNode = new TreeNode();
                                    infoTextNode = new TreeNode();

                                    lbStatus.Text = OLAPDimension.Name + " / " + OLAPDimAttribute.Attribute.Name + " / " + m.Name;

                                    selectNode.Text = "OK. Select funker i ny " + selectCounter + ". Antall rader: " + numberOfRecordsNew;
                                    infoTextNode.Text = mdx;

                                    if (runMdx(mdx, oldServerConnection, out oldCell, out numberOfRecordsOld))
                                    {
                                        selectNode.Nodes.Add("OK. Select funker i gammel. Antall rader: " + numberOfRecordsOld);

                                        if (numberOfRecordsOld != 0)
                                        {
                                            if (compareContent(oldCell, newCell, out errorMessage) == false)
                                            {
                                                selectNode.Nodes.Add("FEIL: Sammenligning av tall. " + errorMessage);

                                                dimNode.ForeColor = Color.Red;
                                                selectNode.ForeColor = Color.Red;
                                                infoTextNode.ForeColor = Color.Red;
                                                measureNode.ForeColor = Color.Red;
                                            }
                                            else
                                            {
                                                selectNode.Nodes.Add("OK: Sammenligning av tall. " + errorMessage);
                                            }
                                        }
                                        infoTextNode.Text = mdx;
                                    }
                                    else
                                    {
                                        selectNode.Text = "Spørring feilet i gammel cube. Select " + selectCounter + " ... ";
                                        selectNode.ForeColor = Color.Red;
                                        infoTextNode.Text = mdx;
                                        dimNode.ForeColor = Color.Red;
                                        infoTextNode.ForeColor = Color.Red;
                                        measureNode.ForeColor = Color.Red;
                                    }

                                    selectNode.Nodes.Add(infoTextNode);
                                    measureNode.Nodes.Add(selectNode);
                                    dimNode.Nodes.Add(measureNode);
                                }
                                else
                                {
                                    selectNode = new TreeNode();
                                    selectNode.Text = "Warning.. Select " + selectCounter + " ... ";
                                    //selectNode.ForeColor = Color.Red;
                                    infoTextNode = new TreeNode(mdx);

                                    selectNode.Nodes.Add(infoTextNode);
                                    measureNode.Nodes.Add(selectNode);
                                    dimNode.Nodes.Add(measureNode);
                                }

                                Application.DoEvents();
                                selectCounter++;
                            }
                        }
                    }
                    counter++;

                    tvStatus.Nodes.Add(dimNode);
                    tvStatus.EndUpdate();
                }
            }
        }

        private bool runMdx(string MDX, string strConnection, out CellSet tmpCellSet, out int RecordCount)
        {
            RecordCount = 0;

            try
            {
                AdomdConnection conn = new AdomdConnection(strConnection);
                conn.Open();

                string commandText = MDX;

                AdomdCommand cmd = new AdomdCommand(commandText, conn);

                CellSet myCell = cmd.ExecuteCellSet();

                //http://msdn.microsoft.com/en-us/library/ms123476.aspx

                tmpCellSet = myCell;
                RecordCount = myCell.Cells.Count;
                return true;
            }
            catch (Exception e)
            {
                //lbInfo.Items.Add(e.Message);
                tmpCellSet = null;

                return false;
            }
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            loopCube(newServerConnection);

            MessageBox.Show("Ferdig. Antall tester gjennomført: " + numberOfTests +". Feil funnet: " + errorFound);
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            TreeNode tmpNode = tvStatus.GetNodeAt(X, Y);

            Clipboard.SetText(tmpNode.Text);

            MessageBox.Show("Lagret MDX til clipboard");
        }

        private void tvStatus_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                //Lagre koordinater

                X = e.X;
                Y = e.Y;
            }
        }
    }
}
