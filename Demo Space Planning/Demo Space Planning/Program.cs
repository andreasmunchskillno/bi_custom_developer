﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using IXCatalog;
using ProSpace;
using System.Runtime.InteropServices;

namespace Demo_Space_Planning
{
    class Program
    {
        // Planograms - Activate Method
        // Activates a Planogram object
        // This method should not be called for a Planogram within a project that is hidden
        // strFilePath         - Path of the PSA File to Open
        // nPlanogramIndex     - Index of the Planogram to Activate
        // strPlanogramName    - Name of the Planogram for verification
        public bool PlanogramsActivate(string strFilePath, int nPlanogramIndex, string strPlanogramName)
        {
            Application objPSApplication = null;      // Application object reference
            Projects objPSProjects = null;            // Projects object reference
            Project objPSProject = null;              // Project object reference
            Planograms objPSPlanograms = null;        // Planograms object reference
            Planogram objPSPlanogram = null;          // Planogram object reference
            bool bReturnValue;

            try
            {

                // Create Prospace application and get the reference to the application object
                objPSApplication = new Application();

                // Initialize SpacePlanning Objects
                objPSProjects = objPSApplication.Projects;

                // Opens a Space Planning Project
                objPSProject = objPSProjects.Open(strFilePath, true, null);

                objPSPlanograms = objPSProject.Planograms;

                // Activating the Planogram that is having index as nPlanogramIndex
                objPSPlanograms.Activate(nPlanogramIndex);

                // Get the Planogram object reference that represents the current Planogram
                objPSPlanogram = objPSApplication.ActivePlanogram;

                // Compare the Active Planogram name with the user input
                if ((string)objPSPlanogram.get_PlanogramField(ePlanogramFields.PlanogramName) == strPlanogramName && objPSPlanogram.Index == nPlanogramIndex)
                {
                    bReturnValue = true;
                }
                else
                {
                    bReturnValue = false;
                }
            }
            catch (Exception ex)
            {
                bReturnValue = false;
                // Log Error
                EventLog.WriteEntry("PlanogramsActivate : ", ex.Message.ToString(), EventLogEntryType.Error);
            }
            finally
            {
                // Release Objects and Cleanup
                Marshal.ReleaseComObject(objPSPlanogram);
                objPSPlanogram = null;
                Marshal.ReleaseComObject(objPSPlanograms);
                objPSPlanograms = null;
                Marshal.ReleaseComObject(objPSProjects);
                objPSProjects = null;
                if (objPSProject != null)
                {
                    objPSProject.Close(null, null, null, null, null);
                }
                Marshal.ReleaseComObject(objPSProject);
                objPSProject = null;
                Marshal.ReleaseComObject(objPSApplication);
                objPSApplication = null;

                // Check to see if Space Planning exited
                // Terminate Space Planning
                SharedFunctions.KillProcess("ProSpace") ;
            }
            return bReturnValue;
        }


        // Planograms - Add Method
        // Adds a new planogram to the existing Planograms collection.
        // Parameter Description:
        // strFilePath    - Path of the PSA File to Open
        public bool PlanogramsAdd(string strFilePath)
        {
            Application objPSApplication = null;      // Application object reference
            Projects objPSProjects = null;            // Projects object reference
            Project objPSProject = null;              // Project object reference
            Planograms objPSPlanograms = null;        // Planograms object reference
            Planogram objPSPlanogram = null;          // Planogram Object reference

            int nPlanogramsCountBeforeAdd;
            bool bReturnValue;

            try
            {
                // Create Prospace application and get the reference to the application object
                objPSApplication = new Application();

                objPSProjects = objPSApplication.Projects;

                // Opens a Space Planning Project
                objPSProject = objPSProjects.Open(strFilePath, null, null);

                objPSPlanograms = objPSProject.Planograms;

                // Count of Planograms before adding new Planogram
                nPlanogramsCountBeforeAdd = objPSPlanograms.Count;

                // Add a Planogram to the existing collection
                objPSPlanogram = objPSPlanograms.Add();

                // Compare the count before and after adding a Planogram
                if (objPSPlanograms.Count == nPlanogramsCountBeforeAdd + 1)
                {
                    bReturnValue = true;
                }
                else
                {
                    bReturnValue = false;
                }
            }
            catch (Exception ex)
            {
                bReturnValue = false;
                // Log Error
                EventLog.WriteEntry("PlanogramsAdd : ", ex.Message.ToString(), EventLogEntryType.Error);
            }
            finally
            {
                // Release Objects and Cleanup
                Marshal.ReleaseComObject(objPSPlanogram);
                objPSPlanogram = null;
                Marshal.ReleaseComObject(objPSPlanograms);
                objPSPlanograms = null;
                Marshal.ReleaseComObject(objPSProjects);
                objPSProjects = null;
                if (objPSProject != null)
                {
                    objPSProject.Close(null, null, null, null, null);
                }
                Marshal.ReleaseComObject(objPSProject);
                objPSProject = null;
                Marshal.ReleaseComObject(objPSApplication);
                objPSApplication = null;

                // Check to see if Space Planning exited
                // Terminate Space Planning
                SharedFunctions.KillProcess("ProSpace") ;
            }
            return bReturnValue;
        }


        // Planograms - DeSelect Method
        // Deselects the selected planogram from the collection.
        // Parameter Description:
        // strFilePath         - Path of the PSA File to Open
        // nPlanogramIndex     - Planogram Index to be deselected.
        public bool PlanogramsDeSelect(string strFilePath, int nPlanogramIndex)
        {
            Application objPSApplication = null;      // Application object reference
            Projects objPSProjects = null;            // Projects object reference
            Project objPSProject = null;              // Project object reference
            Planograms objPSPlanograms = null;        // Planograms object reference

            object selectedObjects;
            bool bFailed = false;
            int i;
            bool bReturnValue;

            try
            {

                // Create Prospace application and get the reference to the Planograms object
                objPSApplication = new Application();

                objPSProjects = objPSApplication.Projects;

                // Opens a Space Planning Project
                objPSProject = objPSProjects.Open(strFilePath, null, null);

                objPSPlanograms = objPSProject.Planograms;

                // First Select all the Planograms
                for (i = 0; i <= objPSPlanograms.Count - 1; i++)
                {
                    objPSPlanograms.Select(i, true);
                }

                // Get the information for selected objects
                selectedObjects = new object[objPSProject.SelectedObjectsCount, 4];

                // If object is not selected first then we cannot deselect
                objPSProject.GetSelectedObjects(ref selectedObjects);

                // Compare the index of the selected object with actual index and deselect the Planogram with given index
                for (i = 0; i <= ((object[,])selectedObjects).GetUpperBound(0); i++)
                {

                    if ((int)((object[,])selectedObjects)[i, 0] == (int)eProSpaceMetaDataObjects.MDPlanogram && (int)((object[,])selectedObjects)[i, 1] == nPlanogramIndex)
                    {
                        objPSPlanograms.Deselect(nPlanogramIndex);
                    }
                }

                // Get the selected objects information for the rest of the Planograms
                selectedObjects = new object[objPSProject.SelectedObjectsCount, 4];
                objPSProject.GetSelectedObjects(ref selectedObjects);

                // If the Deselected Planogram information is present then the above Deselect operation Failed.
                for (i = 0; i <= ((object[,])selectedObjects).GetUpperBound(0); i++)
                {

                    if ((int)((object[,])selectedObjects)[i, 0] == (int)eProSpaceMetaDataObjects.MDPlanogram && (int)((object[,])selectedObjects)[i, 1] == nPlanogramIndex)
                    {
                        bFailed = true;
                        break;
                    }
                }

                // If all conditions fulfill return true
                if (bFailed == false)
                {
                    bReturnValue = true;
                }
                else
                {
                    bReturnValue = false;
                }
            }
            catch (Exception ex)
            {
                bReturnValue = false;
                // Log Error
                EventLog.WriteEntry("PlanogramsDeSelect : ", ex.Message.ToString(), EventLogEntryType.Error);
            }
            finally
            {
                // Release Objects and Cleanup               
                Marshal.ReleaseComObject(objPSPlanograms);
                objPSPlanograms = null;
                Marshal.ReleaseComObject(objPSProjects);
                objPSProjects = null;
                if (objPSProject != null)
                {
                    objPSProject.Close(null, null, null, null, null);
                }
                Marshal.ReleaseComObject(objPSProject);
                objPSProject = null;
                Marshal.ReleaseComObject(objPSApplication);
                objPSApplication = null;

                // Check to see if Space Planning exited
                // Terminate Space Planning
                SharedFunctions.KillProcess("ProSpace") ;
            }
            return bReturnValue;
        }


        // Planograms - GetPlanogramFields Method
        // Retrieves one, more than one, or all Planogram data fields for one, more than one, or all Planograms in the collection.
        // Parameter Description:
        // strFilePath                 - Path of the PSA File to Open
        // nStartIndex                 - Start index as integer
        // planogramIndexes            - 1D Array to hold the Planogram indexes.
        // planogramFields             - 1D Array to hold field IDs of Planogram object. For all fields set it to null.
        // expectedPlanogramsData      - 2D Array to hold Planogram Field's Data. Beginning with column 2 for each row ( For example,(1, 2),(1, 3),....,(2, 2),(2, 3),........ ).
        // expectedErrors              - 1D Array to hold error numbers (error number as defined in eCatalogErrorNumbers).
        public bool PlanogramsGetPlanogramFields(string strFilePath, int nStartIndex, object planogramIndexes, object planogramFields, object expectedPlanogramsData, object expectedErrors)
        {
            Application objPSApplication = null;      // Application object reference
            Projects objPSProjects = null;            // Projects object reference
            Project objPSProject = null;              // Project object reference
            Planograms objPSPlanograms = null;        // Planograms object reference
            Planogram objPSPlanogram = null;          // Planogram object reference
                        
            object planogramData;
            object[,] readErrors;
            int nIndexes;
            int nFields;
            bool bFailed = false;
            string strErrors = "";
            int i;
            int j;
            bool bReturnValue;

            try
            {

                // Create Prospace application and get the reference to the Planograms object
                objPSApplication = new Application();

                objPSProjects = objPSApplication.Projects;

                // Opens a Space Planning Project
                objPSProject = objPSProjects.Open(strFilePath, null, null);
                objPSPlanograms = objPSProject.Planograms;

                // Number of indexes in planogramIndexes
                if (planogramIndexes != null)
                {
                    nIndexes = ((object[])planogramIndexes).GetUpperBound(0) + 1;
                }
                else
                {
                    // All objects in the collection
                    nIndexes = objPSPlanograms.Count;
                }

                // Number of fields in planogramFields
                if (planogramFields != null)
                {
                    // Number of fields plus one
                    nFields = ((object[])planogramFields).GetUpperBound(0) + 2;
                }
                else
                {
                    // All fields for Planogram object
                    nFields = objPSPlanograms.FieldIdCount;
                }

                // Re-dimensioning planogramData
                // The first upper bound should be the number of Planograms, and the second upper bound should be the number of fields plus one

                // If nStartIndex is greater than zero and index array is empty, then retrieve data for consecutive Planograms in the collection starting with the Planogram whose index is nStartIndex.
                if (nStartIndex > 0 && (planogramIndexes == null))
                {
                    planogramData = new object[nIndexes - nStartIndex + 1, nFields + 1];
                }
                else
                {
                    planogramData = new object[nIndexes + 1, nFields + 1];
                }

                // Retrieve data for Planograms whose indexes are in planogramIndexes, put each Planogram's object references in the 0 column
                if (planogramIndexes != null)
                {
                    for (i = 0; i <= nIndexes - 1; i++)
                    {
                        // Verify Planogram index available in planogramIndexes
                        if (Convert.ToInt32(((object[])planogramIndexes)[i]) < 0 || Convert.ToInt32(((object[])planogramIndexes)[i]) > objPSPlanograms.Count - 1)
                        {
                            throw new Exception("Planogram index " + Convert.ToInt32(((object[])planogramIndexes)[i]) + " is invalid.");
                        }

                        // Put each Planogram's object references in the 0 column like (1,0),(2,0)...
                        ((object[,])planogramData)[i + 1, 0] = objPSPlanograms.Item(Convert.ToInt32(((object[])planogramIndexes)[i]));
                    }
                }

                if (planogramFields != null)
                {
                    for (j = 0; j <= nFields - 2; j++)
                    {
                        // Indexes of the fields to be retrieved in consecutive cells of row zero like (0,2),(0,3)...
                        ((object[,])planogramData)[0, j + 2] = Convert.ToInt32(((object[])planogramFields)[j]);
                    }
                }

                if (planogramIndexes != null)
                {
                    readErrors = (object[,])objPSPlanograms.GetPlanogramFields(ref planogramData, null);
                }
                else
                {
                    // To retrieve data for consecutive Planograms in the collection starting with the Planogram whose index is nStartIndex.
                    readErrors = (object[,])objPSPlanograms.GetPlanogramFields(ref planogramData, nStartIndex);
                }

                // Verify data for the fields available in planogramFields (Selected fields)
                if (planogramFields != null)
                {
                    for (i = 1; i <= ((object[,])planogramData).GetUpperBound(0); i++)
                    {
                        for (j = 2; j <= ((object[,])planogramData).GetUpperBound(1); j++)
                        {
                            if (!Equals(((object[,])planogramData)[i, j], ((object[,])expectedPlanogramsData)[i, j]))
                            {
                                bFailed = true;
                                break;
                            }
                        }
                        if (bFailed == true)
                        {
                            break;
                        }
                    }
                }
                // Verify data for all fields (using PlanogramField property)
                else
                {
                    for (i = 1; i <= ((object[,])planogramData).GetUpperBound(0); i++)
                    {
                        if (nStartIndex > 0 && (planogramIndexes == null))
                        {
                            objPSPlanogram = objPSPlanograms.Item(nStartIndex + i - 1);

                        }
                        else if (nStartIndex == 0 && (planogramIndexes == null))
                        {
                            objPSPlanogram = objPSPlanograms.Item(i - 1);
                        }
                        else
                        {
                            objPSPlanogram = objPSPlanograms.Item(Convert.ToInt32(((object[])planogramIndexes)[i - 1]));
                        }

                        for (j = 2; j <= ((object[,])planogramData).GetUpperBound(1); j++)
                        {
                            if (!Equals(((object[,])planogramData)[i, j], objPSPlanogram.get_PlanogramFieldN(j - 2)))
                            {
                                bFailed = true;
                                break;
                            }
                        }
                        if (bFailed == true)
                        {
                            break;
                        }
                    }
                }

                // Check for Errors
                if (expectedErrors != null)
                {
                    if (readErrors != null)
                    {
                        for (i = 0; i <= ((object[])expectedErrors).GetUpperBound(0); i++)
                        {
                            // Check the Expected errors with actual errors
                            if (!Equals(readErrors[i, 2], ((object[])expectedErrors)[i]))
                            {
                                throw new Exception("Expected error " + ((object[])expectedErrors)[i] + " in row " + (i + 1) + " is not matching with actual error. " + "Actual error encountered is " + readErrors[i, 2] + "   " + SharedFunctions.ErrorDescription((int)readErrors[i, 2]));
                            }
                        }
                        bFailed = false;
                    }
                    else
                    {
                        throw new Exception("No error encountered in the process.");
                    }
                }
                else
                {
                    // Raise all errors encountered in the process if data is not matching
                    if (readErrors != null && bFailed == true)
                    {
                        for (i = 0; i <= readErrors.GetUpperBound(0); i++)
                        {
                            strErrors = strErrors + "In Cell(" + readErrors[i, 0] + "," + readErrors[i, 1] + ") : " + readErrors[i, 2] + "   " + SharedFunctions.ErrorDescription((int)readErrors[i, 2]) + "  ";
                        }
                        throw new Exception("Error encountered in the execution and the details are : " + strErrors);
                    }
                }

                if (bFailed == false)
                {
                    bReturnValue = true;
                }
                else
                {
                    bReturnValue = false;
                }
            }
            catch (Exception ex)
            {
                bReturnValue = false;
                // Log Error
                EventLog.WriteEntry("PlanogramsGetPlanogramFields : ", ex.Message.ToString(), EventLogEntryType.Error);
            }
            finally
            {
                // Clean up all the objects
                if (objPSPlanogram != null)
                {
                    Marshal.ReleaseComObject(objPSPlanogram);
                }
                objPSPlanogram = null;
                Marshal.ReleaseComObject(objPSPlanograms);
                objPSPlanograms = null;
                Marshal.ReleaseComObject(objPSProjects);
                objPSProjects = null;
                if (objPSProject != null)
                {
                    objPSProject.Close(null, null, null, null, null);
                }
                Marshal.ReleaseComObject(objPSProject);
                objPSProject = null;
                Marshal.ReleaseComObject(objPSApplication);
                objPSApplication = null;

                // Check to see if Space Planning exited
                // Terminate Space Planning
                SharedFunctions.KillProcess("ProSpace") ;
            }
            return bReturnValue;
        }

        /*
        // Planograms - Index Method
        // Returns the index of a particular Planogram in the collection.
        // Parameter Description:
        // strFilePath         - Path of the PSA File to Open
        // nPlanogramIndex     - Expected Planogram Index.
        public bool PlanogramsIndex(string strFilePath, int nPlanogramIndex)
        {
            Application objPSApplication = null;      // Application object reference
            Projects objPSProjects = null;            // Projects object reference
            Project objPSProject = null;              // Project object reference
            Planograms objPSPlanograms = null;        // Planograms object reference

            object selectedObjects = null;
            bool bReturnValue;

            try
            {

                // Create Prospace application and get the reference to the Planograms object
                objPSApplication = new Application();

                objPSProjects = objPSApplication.Projects;

                // Opens a Space Planning Project
                objPSProject = objPSProjects.Open(strFilePath, null, null);

                objPSPlanograms = objPSProject.Planograms;

                // Select the object in the index
                objPSPlanograms.Select(nPlanogramIndex, true);

                // Get the Information about selected object and get its reference.cell 0-Type of object;cell 1-Selected object index
                selectedObjects = new object[1, 2];
                objPSProject.GetSelectedObjectRefs(ref selectedObjects);

                // Get the index of the object reference from above call.
                if (objPSPlanograms.Index(((object[,])selectedObjects)[0, 1]) == nPlanogramIndex)
                {
                    bReturnValue = true;
                }
                else
                {
                    bReturnValue = false;
                }
            }
            catch (Exception ex)
            {
                bReturnValue = false;
                // Log Error
                EventLog.WriteEntry("PlanogramsIndex : ", ex.Message.ToString(), EventLogEntryType.Error);
            }
            finally
            {
                // Release Objects and Cleanup
                ((object[,])selectedObjects)[0, 1] = null;
                Marshal.ReleaseComObject(objPSPlanograms);
                objPSPlanograms = null;
                Marshal.ReleaseComObject(objPSProjects);
                objPSProjects = null;
                if (objPSProject != null)
                {
                    objPSProject.Close(null, null, null, null, null);
                }
                Marshal.ReleaseComObject(objPSProject);
                objPSProject = null;
                Marshal.ReleaseComObject(objPSApplication);
                objPSApplication = null;

                // Check to see if Space Planning exited
                // Terminate Space Planning
                SharedFunctions.KillProcess("ProSpace") ;
            }
            return bReturnValue;
        }
        */

        // Planograms - Item Method
        // Returns a single member of a collection.
        // strFilePath        - Path of the PSA File to Open
        // nPlanogramIndex    - Planogram Index for which reference is returned.
        public bool PlanogramsItem(string strFilePath, int nPlanogramIndex)
        {
            Application objPSApplication = null;      // Application object reference
            Projects objPSProjects = null;            // Projects object reference
            Project objPSProject = null;              // Project object reference
            Planograms objPSPlanograms = null;        // Planograms object reference
            Planogram objPSPlanogram = null;          // Planogram Object reference

            object selectedObjects = null;
            bool bReturnValue;

            try
            {

                // Create Prospace application and get the reference to the Planograms object
                objPSApplication = new Application();

                objPSProjects = objPSApplication.Projects;

                // Opens a Space Planning Project
                objPSProject = objPSProjects.Open(strFilePath, null, null);

                objPSPlanograms = objPSProject.Planograms;

                // Select the object in the index
                objPSPlanograms.Select(nPlanogramIndex, true);

                // Get the Information about selected object and get its reference.
                selectedObjects = new object[1, 2]; // Cell 0: The object's type as defined in the JDA Space Planning Type Library.
                // Cell 1: The selected object's reference.

                objPSProject.GetSelectedObjectRefs(ref selectedObjects);

                // Get the reference to the item with given index
                objPSPlanogram = objPSPlanograms.Item(nPlanogramIndex);

                // Compare the two object references
                if (objPSPlanogram == ((object[,])selectedObjects)[0, 1])
                {
                    bReturnValue = true;
                }
                else
                {
                    bReturnValue = false;
                }
            }
            catch (Exception ex)
            {
                bReturnValue = false;
                // Log Error
                EventLog.WriteEntry("PlanogramsItem : ", ex.Message.ToString(), EventLogEntryType.Error);
            }
            finally
            {
                // Release Objects and Cleanup
                ((object[,])selectedObjects)[0, 1] = null;
                Marshal.ReleaseComObject(objPSPlanogram);
                objPSPlanogram = null;
                Marshal.ReleaseComObject(objPSPlanograms);
                objPSPlanograms = null;
                Marshal.ReleaseComObject(objPSProjects);
                objPSProjects = null;
                if (objPSProject != null)
                {
                    objPSProject.Close(null, null, null, null, null);
                }
                Marshal.ReleaseComObject(objPSProject);
                objPSProject = null;
                Marshal.ReleaseComObject(objPSApplication);
                objPSApplication = null;

                // Check to see if Space Planning exited
                // Terminate Space Planning
                SharedFunctions.KillProcess("ProSpace") ;
            }
            return bReturnValue;
        }


        // Planograms - Remove Method
        // Removes planogram from Planograms collection
        // strFilePath        - Path of the PSA File to Open
        // nPlanogramIndex    - PlanogramIndex to be removed.
        public bool PlanogramsRemove(string strFilePath, int nPlanogramIndex)
        {
            Application objPSApplication = null;      // Application object reference
            Projects objPSProjects = null;            // Projects object reference
            Project objPSProject = null;              // Project object reference
            Planograms objPSPlanograms = null;        // Planograms object reference
            Planogram objPSPlanogram = null;          // Planogram Object reference

            object selectedObjects = null;
            bool bFailed = false;
            int i;
            bool bReturnValue;

            try
            {

                // Create Prospace application and get the reference to the application object
                objPSApplication = new Application();

                objPSProjects = objPSApplication.Projects;

                // Opens a Space Planning Project
                objPSProject = objPSProjects.Open(strFilePath, null, null);

                objPSPlanograms = objPSProject.Planograms;

                //Get the Planogram reference with the given index
                objPSPlanogram = objPSPlanograms.Item(nPlanogramIndex);

                // Select all the Planograms initially
                for (i = 0; i <= objPSPlanograms.Count - 1; i++)
                {
                    objPSPlanograms.Select(i, true);
                }

                // Remove the planogram with given index
                objPSPlanograms.Remove(nPlanogramIndex);

                // Get information about the selected Planograms
                selectedObjects = new object[objPSProject.SelectedObjectsCount - 1 + 1, 2];
                objPSProject.GetSelectedObjectRefs(ref selectedObjects);

                // If the removed object reference is found in selected objects information return false and close the application
                for (i = 0; i <= ((object[,])selectedObjects).GetUpperBound(0); i++)
                {
                    if (((object[,])selectedObjects)[i, 1] == objPSPlanogram)
                    {
                        bFailed = true;
                        break;
                    }
                }

                // If the removed object reference is not present in selected objects information,return true from function
                if (bFailed == false)
                {
                    bReturnValue = true;
                }
                else
                {
                    bReturnValue = false;
                }
            }
            catch (Exception ex)
            {
                bReturnValue = false;
                // Log Error
                EventLog.WriteEntry("PlanogramsRemove : ", ex.Message.ToString(), EventLogEntryType.Error);
            }
            finally
            {
                // Release Objects and Cleanup
                for (i = 0; i <= ((object[,])selectedObjects).GetUpperBound(0); i++)
                {
                    ((object[,])selectedObjects)[i, 1] = null;
                }
                Marshal.ReleaseComObject(objPSPlanogram);
                objPSPlanogram = null;
                Marshal.ReleaseComObject(objPSPlanograms);
                objPSPlanograms = null;
                Marshal.ReleaseComObject(objPSProjects);
                objPSProjects = null;
                if (objPSProject != null)
                {
                    objPSProject.Close(null, null, null, null, null);
                }
                Marshal.ReleaseComObject(objPSProject);
                objPSProject = null;
                Marshal.ReleaseComObject(objPSApplication);
                objPSApplication = null;

                // Check to see if Space Planning exited
                // Terminate Space Planning
                SharedFunctions.KillProcess("ProSpace") ;
            }
            return bReturnValue;

        }


        // Planograms - Select Method
        // Selects the Planogram object in the collection.
        // strFilePath        - Path of the PSA File to Open
        // nPlanogramIndex    - Planogram Index to be selected.
        public bool PlanogramsSelect(string strFilePath, int nPlanogramIndex)
        {
            Application objPSApplication = null;      // Application object reference
            Projects objPSProjects = null;            // Projects object reference
            Project objPSProject = null;              // Project object reference
            Planograms objPSPlanograms = null;        // Planograms object reference

            object selectedObjects = null;
            bool bReturnValue;

            try
            {

                // Create Prospace application and get the reference to the Planograms object
                objPSApplication = new Application();

                objPSProjects = objPSApplication.Projects;

                // Opens a Space Planning Project
                objPSProject = objPSProjects.Open(strFilePath, null, null);
                objPSPlanograms = objPSProject.Planograms;

                // Select the Object with the given index
                objPSPlanograms.Select(nPlanogramIndex, true);

                // Get the Information for the selected object.0-Type of object,1-Index of object selected
                selectedObjects = new object[1, 4]; // cell 0 : Object type; cell 1 :Selected object index; cell 2:Index of Parent object; cell 3:Index of Planogram

                objPSProject.GetSelectedObjects(ref selectedObjects);

                // Compare the index of the selected object with actual index
                if ((int)((object[,])selectedObjects)[0, 0] == (int)eProSpaceMetaDataObjects.MDPlanogram && (int)((object[,])selectedObjects)[0, 1] == nPlanogramIndex)
                {
                    bReturnValue = true;
                }
                else
                {
                    bReturnValue = false;
                }
            }
            catch (Exception ex)
            {
                bReturnValue = false;
                // Log Error
                EventLog.WriteEntry("PlanogramsSelect : ", ex.Message.ToString(), EventLogEntryType.Error);
            }
            finally
            {
                // Release Objects and Cleanup                
                Marshal.ReleaseComObject(objPSPlanograms);
                objPSPlanograms = null;
                Marshal.ReleaseComObject(objPSProjects);
                objPSProjects = null;
                if (objPSProject != null)
                {
                    objPSProject.Close(null, null, null, null, null);
                }
                Marshal.ReleaseComObject(objPSProject);
                objPSProject = null;
                Marshal.ReleaseComObject(objPSApplication);
                objPSApplication = null;

                // Check to see if Space Planning exited
                // Terminate Space Planning
                SharedFunctions.KillProcess("ProSpace") ;
            }
            return bReturnValue;
        }
        
        // Planograms - SetPlanogramFields Method
        // Updates one, more than one, or all of the Planogram data fields for one or more items in the collection, or creates one or more new Planograms in the collection
        // Parameter Description:
        // strFilePath               - Path of the PSA File to Open
        // planogramIndexes          - 1D Array to hold Planogram indexes.
        // planogramFields           - 1D Array to hold field IDs of Planogram object.
        // expectedPlanogramsData    - 2D Array to hold Planogram Field's Data. Beginning with column 2 for each row ( For example,(1, 2),(1, 3),....,(2, 2),(2, 3),........ ).
        // expectedErrors            - 1D Array to hold error numbers (error number as defined in eCatalogErrorNumbers).
        // insertAfterIndex          - 1D Array with indexes of Planograms after which newly created Planograms are to be placed.
        public bool PlanogramsSetPlanogramFields(string strFilePath, object planogramIndexes, object planogramFields, object expectedPlanogramsData, object expectedErrors, object insertafterIndex)
        {
            Application objPSApplication = null;      // Application object reference
            Projects objPSProjects = null;            // Projects object reference
            Project objPSProject = null;              // Project object reference
            Planograms objPSPlanograms = null;        // Planograms object reference
            Planogram objPSPlanogram = null;          // Planogram object reference
                        
            object setPlanogramData = null;
            object getPlanogramData = null;
            object[,] readErrors;
            object[,] writeErrors;
            int nInitialCountOfPlanograms;
            int nIndexes = 0;
            int nFields;
            bool bFailed = false;
            string strErrors = "";
            int i;
            int j;
            bool bReturnValue;

            try
            {

                // Create Prospace application and get the reference to the application object
                objPSApplication = new Application();
                objPSProjects = objPSApplication.Projects;

                // Opens a Space Planning Project
                objPSProject = objPSProjects.Open(strFilePath, null, null);
                objPSPlanograms = objPSProject.Planograms;

                // Get the initial count of Planograms
                nInitialCountOfPlanograms = objPSPlanograms.Count;

                // Number of indexes in planogramIndexes (if empty then SetPlanogramFields will attempt to create a new default object and set the fields that specified in the planogramFields.)
                if (planogramIndexes != null)
                {
                    if (((object[,])expectedPlanogramsData).GetUpperBound(0)!= ((object[])planogramIndexes).GetUpperBound(0) + 1)
                    {
                        throw new Exception("ExpectedPlanogramsData array not having enough records as compared to PlanogramIndexes array");
                    }
                    else
                    {
                        nIndexes = ((object[])planogramIndexes).GetUpperBound(0) + 1;
                    }
                }

                // Number of fields in planogramFields
                if (planogramFields != null)
                {
                    nFields = ((object[])planogramFields).GetUpperBound(0) + 1;
                }
                else
                {
                    throw new Exception("PlanogramFields Array cannot be empty");
                }

                // Re-dimensioning vaPlanogramData
                // The first upper bound should be the number of Planograms, and the second upper bound should be the number of fields plus one

                // To set fields in planogramFields for indexes in planogramIndexes.
                if (nIndexes != 0 && nFields != 0)
                {

                    setPlanogramData = new object[nIndexes + 1, nFields + 2];
                    getPlanogramData = new object[nIndexes + 1, nFields + 2];
                    
                }
                // To create new Planograms using fields available in planogramFields
                else if (nFields != 0 && nIndexes == 0)
                {

                    setPlanogramData = new object[((object[,])expectedPlanogramsData).GetUpperBound(0) + 1, nFields + 2];
                    getPlanogramData = new object[((object[,])expectedPlanogramsData).GetUpperBound(0) + 1, nFields + 2];

                }

                // Fills the vaPlanogramData's first column with object references of Planograms in the collection
                if (nIndexes != 0)
                {
                    for (i = 0; i <= nIndexes - 1; i++)
                    {
                        // Verify Planogram index available in planogramIndexes
                        if (Convert.ToInt32(((object[])planogramIndexes)[i]) < 0 || Convert.ToInt32(((object[])planogramIndexes)[i]) > objPSPlanograms.Count - 1)
                        {
                            throw new Exception("Planogram index " + Convert.ToInt32(((object[])planogramIndexes)[i]) + " is invalid.");
                        }

                        // Put each Planogram's object references in the 0 column like (1,0),(2,0)...
                        ((object[,])setPlanogramData)[i + 1, 0] = objPSPlanograms.Item(Convert.ToInt32(((object[])planogramIndexes)[i]));
                        ((object[,])getPlanogramData)[i + 1, 0] = objPSPlanograms.Item(Convert.ToInt32(((object[])planogramIndexes)[i]));
                    }
                }
                else
                {
                    // Column1 contains indexes of the Planograms after which newly created Planograms are to be placed. If you put –1, the Planogram will be inserted at the end.
                    if (insertafterIndex != null)
                    {
                        for (i = 1; i <= ((object[,])setPlanogramData).GetUpperBound(0); i++)
                        {
                            ((object[,])setPlanogramData)[i, 1] = Convert.ToInt32(((object[])insertafterIndex)[i - 1]);
                        }
                    }
                }

                // Specify the indexes of the fields to be retrieved in consecutive cells of row zero beginning with the cell 0,2
                for (j = 0; j <= nFields - 1; j++)
                {
                    // Indexes of the fields to be retrieved in consecutive cells of row zero like (0,2),(0,3)...
                    ((object[,])setPlanogramData)[0, j + 2] = Convert.ToInt32(((object[])planogramFields)[j]);
                    ((object[,])getPlanogramData)[0, j + 2] = Convert.ToInt32(((object[])planogramFields)[j]);
                }

                // In each row starting with column 2, specify the values to set each field specified in the top row.
                for (i = 1; i <= ((object[,])expectedPlanogramsData).GetUpperBound(0); i++)
                {
                    for (j = 2; j <= ((object[,])expectedPlanogramsData).GetUpperBound(1); j++)
                    {
                        ((object[,])setPlanogramData)[i, j] = ((object[,])expectedPlanogramsData)[i, j];
                    }
                }

                // SetPlanogramFields can be used either to create new objects or modify existing objects
                writeErrors = (object[,])objPSPlanograms.SetPlanogramFields(ref setPlanogramData);

                // Get the data for newly created Planograms
                if (nIndexes == 0)
                {
                    if (insertafterIndex != null)
                    {
                        // Get the indexes of the newly created Planograms
                        for (i = 1; i <= ((object[,])getPlanogramData).GetUpperBound(0); i++)
                        {
                            if (Convert.ToInt32(((object[])insertafterIndex)[i - 1]) != -1)
                            {
                                ((object[,])getPlanogramData)[i, 0] = objPSPlanograms.Item((Convert.ToInt32(((object[])insertafterIndex)[i - 1])) + 1);
                            }
                            else
                            {
                                ((object[,])getPlanogramData)[i, 0] = objPSPlanograms.Item(nInitialCountOfPlanograms + i - 1);
                            }
                        }
                    }
                }

                // To retrieve data for Planograms in the collection.
                readErrors = (object[,])objPSPlanograms.GetPlanogramFields(ref getPlanogramData, null);

                // Check expected data with actual data
                for (i = 1; i <= ((object[,])getPlanogramData).GetUpperBound(0); i++)
                {
                    for (j = 2; j <= ((object[,])getPlanogramData).GetUpperBound(1); j++)
                    {
                        if (!Equals(((object[,])getPlanogramData)[i, j], ((object[,])expectedPlanogramsData)[i, j]))
                        {
                            bFailed = true;
                            break;
                        }
                    }
                    if (bFailed == true)
                    {
                        break;
                    }
                }

                // Check for Errors
                if (expectedErrors != null)
                {
                    if (writeErrors != null)
                    {
                        for (i = 0; i <= ((object[])expectedErrors).GetUpperBound(0); i++)
                        {
                            // Check the Expected errors with actual errors
                            if (!Equals(writeErrors[i, 2], ((object[])expectedErrors)[i]))
                            {
                                throw new Exception("Expected error " + ((object[])expectedErrors)[i] + " in row " + (i + 1) + " is not matching with actual error. " + "Actual error encountered is " + writeErrors[i, 2] + "   " + SharedFunctions.ErrorDescription((int)writeErrors[i, 2]));
                            }
                        }
                        bFailed = false;
                    }
                    else
                    {
                        throw new Exception("No error encountered in the process.");
                    }
                }
                else
                {
                    // Raise all errors encountered in the process if data is not matching
                    if (writeErrors != null && bFailed == true)
                    {
                        for (i = 0; i <= writeErrors.GetUpperBound(0); i++)
                        {
                            strErrors = strErrors + "In Cell(" + writeErrors[i, 0] + "," + writeErrors[i, 1] + ") : " + writeErrors[i, 2] + "   " + SharedFunctions.ErrorDescription((int)writeErrors[i, 2]) + "  ";
                        }
                        throw new Exception("Error encountered in the execution and the details are : " + strErrors);
                    }
                }

                if (bFailed == false)
                {
                    bReturnValue = true;
                }
                else
                {
                    bReturnValue = false;
                }
            }
            catch (Exception ex)
            {
                bReturnValue = false;
                // Log Error
                EventLog.WriteEntry("PlanogramsSetPlanogramFields : ", ex.Message.ToString(), EventLogEntryType.Error);
            }
            finally
            {
                // Clean up all the objects
                if (objPSPlanogram != null)
                {
                    Marshal.ReleaseComObject(objPSPlanogram);
                }
                objPSPlanogram = null;
                Marshal.ReleaseComObject(objPSPlanograms);
                objPSPlanograms = null;
                Marshal.ReleaseComObject(objPSProjects);
                objPSProjects = null;
                if (objPSProject != null)
                {
                    objPSProject.Close(null, null, null, null, null);
                }
                Marshal.ReleaseComObject(objPSProject);
                objPSProject = null;
                Marshal.ReleaseComObject(objPSApplication);
                objPSApplication = null;

                // Check to see if Space Planning exited
                // Terminate Space Planning
                SharedFunctions.KillProcess("ProSpace") ;
            }
            return bReturnValue;
        }


        // Planograms - Application Property
        // Returns an Application object reference that represents the instance of Space Planning to which the object belongs.
        // Parameter Description:
        // strFilePath           - Path of the PSA File to Open
        // strApplicationName    - Name of the Application to compare with.
        public bool PlanogramsApplication(string strFilePath, string strApplicationName)
        {
            Application objPSApplication = null;      // Application object reference
            Projects objPSProjects = null;            // Projects object reference
            Project objPSProject = null;              // Project object reference
            Planograms objPSPlanograms = null;        // Planograms object reference
            object objExpectedObject = null;          // Generic object reference           
            bool bReturnValue;

            try
            {

                // Create Prospace application and get the reference to the application object
                objPSApplication = new Application();

                objPSProjects = objPSApplication.Projects;

                // Opens a Space Planning Project
                objPSProject = objPSProjects.Open(strFilePath, null, null);

                objPSPlanograms = objPSProject.Planograms;

                // Get the reference to application object from planogram object
                objExpectedObject = objPSPlanograms.Application;

                // Compare the two application references
                if (objExpectedObject is Application && objExpectedObject == objPSApplication && strApplicationName == ((Application)objExpectedObject).Name)
                {
                    bReturnValue = true;
                }
                else
                {
                    bReturnValue = false;
                }
            }
            catch (Exception ex)
            {
                bReturnValue = false;
                // Log Error
                EventLog.WriteEntry("PlanogramsApplication : ", ex.Message.ToString(), EventLogEntryType.Error);
            }
            finally
            {
                // Release Objects and Cleanup
                Marshal.ReleaseComObject(objPSPlanograms);
                objPSPlanograms = null;
                Marshal.ReleaseComObject(objPSProjects);
                objPSProjects = null;
                if (objPSProject != null)
                {
                    objPSProject.Close(null, null, null, null, null);
                }
                Marshal.ReleaseComObject(objPSProject);
                objPSProject = null;
                Marshal.ReleaseComObject(objExpectedObject);
                objExpectedObject = null;
                Marshal.ReleaseComObject(objPSApplication);
                objPSApplication = null;

                // Check to see if Space Planning exited
                // Terminate Space Planning
                SharedFunctions.KillProcess("ProSpace") ;
            }
            return bReturnValue;
        }


        // Planograms - Count Property
        // Returns the number of objects in the collection.
        // Parameter Description:
        // strFilePath                  - Path of the PSA File to Open
        // nExpectedPlanogramsCount     - Count of the Planogram in the Project.
        public bool PlanogramsCount(string strFilePath, int nExpectedPlanogramsCount)
        {
            Application objPSApplication = null;      // Application object reference
            Projects objPSProjects = null;            // Projects object reference
            Project objPSProject = null;              // Project object reference
            Planograms objPSPlanograms = null;        // Planograms object reference
            bool bReturnValue;

            try
            {

                // Create Prospace application and get the reference to the application object
                objPSApplication = new Application();

                objPSProjects = objPSApplication.Projects;

                // Opens a Space Planning Project
                objPSProject = objPSProjects.Open(strFilePath, null, null);
                objPSPlanograms = objPSProject.Planograms;

                // Compare the Planograms count with expected count
                if (objPSPlanograms.Count == nExpectedPlanogramsCount)
                {
                    bReturnValue = true;
                }
                else
                {
                    bReturnValue = false;
                }
            }
            catch (Exception ex)
            {
                bReturnValue = false;
                // Log Error
                EventLog.WriteEntry("PlanogramsCount : ", ex.Message.ToString(), EventLogEntryType.Error);
            }
            finally
            {
                Marshal.ReleaseComObject(objPSPlanograms);
                objPSPlanograms = null;
                Marshal.ReleaseComObject(objPSProjects);
                objPSProjects = null;
                if (objPSProject != null)
                {
                    objPSProject.Close(null, null, null, null, null);
                }
                Marshal.ReleaseComObject(objPSProject);
                objPSProject = null;
                Marshal.ReleaseComObject(objPSApplication);
                objPSApplication = null;

                // Check to see if Space Planning exited
                // Terminate Space Planning
                SharedFunctions.KillProcess("ProSpace") ;
            }
            return bReturnValue;
        }


        // Planograms - FieldIDCount Property
        // Returns the number of fields that make up a Space Planning data object.
        // Parameter Description:
        // strFilePath              - Path of the PSA File to Open
        // nExpectedFieldIDCount    - Expected Field ID Count for ePlanogramFields.
        public bool PlanogramsFieldIDCount(string strFilePath, int nExpectedFieldIDCount)
        {
            Application objPSApplication = null;      // Application object reference
            Projects objPSProjects = null;            // Projects object reference
            Project objPSProject = null;              // Project object reference
            Planograms objPSPlanograms = null;        // Planograms object reference
            bool bReturnValue;

            try
            {

                // Create the Prospace Application and get the reference to Planograms object
                objPSApplication = new Application();

                objPSProjects = objPSApplication.Projects;

                // Opens a Space Planning Project
                objPSProject = objPSProjects.Open(strFilePath, null, null);

                objPSPlanograms = objPSProject.Planograms;

                //If Actual count doesn't equal Expected value return false
                if (objPSPlanograms.FieldIdCount == nExpectedFieldIDCount)
                {
                    bReturnValue = true;
                }
                else
                {
                    bReturnValue = false;
                }
            }
            catch (Exception ex)
            {
                bReturnValue = false;
                // Log Error
                EventLog.WriteEntry("PlanogramsFieldIDCount : ", ex.Message.ToString(), EventLogEntryType.Error);
            }
            finally
            {
                Marshal.ReleaseComObject(objPSPlanograms);
                objPSPlanograms = null;
                Marshal.ReleaseComObject(objPSProjects);
                objPSProjects = null;
                if (objPSProject != null)
                {
                    objPSProject.Close(null, null, null, null, null);
                }
                Marshal.ReleaseComObject(objPSProject);
                objPSProject = null;
                Marshal.ReleaseComObject(objPSApplication);
                objPSApplication = null;

                // Check to see if Space Planning exited
                // Terminate Space Planning
                SharedFunctions.KillProcess("ProSpace") ;
            }
            return bReturnValue;
        }


        // Planograms - Parent Property
        // Returns the Parent object of the Planogram which is Project object.
        // Parameter Description:
        // strFilePath    - Path of the PSA File to Open
        public bool PlanogramsParent(string strFilePath)
        {
            Application objPSApplication = null;      // Application object reference
            Projects objPSProjects = null;            // Projects object reference
            Project objPSProject = null;              // Project object reference
            Planograms objPSPlanograms = null;        // Planograms object reference
            object objExpectedObject = null;          // Generic spacePlanning object reference
            bool bReturnValue;

            try
            {

                // Create Prospace application and get the reference to the application object
                objPSApplication = new Application();

                objPSProjects = objPSApplication.Projects;

                // Opens a Space Planning Project
                objPSProject = objPSProjects.Open(strFilePath, null, null);

                objPSPlanograms = objPSProject.Planograms;

                // Get the reference to Project object from planogram object through parent property
                objExpectedObject = objPSPlanograms.Parent;

                // Compare the typeof objExpectedObject is Project and it references the actual Project object
                if (objExpectedObject is Project && objExpectedObject == objPSProject)
                {
                    bReturnValue = true;
                }
                else
                {
                    bReturnValue = false;
                }
            }
            catch (Exception ex)
            {
                bReturnValue = false;
                // Log Error
                EventLog.WriteEntry("PlanogramsParent : ", ex.Message.ToString(), EventLogEntryType.Error);
            }
            finally
            {
                // Release Objects and Cleanup
                Marshal.ReleaseComObject(objExpectedObject);
                objExpectedObject = null;
                Marshal.ReleaseComObject(objPSPlanograms);
                objPSPlanograms = null;
                Marshal.ReleaseComObject(objPSProjects);
                objPSProjects = null;
                if (objPSProject != null)
                {
                    objPSProject.Close(null, null, null, null, null);
                }
                Marshal.ReleaseComObject(objPSProject);
                objPSProject = null;
                Marshal.ReleaseComObject(objPSApplication);
                objPSApplication = null;

                // Check to see if Space Planning exited
                // Terminate Space Planning
                SharedFunctions.KillProcess("ProSpace") ;
            }
            return bReturnValue;
        }


        // Planograms - Project Property
        // Returns an object reference to the parent Project.
        // Parameter Description:
        // strFilePath       - Path of the PSA File to Open
        // strProjectName    - Project name for verification
        public bool PlanogramsProject(string strFilePath, string strProjectName)
        {
            Application objPSApplication = null;      // Application object reference
            Projects objPSProjects = null;            // Projects object reference
            Project objPSProject = null;              // Project object reference
            Planograms objPSPlanograms = null;        // Planograms object reference
            object objExpectedObject = null;          // Generic SpacePlanning object reference
            bool bReturnValue;

            try
            {
                // Create Prospace application and get the reference to the Planograms object
                objPSApplication = new Application();

                objPSProjects = objPSApplication.Projects;

                // Opens a Space Planning Project
                objPSProject = objPSProjects.Open(strFilePath, null, null);
                objPSPlanograms = objPSProject.Planograms;

                // Get the reference to Project object from planogram object through parent property
                objExpectedObject = objPSPlanograms.Project;

                // Compare the typeof objExpectedObject is Project and it references the actual Project object
                if (objExpectedObject is Project && objExpectedObject == objPSProject && ((Project)objExpectedObject).Name == strProjectName)
                {
                    bReturnValue = true;
                }
                else
                {
                    bReturnValue = false;
                }
            }
            catch (Exception ex)
            {
                bReturnValue = false;
                // Log Error
                EventLog.WriteEntry("PlanogramsProject : ", ex.Message.ToString(), EventLogEntryType.Error);
            }
            finally
            {
                // Release Objects and Cleanup
                Marshal.ReleaseComObject(objExpectedObject);
                objExpectedObject = null;
                Marshal.ReleaseComObject(objPSPlanograms);
                objPSPlanograms = null;
                Marshal.ReleaseComObject(objPSProjects);
                objPSProjects = null;
                if (objPSProject != null)
                {
                    objPSProject.Close(null, null, null, null, null);
                }
                Marshal.ReleaseComObject(objPSProject);
                objPSProject = null;
                Marshal.ReleaseComObject(objPSApplication);
                objPSApplication = null;

                // Check to see if Space Planning exited
                // Terminate Space Planning
                SharedFunctions.KillProcess("ProSpace") ;
            }
            return bReturnValue;
        }

        // Planograms - GetPlanogramFields Method
        // Retrieves one, more than one, or all Planogram data fields for one, more than one, or all Planograms in the collection.
        // Parameter Description:
        // strFilePath                 - Path of the PSA File to Open
        // nStartIndex                 - Start index as integer
        // planogramIndexes            - 1D Array to hold the Planogram indexes.
        // planogramFields             - 1D Array to hold field IDs of Planogram object. For all fields set it to null.
        // expectedPlanogramsData      - 2D Array to hold Planogram Field's Data. Beginning with column 2 for each row ( For example,(1, 2),(1, 3),....,(2, 2),(2, 3),........ ).
        // expectedErrors              - 1D Array to hold error numbers (error number as defined in eCatalogErrorNumbers).
        public bool SindreTest(string strFilePath, int nStartIndex, object planogramIndexes, object planogramFields, object expectedPlanogramsData, object expectedErrors)
        {
            Application objPSApplication = null;      // Application object reference
            Projects objPSProjects = null;            // Projects object reference
            Project objPSProject = null;              // Project object reference
            Planograms objPSPlanograms = null;        // Planograms object reference
            Planogram objPSPlanogram = null;          // Planogram object reference

            object planogramData;
            object[,] readErrors;
            int nIndexes;
            int nFields;
            bool bFailed = false;
            string strErrors = "";
            int i;
            int j;
            bool bReturnValue;

            try
            {

                // Create Prospace application and get the reference to the Planograms object
                objPSApplication = new Application();

                objPSProjects = objPSApplication.Projects;

                // Opens a Space Planning Project
                objPSProject = objPSProjects.Open(strFilePath, null, null);
                objPSPlanograms = objPSProject.Planograms;

                // Number of indexes in planogramIndexes
                if (planogramIndexes != null)
                {
                    nIndexes = ((object[])planogramIndexes).GetUpperBound(0) + 1;
                }
                else
                {
                    // All objects in the collection
                    nIndexes = objPSPlanograms.Count;
                }

                // Number of fields in planogramFields
                if (planogramFields != null)
                {
                    // Number of fields plus one
                    nFields = ((object[])planogramFields).GetUpperBound(0) + 2;
                }
                else
                {
                    // All fields for Planogram object
                    nFields = objPSPlanograms.FieldIdCount;
                }

                // Re-dimensioning planogramData
                // The first upper bound should be the number of Planograms, and the second upper bound should be the number of fields plus one

                // If nStartIndex is greater than zero and index array is empty, then retrieve data for consecutive Planograms in the collection starting with the Planogram whose index is nStartIndex.
                if (nStartIndex > 0 && (planogramIndexes == null))
                {
                    planogramData = new object[nIndexes - nStartIndex + 1, nFields + 1];
                }
                else
                {
                    planogramData = new object[nIndexes + 1, nFields + 1];
                }

                // Retrieve data for Planograms whose indexes are in planogramIndexes, put each Planogram's object references in the 0 column
                if (planogramIndexes != null)
                {
                    for (i = 0; i <= nIndexes - 1; i++)
                    {
                        // Verify Planogram index available in planogramIndexes
                        if (Convert.ToInt32(((object[])planogramIndexes)[i]) < 0 || Convert.ToInt32(((object[])planogramIndexes)[i]) > objPSPlanograms.Count - 1)
                        {
                            throw new Exception("Planogram index " + Convert.ToInt32(((object[])planogramIndexes)[i]) + " is invalid.");
                        }

                        // Put each Planogram's object references in the 0 column like (1,0),(2,0)...
                        ((object[,])planogramData)[i + 1, 0] = objPSPlanograms.Item(Convert.ToInt32(((object[])planogramIndexes)[i]));
                    }
                }

                if (planogramFields != null)
                {
                    for (j = 0; j <= nFields - 2; j++)
                    {
                        // Indexes of the fields to be retrieved in consecutive cells of row zero like (0,2),(0,3)...
                        ((object[,])planogramData)[0, j + 2] = Convert.ToInt32(((object[])planogramFields)[j]);
                    }
                }

                if (planogramIndexes != null)
                {
                    readErrors = (object[,])objPSPlanograms.GetPlanogramFields(ref planogramData, null);
                }
                else
                {
                    // To retrieve data for consecutive Planograms in the collection starting with the Planogram whose index is nStartIndex.
                    readErrors = (object[,])objPSPlanograms.GetPlanogramFields(ref planogramData, nStartIndex);
                }

                // Verify data for the fields available in planogramFields (Selected fields)
                if (planogramFields != null)
                {
                    for (i = 1; i <= ((object[,])planogramData).GetUpperBound(0); i++)
                    {
                        for (j = 2; j <= ((object[,])planogramData).GetUpperBound(1); j++)
                        {
                            if (!Equals(((object[,])planogramData)[i, j], ((object[,])expectedPlanogramsData)[i, j]))
                            {
                                bFailed = true;
                                break;
                            }
                        }
                        if (bFailed == true)
                        {
                            break;
                        }
                    }
                }
                // Verify data for all fields (using PlanogramField property)
                else
                {
                    for (i = 1; i <= ((object[,])planogramData).GetUpperBound(0); i++)
                    {
                        if (nStartIndex > 0 && (planogramIndexes == null))
                        {
                            objPSPlanogram = objPSPlanograms.Item(nStartIndex + i - 1);

                        }
                        else if (nStartIndex == 0 && (planogramIndexes == null))
                        {
                            objPSPlanogram = objPSPlanograms.Item(i - 1);
                        }
                        else
                        {
                            objPSPlanogram = objPSPlanograms.Item(Convert.ToInt32(((object[])planogramIndexes)[i - 1]));
                        }

                        for (j = 2; j <= ((object[,])planogramData).GetUpperBound(1); j++)
                        {
                            if (!Equals(((object[,])planogramData)[i, j], objPSPlanogram.get_PlanogramFieldN(j - 2)))
                            {
                                bFailed = true;
                                break;
                            }
                        }
                        if (bFailed == true)
                        {
                            break;
                        }
                    }
                }

                // Check for Errors
                if (expectedErrors != null)
                {
                    if (readErrors != null)
                    {
                        for (i = 0; i <= ((object[])expectedErrors).GetUpperBound(0); i++)
                        {
                            // Check the Expected errors with actual errors
                            if (!Equals(readErrors[i, 2], ((object[])expectedErrors)[i]))
                            {
                                throw new Exception("Expected error " + ((object[])expectedErrors)[i] + " in row " + (i + 1) + " is not matching with actual error. " + "Actual error encountered is " + readErrors[i, 2] + "   " + SharedFunctions.ErrorDescription((int)readErrors[i, 2]));
                            }
                        }
                        bFailed = false;
                    }
                    else
                    {
                        throw new Exception("No error encountered in the process.");
                    }
                }
                else
                {
                    // Raise all errors encountered in the process if data is not matching
                    if (readErrors != null && bFailed == true)
                    {
                        for (i = 0; i <= readErrors.GetUpperBound(0); i++)
                        {
                            strErrors = strErrors + "In Cell(" + readErrors[i, 0] + "," + readErrors[i, 1] + ") : " + readErrors[i, 2] + "   " + SharedFunctions.ErrorDescription((int)readErrors[i, 2]) + "  ";
                        }
                        throw new Exception("Error encountered in the execution and the details are : " + strErrors);
                    }
                }

                if (bFailed == false)
                {
                    bReturnValue = true;
                }
                else
                {
                    bReturnValue = false;
                }
            }
            catch (Exception ex)
            {
                bReturnValue = false;
                // Log Error
                EventLog.WriteEntry("PlanogramsGetPlanogramFields : ", ex.Message.ToString(), EventLogEntryType.Error);
            }
            finally
            {
                // Clean up all the objects
                if (objPSPlanogram != null)
                {
                    Marshal.ReleaseComObject(objPSPlanogram);
                }
                objPSPlanogram = null;
                Marshal.ReleaseComObject(objPSPlanograms);
                objPSPlanograms = null;
                Marshal.ReleaseComObject(objPSProjects);
                objPSProjects = null;
                if (objPSProject != null)
                {
                    objPSProject.Close(null, null, null, null, null);
                }
                Marshal.ReleaseComObject(objPSProject);
                objPSProject = null;
                Marshal.ReleaseComObject(objPSApplication);
                objPSApplication = null;

                // Check to see if Space Planning exited
                // Terminate Space Planning
                SharedFunctions.KillProcess("ProSpace");
            }
            return bReturnValue;
        }


        static void Main(string[] args)
        {
            Program P = new Program();

            object test1 = null;
            object test2 = null;
            object test3 = null;
            object error = null;
 
            string file = @"C:\Program Files (x86)\JDA\Intactix\Samples\Space Planning Planograms\3D Sample - Cooler.psa";

            P.SindreTest(file, 0, test1, test2, test3, error);
        }
    }
}
