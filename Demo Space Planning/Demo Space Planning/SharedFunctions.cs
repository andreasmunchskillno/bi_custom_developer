﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;                   // Required for Process Kill code and EventLog
using System.Runtime.InteropServices;       // for DllImport
using System.IO;                            // File IO like Path


public class SharedFunctions
{
    // API
    [DllImport("kernel32.dll")]
    public static extern uint GetTickCount();
    [DllImport("user32.dll")]
    public static extern int GetWindowThreadProcessId(IntPtr hWnd, out int lpdwProcessId);
    [DllImport("kernel32.dll")]
    public static extern IntPtr OpenProcess(UInt32 dwDesiredAccess, bool bInheritHandle, UInt32 dwProcessId);
    [DllImport("kernel32.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool TerminateProcess(IntPtr hProcess, uint uExitCode);
    public const uint PROCESS_ALL_ACCESS = 0x000F0000 | 0x00100000 | 0xFFF;

    // Shared Functions
    public static string RandomFilename(string striFileExtension)
    {
        double nRandom = 0;
        nRandom = GetTickCount();
        return ("TMP" + Convert.ToString(nRandom) + "." + striFileExtension);
    }

    public static string GetTempFolder()
    {
        return Path.GetTempPath();
    }

    public static bool ProcessActive(string strProcessName)
    {
        Process[] proActiveProcess = Process.GetProcessesByName(strProcessName);
        if (proActiveProcess.Length != 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static void KillProcess(string strProcessName)
    {
        //Get a list of all running processes on the computer
        foreach (Process clsProcess in Process.GetProcesses())
        {
            //If any of the running processes match the currently running processes by using the StartsWith Method,
            //this prevents us from including the .EXE for the process we're looking for.
            //. Be sure to not add the .exe to the name you provide, i.e: PrSpace,
            //not ProSpace.EXE or false is always returned even if Space Planning is running
            if (clsProcess.ProcessName.StartsWith(strProcessName))
            {
                //since we found the process we now need to use the
                //Kill Method to kill the process. Remember, if you have
                //the process running more than once, say Space Planning open 4
                //times the loop the way it is now will close all 4,
                //if you want it to just close the first one it finds
                //then add a return; after the Kill
                clsProcess.Kill();
            }
        }
    }

    public static void RemoveProjectReadOnlyAttribute(string strFilename)
    {
        if (File.Exists(strFilename))
        {
            if ((File.GetAttributes(strFilename) == FileAttributes.ReadOnly))
            {
                File.SetAttributes(strFilename, FileAttributes.Normal);
            }
        }

    }

    public static string ErrorDescription(int nErrorNum)
    {
        string strErrorDescription = null;

        switch (nErrorNum)
        {

            case 5000:
                strErrorDescription = "CreateDenied";
                break;
            case 5001:
                strErrorDescription = "Delete Denied";
                break;
            case 5002:
                strErrorDescription = "ReadAccessDenied";
                break;
            case 5003:
                strErrorDescription = "WriteAccessDenied";
                break;
            case 5004:
                strErrorDescription = "Field Index Invalid";
                break;
            case 5005:
                strErrorDescription = "Field Type Invalid";
                break;
            case 5006:
                strErrorDescription = "FieldValueNotInRange";
                break;
            case 5007:
                strErrorDescription = "VariantArrayExpected";
                break;
            case 5008:
                strErrorDescription = "TwoDimensionsExpected";
                break;
            case 5009:
                strErrorDescription = "InvalidModifier";
                break;
            case 5010:
                strErrorDescription = "AttemptToOverwriteObjectReference";
                break;
            case 5011:
                strErrorDescription = "InvalidObjectReference";
                break;
            case 5012:
                strErrorDescription = "ObjectreferenceExpected";
                break;
            case 5013:
                strErrorDescription = "StartInvalid";
                break;
            case 5014:
                strErrorDescription = "ToomanyRows";
                break;
            case 5015:
                strErrorDescription = "ObjectReferenceandStartDefined";
                break;
            case 5016:
                strErrorDescription = "ObjectReferenceorEmptyExpected";
                break;
            case 5017:
                strErrorDescription = "FieldIdExpected";
                break;
            case 5018:
                strErrorDescription = "FieldIDTypeInvalid";
                break;
            case 5019:
                strErrorDescription = "FieldIdInvalid";
                break;
            case 5020:
                strErrorDescription = "ObjectReferenceIgnored";
                break;
            case 5021:
                strErrorDescription = "InsertBeforeIndexExpected";
                break;
            case 5022:
                strErrorDescription = "InsertAfterIndexExpected";
                break;
            case 5023:
                strErrorDescription = "NotEnoughRows";
                break;
            case 5024:
                strErrorDescription = "NotEnoughColumns";
                break;
            case 5025:
                strErrorDescription = "TooManyColumns";
                break;
            case 5026:
                strErrorDescription = "ObjectNotinCollection";
                break;
            case 5027:
                strErrorDescription = "InsertAfterIndexInvalid";
                break;
            case 5028:
                strErrorDescription = "InsertAfterindexTypeInvalid";
                break;
            case 5029:
                strErrorDescription = "CatalogCollectionObjectEmpty";
                break;
            case 5030:
                strErrorDescription = "FieldIdIgnored";
                break;
            case 5031:
                strErrorDescription = "InsertAfterIndexIgnored";
                break;
            case 5032:
                strErrorDescription = "InvalidPointIndex";
                break;
            case 5033:
                strErrorDescription = "PointIndexExpected";
                break;
            case 5034:
                strErrorDescription = "pointIndexandStartDefined";
                break;
            case 5035:
                strErrorDescription = "PointIndexorEmptyExpected";
                break;
            case 5036:
                strErrorDescription = "PointIndexIgnored";
                break;
            case 5037:
                strErrorDescription = "AttempttoOverwritePointIndex";
                break;
            case 5038:
                strErrorDescription = "OneDimensionExpected";
                break;
            case 5039:
                strErrorDescription = "FieldvalueMustbeUnique";
                break;
        }
        return strErrorDescription;
    }
    public static string GetPathPart(string strSource)
    {
        string returnValue;
        //Returns the folder from a full file name.
        returnValue = Path.GetDirectoryName(strSource);
        return returnValue;
    }
    public static string GetFilePart(string strSource)
    {
        string returnValue;
        //Returns the file name from a full file name.
        returnValue = Path.GetFileName(strSource);
        return returnValue;
    }
}


