﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.AnalysisServices;
using Microsoft.AnalysisServices.Hosting;
using Microsoft.AnalysisServices.AdomdClient;
using System.Text.RegularExpressions;

namespace EP_Build_MDX_With_All_Measures
{
    public partial class Form1 : Form
    {
        public string newServerConnection = "Data Source=epsql7;Catalog=StoreSales";
        string mdx = "";

        public Form1()
        {
            InitializeComponent();
        }

        public void loopCube()
        {
            string ConnStr = newServerConnection;
            Server OLAPServer = new Server();
            OLAPServer.Connect(ConnStr);

            Database OLAPDatabase = OLAPServer.Databases["StoreSales"];

            foreach (Cube OLAPCubex in OLAPDatabase.Cubes)
            {
                foreach (Microsoft.AnalysisServices.Measure m in OLAPCubex.AllMeasures)
                {
                    mdx += "[Measures].[" + m.Name + "]," + Environment.NewLine;
                }
            }
        }

        

        private void button1_Click(object sender, EventArgs e)
        {
            loopCube();

            textBox1.Text = mdx;

            Clipboard.SetData(DataFormats.Text, textBox1.Text);           
        }
    }
}
