﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace EP_Build_MDX_With_All_Measures
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
