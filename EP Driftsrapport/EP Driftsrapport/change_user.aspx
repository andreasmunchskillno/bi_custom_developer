﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="change_user.aspx.cs" Inherits="EP_Driftsrapport.change_user" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

     <h1>Bytt bruker</h1>
<p>Brukernavn (uten epint/ foran)
    <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
</p>
     <p>
         <asp:Label ID="lbInfo" runat="server"></asp:Label>
</p>
     <p>NB: Når man har logget seg på med en annen bruker og trykket &quot;OK&quot; vil endringen først tre i kraft etter at man brukt linkene i toppmenyen.<br />
         <br />
         For å logge seg på med en annen bruker må man lukke nettleseren og åpne den på nytt</p>
<p>
    <asp:Button ID="btnChangeUser" runat="server" Text="OK" OnClick="btnChangeUser_Click" />
</p>

    

</asp:Content>
