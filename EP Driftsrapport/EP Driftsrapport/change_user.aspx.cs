﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.DirectoryServices.AccountManagement;

namespace EP_Driftsrapport
{
    public partial class change_user : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public bool DoesUserExist(string userName)
        {
            using (var domainContext = new PrincipalContext(ContextType.Domain, "EPINT", "moss_Service", "Ms434!"))
            {
                using (var foundUser = UserPrincipal.FindByIdentity(domainContext, IdentityType.SamAccountName, userName))
                {
                    return foundUser != null;
                }
            }
        }

        protected void btnChangeUser_Click(object sender, EventArgs e)
        {
            Session["username"] = txtUserName.Text;

            if(DoesUserExist(txtUserName.Text))
                lbInfo.Text = "Logget på som annen bruker";
            else
                lbInfo.Text = "Feil: Brukeren finnes ikke i AD. Prøv på nytt.";
        }
    }
}