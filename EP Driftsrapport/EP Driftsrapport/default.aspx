﻿<%@ Page Title="" Language="C#"  MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="EP_Driftsrapport._default" %>
<%@ MasterType VirtualPath="~/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Legg til nytt besøk</h2>
    <table>

    <tr>
    <td>
    Besøk hos 
    <asp:DropDownList ID="dpStore" runat="server" CssClass="inn" AutoPostBack="True" OnSelectedIndexChanged="dpStore_SelectedIndexChanged">
    </asp:DropDownList>

        <asp:DropDownList ID="dpDates" runat="server" AutoPostBack="True" OnSelectedIndexChanged="dpDates_SelectedIndexChanged" Width="100px" CssClass="inn">
        </asp:DropDownList>
        
        <asp:Button ID="btnViewNumbers" runat="server" Text="Vis statistikk (salg/lønn)" OnClick="btnViewNumbers_Click" />

        </td>
        </tr>
     
    <tr><td>

                Personer tilstede&nbsp;
        <asp:TextBox ID="txtVisitedPersons" runat="server" Width="523px"></asp:TextBox>


        </td></tr>

        <tr>

            <td>

                Kommentar
                <br /><asp:TextBox ID="txtDescription" runat="server" Height="193px" Width="630px" TextMode="MultiLine"></asp:TextBox>

            </td>
        </tr>
        <tr>

            <td>
           <asp:Button ID="btnSaveVisit" runat="server" Text="Lagre" OnClick="btnSaveVisit_Click" />
            </td>
        </tr>
        <tr>

            <td>
                <asp:Label ID="lbInfo" runat="server"></asp:Label>

                <br />
                <asp:HyperLink ID="hpChangeUser" runat="server" NavigateUrl="~/change_user.aspx">Logg på som en annen bruker</asp:HyperLink>

            </td>
        </tr>
        
        </table>

            <asp:Literal ID="ltSales" runat="server"></asp:Literal>

        <asp:Literal ID="ltLedger" runat="server"></asp:Literal>

            
</asp:Content>
