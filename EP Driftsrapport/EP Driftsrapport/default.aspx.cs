﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EP_Driftsrapport;
using Microsoft.AnalysisServices.AdomdClient;
using System.Globalization;
using EP_Driftsrapport.db_driftsrapportTableAdapters;
using System.DirectoryServices.AccountManagement;

namespace EP_Driftsrapport
{
    public partial class _default : System.Web.UI.Page
    {
        /// <summary>
        /// Funksjon for å beregne når startdatoen i en uke var.
        /// Denne er laget slik pga hvordan ukeshierarkiet er bygget opp
        /// </summary>
        /// <param name="dayInWeek"></param>
        /// <param name="firstDay"></param>
        /// <returns></returns>
        public static DateTime GetFirstDayInWeek(DateTime dayInWeek, DayOfWeek firstDay)
        {
            int difference = ((int)dayInWeek.DayOfWeek) - ((int)firstDay);
            difference = (7 + difference) % 7;
            return dayInWeek.AddDays(-difference).Date;
        }

        public static int GetWeekNumber(DateTime dtPassed)
        {
            CultureInfo ciCurr = CultureInfo.CurrentCulture;
            int weekNum = ciCurr.Calendar.GetWeekOfYear(dtPassed, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            return weekNum;
        }

        private void viewLedgerData(string StoreNo)
        {
            try
            {
                string startDateOfWeek = String.Format("{0:yyyy-MM-dd}", GetFirstDayInWeek(DateTime.Now.AddDays(-15), DayOfWeek.Monday)).ToString();

                string MDX = @"SELECT NON EMPTY
                    {[Accounts EP].[Account Description].&[1575  LØNNSFORSKUDD ANSATTE], 
                    [Accounts EP].[Account Description].&[5996  NON RECURING LØNN], 
                    [Accounts EP].[Account Description].&[5720  ANNET LØNNSTILLEGG], 
                    [Accounts EP].[Account Description].&[5280  LØNNSTILSKUDD], 
                    [Accounts EP].[Account Description].&[5190  PÅLØPT LØNN], 
                    [Accounts EP].[Account Description].&[5100  PRESTASJONSLØNN], 
                    [Accounts EP].[Account Description].&[5010  FASTLØNN], 
                    [Accounts EP].[Account Description].&[5005  LØNN U FP/ PENSJONSDEKNING], 
                    [Accounts EP].[Account Description].&[5000  TIMELØNN], 
                    [Accounts EP].[Account Description].&[2935  AVSATT LØNN]} ON 1,
                    [Company Structure].[Store Number].&[" + StoreNo + @"] on 0
                    FROM [Ledger]
                    where
                    (
	                    [Measures].[Actual],
	                    [Calendar].[ISO 8601 Week].&[" + startDateOfWeek + @"T00:00:00]
                    )";

                int counter = 0;

                AdomdConnection conn = new AdomdConnection(@"Data Source=epsql7;UID=epint\dwh_readonly;PWD=TullMedTall;Catalog=Ledger;");
                conn.Open();
                AdomdCommand cmd = new AdomdCommand(MDX, conn);
                System.Data.DataTable objDatatable = new System.Data.DataTable();

                cmd.Connection = conn;
                cmd.CommandText = MDX;
                CellSet objCellSet = cmd.ExecuteCellSet();
                AdomdDataAdapter objDataAdapter = new AdomdDataAdapter(cmd);
                objDataAdapter.Fill(objDatatable);

                conn.Close();
                System.Text.StringBuilder str = new System.Text.StringBuilder();

                str.Append("<h3>Viser lønnstall uke " + GetWeekNumber(DateTime.Now.AddDays(-15)) + " for " + dpStore.SelectedItem.Text + "</h3>");

                str.Append("<table>");

                foreach (System.Data.DataRow row in objDatatable.Rows)
                {
                    str.Append("<tr>");
                    object[] rows = row.ItemArray;

                    foreach (object dRow in rows)
                    {
                        str.Append("<td>");

                        if (dRow is Double)
                        {
                            str.Append(String.Format("{0:0,0.0}", Convert.ToDouble(dRow)));
                        }
                        else if (dRow is int || dRow is System.Decimal)
                        {
                            str.Append(String.Format("{0:0,0}", Convert.ToInt32(dRow)));
                        }
                        else
                        {
                            str.Append(dRow.ToString());
                        }

                        str.Append("</td>");
                        counter++;
                    }

                    str.Append("</tr>");
                }

                str.Append("</table>");

                ltLedger.Text = str.ToString();
            }
            catch (Exception err)
            {
                ltLedger.Text = err.Message;
            }
        }

        private void viewSalesData()
        {
            string MDX = @"select {[Measures].[Net Amount Ex Tax], [Measures].[Margin], [Measures].[Shopping Cart], [Measures].[Customer Count], [Measures].[Budget]} on 1,
                [Store].[Store No].&[" + dpStore.SelectedItem.Text.Substring(0,3) + @"] on 0
                from StoreSales
                where (
	                [2 weeks ago]
                )";

            int counter = 0;

            AdomdConnection conn = new AdomdConnection(@"Data Source=epsql7;;UID=epint\dwh_readonly;PWD=TullMedTall;Catalog=StoreSales");
            conn.Open();
            AdomdCommand cmd = new AdomdCommand(MDX, conn);
            System.Data.DataTable objDatatable = new System.Data.DataTable();

            cmd.Connection = conn;
            cmd.CommandText = MDX;
            CellSet objCellSet = cmd.ExecuteCellSet();
            AdomdDataAdapter objDataAdapter = new AdomdDataAdapter(cmd);
            objDataAdapter.Fill(objDatatable);

            conn.Close();
            System.Text.StringBuilder str = new System.Text.StringBuilder();

            str.Append("<h3>Viser salgstall uke " + GetWeekNumber(DateTime.Now.AddDays(-14)) + " for " + dpStore.SelectedItem.Text + "</h3>");

            str.Append("<table>");

            foreach (System.Data.DataRow row in objDatatable.Rows)
            {
                str.Append("<tr>");
                object[] rows = row.ItemArray;

                foreach (object dRow in rows)
                {
                    str.Append("<td>");

                    if (dRow is Double)
                    {
                        //Må hardkode inn hvilken plass desimal er. Siden disse må ganges med 100.
                        if (counter == 3)
                        {
                            double percent = Convert.ToDouble(dRow) * 100;
                            str.Append(String.Format("{0:0,0.0}", percent));
                        }
                        else
                        {
                            str.Append(String.Format("{0:0,0.0}", Convert.ToDouble(dRow)));
                        }
                    }
                    else if (dRow is int || dRow is System.Decimal)
                    {
                        str.Append(String.Format("{0:0,0}", Convert.ToInt32(dRow)));
                    }
                    else
                    {
                        str.Append(dRow.ToString());
                    }

                    str.Append("</td>");
                    counter++;
                }

                str.Append("</tr>");
            }

            str.Append("</table>");

            ltSales.Text = str.ToString();
        }

        public static string[] GetGroupNames(string userName)
        {
            List<string> result = new List<string>();

            using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, "EPINT", "moss_Service", "Ms434!"))
            {
                using (PrincipalSearchResult<Principal> src = UserPrincipal.FindByIdentity(pc, userName).GetGroups(pc))
                {
                    src.ToList().ForEach(sr => result.Add(sr.SamAccountName));
                }
            }

            return result.ToArray();
        }

        public string getUserName()
        {
            string userName = @Environment.UserName;
            //string userName = "halsyl";
            //string userName = "BAKKAT"; //Bruker uten tilgang
            //string userName = "test_butikk"; //MOSS

            //string userName = "myhtor"; //Tilgang til 2 butikker

            if (Session["username"] != null)
                userName = Session["username"].ToString();
            else
                Session["username"] = userName;

            return userName;
        }

        public void checkLogin()
        {
            string[] groups = GetGroupNames(getUserName());
            string whereStatement = "";

            EP_Driftsrapport.db_driftsrapportTableAdapters.LoginsTableAdapter loginAdapter = new LoginsTableAdapter();
            EP_Driftsrapport.db_driftsrapport.LoginsDataTable loginTable = new db_driftsrapport.LoginsDataTable();

            foreach (string t in groups)
            {
                whereStatement = t + "','" + whereStatement;
            }

            whereStatement = "('" + whereStatement.Substring(0, whereStatement.Length - 3) + "')";

            loginAdapter.checkLogin(loginTable, getUserName(), @whereStatement);

            string login = loginTable.Rows[0][1].ToString();

            if (loginTable.Rows[0][2].ToString() == "3")
            {
                Session["admin"] = 1;
            }
            else
            {
                Session["admin"] = 0;
            }

            Session["id"] = login;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                checkLogin();

                string login = Session["id"].ToString();
               
                if (login == "0")
                    Response.Redirect("failure.aspx");

                if (Session["admin"].ToString() == "1")
                    hpChangeUser.Visible = true;
                else
                    hpChangeUser.Visible = false;

                if (!Page.IsPostBack)
                {
                    EP_Driftsrapport.db_driftsrapportTableAdapters.ShopsTableAdapter shopAdapter = new db_driftsrapportTableAdapters.ShopsTableAdapter();
                    
                    //Hent ut liste med alle butikker. Vise frem denne i drowdown.
                    dpStore.DataSource = shopAdapter.getShops(Convert.ToInt32(Session["id"].ToString()), getUserName());
                    dpStore.DataTextField = "FormattedStore";
                    dpStore.DataValueField = "StoreNo";
                    
                    dpStore.DataBind();
                   
                    if (Session["StoreNo"] != null)
                    {
                        dpStore.SelectedValue = Session["StoreNo"].ToString();

                        fillDates();

                        dpDates.SelectedValue = Session["VisitName"].ToString();

                        getVisitDetails();
                    }

                    //Fjerner muligheten for butikker å skrive inn noe i tekstbokser og å trykke på lagre-knappen.
                    if (Session["id"].ToString() != "-1")
                    {
                        txtDescription.Enabled = false;
                        txtVisitedPersons.Enabled = false;

                        btnSaveVisit.Text = "Hent besøk";
                    }
                }
            }
            catch (Exception err)
            {
                lbInfo.Text = "Err 1: " + err.Message;
            }
        }

        protected void btnSaveVisit_Click(object sender, EventArgs e)
        {
            //Sjekk om det finnes noe på denne fra før.
            EP_Driftsrapport.db_driftsrapportTableAdapters.VisitTableAdapter visitA = new db_driftsrapportTableAdapters.VisitTableAdapter();
           
            if (dpDates.SelectedValue != "- Velg dato -")
            {
                EP_Driftsrapport.db_driftsrapport.VisitDataTable tbl = visitA.getVisitByStoreIdAndDate2(dpDates.SelectedValue, dpStore.SelectedValue);

                //Kun lov å lagre dersom det ikke er en butikk (bs) som er inne
                if (Session["id"].ToString() == "-1")
                {
                    if (tbl.Rows.Count == 0)
                    {
                        //Lagre ny nytt besøk
                        if (visitA.Insert(Convert.ToDateTime(dpDates.SelectedItem.Value), txtDescription.Text, dpStore.SelectedValue, txtVisitedPersons.Text, @Environment.UserName) != 0)
                        {
                            lbInfo.Text = "Lagret nytt besøk";

                            tbl = visitA.getVisitByStoreIdAndDate2(dpDates.SelectedItem.Value, dpStore.SelectedValue);

                            Session["StoreName"] = dpStore.SelectedItem.Text;
                            Session["VisitName"] = dpDates.SelectedItem;
                            Session["StoreNo"] = dpStore.SelectedItem.Value;
                            Session["VisitId"] = tbl.Rows[0]["PK_VisitId"].ToString();
                        }
                        else
                        {
                            lbInfo.Text = "Feil under lagring av besøk. Forsøk igjen";
                        }
                    }
                    else
                    {
                        EP_Driftsrapport.db_driftsrapportTableAdapters.QueriesTableAdapter query = new db_driftsrapportTableAdapters.QueriesTableAdapter();
                        query.updateVisitDescriptionByVisitId(txtDescription.Text, dpStore.SelectedItem.Value, Convert.ToDateTime(dpDates.SelectedValue), txtVisitedPersons.Text);

                        lbInfo.Text = "Oppdaterte besøket";
                    }
                }
                else
                {
                    lbInfo.Text = "Hentet ut detaljer om besøk";

                    tbl = visitA.getVisitByStoreIdAndDate2(dpDates.SelectedItem.Value, dpStore.SelectedValue);

                    Session["StoreName"] = dpStore.SelectedItem.Text;
                    Session["VisitName"] = dpDates.SelectedItem;
                    Session["StoreNo"] = dpStore.SelectedItem.Value;
                    Session["VisitId"] = tbl.Rows[0]["PK_VisitId"].ToString();
                }
            }
            else
            {
                lbInfo.Text = "Vennligst velg dato først.";
            }
        }

        private void fillDates()
        {
            dpDates.Items.Clear();

            EP_Driftsrapport.db_driftsrapportTableAdapters.VisitTableAdapter visit = new db_driftsrapportTableAdapters.VisitTableAdapter();
            EP_Driftsrapport.db_driftsrapport.VisitDataTable tVisit = new db_driftsrapport.VisitDataTable();

            tVisit = visit.GetDataBy(dpStore.SelectedValue.ToString(), Convert.ToInt32(Session["id"]));

            dpDates.Items.Add(new ListItem("- Velg dato -", "- Velg dato -"));

            foreach (EP_Driftsrapport.db_driftsrapport.VisitRow r in tVisit.Rows)
            {
                dpDates.Items.Add(new ListItem(r.Date.ToShortDateString(), r.Date.ToShortDateString()));
            }
        }

        protected void btnViewNumbers_Click(object sender, EventArgs e)
        {
            try
            {
                viewSalesData();
                viewLedgerData(dpStore.SelectedValue);
            }
            catch (Exception err)
            {
                lbInfo.Text = err.Message;
            }
        }

        protected void dpStore_SelectedIndexChanged(object sender, EventArgs e)
        {
            //-999 er verdien på "- Velg butikk -"
            if (dpStore.SelectedValue != "-999")
            {
                fillDates();

                //Dersom det ikke kommer opp annet enn "- Velg dato -" i dropdown med dato. Så ikke endre session-variabel.
                //BS'ene kan komme til å få opp butikker uten besøk. Og de kan ikke legge til nye besøk selv.
                if(dpDates.Items.Count != 1)
                    Session["StoreName"] = dpStore.SelectedItem.Text;

                txtVisitedPersons.Text = "";
                txtDescription.Text = "";
            }
        }

        private void getVisitDetails()
        {
            if (dpDates.SelectedValue != "- Velg dato -")
            {
                txtDescription.Text = "";
                txtVisitedPersons.Text = "";

                //Sjekk om det finnes noe på denne fra før.
                EP_Driftsrapport.db_driftsrapportTableAdapters.VisitTableAdapter visitA = new db_driftsrapportTableAdapters.VisitTableAdapter();
                EP_Driftsrapport.db_driftsrapport.VisitDataTable tbl = visitA.getVisitByStoreIdAndDate2(dpDates.SelectedValue, dpStore.SelectedValue);

                if (tbl.Rows.Count != 0)
                {
                    txtDescription.Text = tbl.Rows[0]["Description"].ToString();
                    txtVisitedPersons.Text = tbl.Rows[0]["VisitedPersons"].ToString();

                    //Lagre i session-variabler
                    Session["StoreName"] = dpStore.SelectedItem.Text;
                    Session["VisitName"] = dpDates.SelectedItem;
                    Session["StoreNo"] = dpStore.SelectedItem.Value;
                    Session["VisitId"] = tbl.Rows[0]["PK_VisitId"].ToString();

                    lbInfo.Text = "Hentet ut besøk";
                }
            }
        }

        protected void dpDates_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dpDates.SelectedValue != "- Velg dato -")
            {
                getVisitDetails();
            }
        }
    }
}