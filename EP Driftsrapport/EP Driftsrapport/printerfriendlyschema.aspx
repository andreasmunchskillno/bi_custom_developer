﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="printerfriendlyschema.aspx.cs" Inherits="EP_Driftsrapport.printerfriendlyschema" %>

<%@ Register assembly="EP Driftsrapport" namespace="EP_Driftsrapport" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<script type="text/javascript">
    window.print();
</script>
    

        <h3>Besøksinformasjon</h3>
    <p>
        <asp:Label ID="lbInfo" runat="server"></asp:Label>
    </p>

    <table style="width: 500px">
        <tr>
            <td><asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Lagre" /></td>
            <td align="right"><asp:Label ID="lbTotal" runat="server" style="text-align: right" Font-Bold="True"></asp:Label></td>
        </tr>

    </table>

        <asp:Literal ID="Literal1" runat="server"></asp:Literal>
    <br />
    <asp:Button ID="btnSave2" runat="server" OnClick="btnSave2_Click" Text="Lagre" />
    <br />
    <asp:Label ID="lbStatus" runat="server"></asp:Label>
</asp:Content>
