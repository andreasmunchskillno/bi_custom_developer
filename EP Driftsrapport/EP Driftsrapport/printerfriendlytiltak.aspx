﻿<%@ Page Title="" Culture="auto" UICulture="auto" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="printerfriendlytiltak.aspx.cs" Inherits="EP_Driftsrapport.printerfrienlytiltak" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit"%>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

    <asp:ToolkitScriptManager EnableScriptGlobalization="true" EnableScriptLocalization="true" ID="ToolkitScriptManager1" runat="Server" />

    <h2>Tiltak</h2>
    <p>
                <asp:Label ID="lbInfo" runat="server"></asp:Label>
    </p>

    <script type="text/javascript">
        window.print();
</script>

            <asp:Panel ID="pnGrid" runat="server">

                <h3>Liste over mål og tiltak som ikke er fullført</h3>

            <asp:GridView ID="gdActivities" runat="server" AutoGenerateColumns="False" DataKeyNames="PK_ActivitiesId" DataSourceID="sqlActivities" Width="100%">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:BoundField AccessibleHeaderText="Navn" DataField="Name" HeaderText="Navn" SortExpression="Name" />
                    <asp:BoundField DataField="PK_ActivitiesId" HeaderText="PK_ActivitiesId" InsertVisible="False" ReadOnly="True" SortExpression="PK_ActivitiesId" Visible="False" />
                    <asp:BoundField DataField="FK_ButikkNr" HeaderText="FK_ButikkNr" SortExpression="FK_ButikkNr" Visible="False" />
                    <asp:BoundField DataField="Goal" HeaderText="Mål" SortExpression="Goal" />
                    <asp:BoundField DataField="Description" HeaderText="Beskrivelse" SortExpression="Description" />
                    <asp:BoundField DataField="Responsible" HeaderText="Ansvarlig" SortExpression="Responsible" >
                    <ItemStyle Width="70px" />
                    </asp:BoundField>

                     <asp:BoundField DataField="DeadlineDate" HeaderText="Frist" SortExpression="DeadlineDate" DataFormatString="{0:dd/MM/yyyy}" >
                    <ItemStyle Width="70px" />
                    </asp:BoundField>

                    <asp:CheckBoxField DataField="Completed" HeaderText="Ferdig" SortExpression="Completed" >
                    <ItemStyle Width="40px" />
                    </asp:CheckBoxField>
                    <asp:BoundField DataField="CreatedDate" HeaderText="CreatedDate" SortExpression="CreatedDate" Visible="False" />
                    <asp:BoundField DataField="ModifiedDate" HeaderText="ModifiedDate" SortExpression="ModifiedDate" Visible="False" />
                </Columns>
                <HeaderStyle BackColor="#FFFFE7" ForeColor="Black" />
                <RowStyle VerticalAlign="Top" />
            </asp:GridView>

                    </asp:Panel>

        <asp:Label ID="lbStatus" runat="server"></asp:Label>
            

            <asp:SqlDataSource ID="sqlActivities" runat="server" ConnectionString="<%$ ConnectionStrings:App_DriftsrapportConnectionString %>" DeleteCommand="DELETE FROM [Activitites] WHERE [PK_ActivitiesId] = @PK_ActivitiesId" InsertCommand="INSERT INTO [Activitites] ([FK_ButikkNr], [Goal], [Description], [Responsible], [Completed], [CreatedDate], [ModifiedDate]) VALUES (@FK_ButikkNr, @Goal, @Description, @Responsible, @Completed, @CreatedDate, @ModifiedDate)" SelectCommand="SELECT PK_ActivitiesId, FK_ButikkNr, Goal, Description, Responsible, Completed, CreatedDate, ModifiedDate, Name, DeadlineDate FROM Activitites WHERE (FK_ButikkNr = @FK_ButikkNr) and completed = 0
ORDER BY Completed, CreatedDate" UpdateCommand="UPDATE [Activitites] SET [FK_ButikkNr] = @FK_ButikkNr, [Goal] = @Goal, [Description] = @Description, [Responsible] = @Responsible, [Completed] = @Completed, [CreatedDate] = @CreatedDate, [ModifiedDate] = @ModifiedDate WHERE [PK_ActivitiesId] = @PK_ActivitiesId">
                <DeleteParameters>
                    <asp:Parameter Name="PK_ActivitiesId" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="FK_ButikkNr" Type="String" />
                    <asp:Parameter Name="Goal" Type="String" />
                    <asp:Parameter Name="Description" Type="String" />
                    <asp:Parameter Name="Responsible" Type="String" />
                    <asp:Parameter Name="Completed" Type="Boolean" />
                    <asp:Parameter Name="CreatedDate" Type="DateTime" />
                    <asp:Parameter Name="ModifiedDate" Type="DateTime" />
                </InsertParameters>
                <SelectParameters>
                    <asp:SessionParameter Name="FK_ButikkNr" SessionField="StoreNo" Type="String" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="FK_ButikkNr" Type="String" />
                    <asp:Parameter Name="Goal" Type="String" />
                    <asp:Parameter Name="Description" Type="String" />
                    <asp:Parameter Name="Responsible" Type="String" />
                    <asp:Parameter Name="Completed" Type="Boolean" />
                    <asp:Parameter Name="CreatedDate" Type="DateTime" />
                    <asp:Parameter Name="ModifiedDate" Type="DateTime" />
                    <asp:Parameter Name="PK_ActivitiesId" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>

   
</asp:Content>
