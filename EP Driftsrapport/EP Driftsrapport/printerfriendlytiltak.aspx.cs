﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EP_Driftsrapport;

namespace EP_Driftsrapport
{
    public partial class printerfrienlytiltak : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Sjekk av tilgang
            string login = Session["id"].ToString();

            if (login == "0")
                Response.Redirect("failure.aspx");

            lbInfo.Text = "Besøk hos " + Session["StoreName"] + " den " + Session["VisitName"] + ". Dersom du ønsker å endre butikk og/eller dato kan du gå til <a href=\"default.aspx\">besøkssiden</a>";

       }
    }
}