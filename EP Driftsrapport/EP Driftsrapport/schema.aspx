﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="schema.aspx.cs" Inherits="EP_Driftsrapport.Default" %>

<%@ Register assembly="EP Driftsrapport" namespace="EP_Driftsrapport" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

    <script lang="JavaScript" type="text/javascript">
        /*
            Kode for å lagre expand-collapse i cookies
        */
        function doMenu(item, Saved) {
            obj = document.getElementById(item);
            col = document.getElementById("x" + item);

            if (!Saved) {
                if (obj.style.display == "none") {
                    obj.style.display = "block";
                    //col.innerHTML = "[-]";
                    setCookie(item, "block", 2); //Set expiration in days
                }
                else {
                    obj.style.display = "none";
                    //col.innerHTML = "[+]";
                    setCookie(item, "none", 2); //Set expiration in days
                }
            }
            else {
                //Values saved in cookie
                if (Saved == 'none') {
                    obj.style.display = "none";
                    //col.innerHTML = "[+]";
                }
                else {
                    obj.style.display = "block";
                    //col.innerHTML = "[-]";
                }
            }
            //setCookie(id, displayValue, 30); //Set expiration in days
        }

        function setCookie(name, value, expiredays) {
            if (expiredays == null) { expiredays = 0; }

            var expireDate = new Date();
            expireDate.setDate(expireDate.getDate() + expiredays);

            var cookieVal = name + '=' + escape(value) + ';expires=' + expireDate.toGMTString();
            document.cookie = cookieVal;
            return;
        }

        function getCookie(searchName) {
            if (document.cookie.length > 0) {
                var nameValuePair, cookieName, cookieValue
                var pairs = document.cookie.split(';');
                for (var i = 0; i < pairs.length; i++) {
                    nameValuePair = pairs[i].split('=');
                    cookieName = nameValuePair[0].replace(/^\s+|\s+$/g, '');
                    cookieValue = nameValuePair[1].replace(/^\s+|\s+$/g, '');
                    if (cookieName == searchName) { return cookieValue; }
                }
            }
            return false;
        }
</script>

        <h3>Besøksinformasjon</h3>
    <p>
        <asp:Label ID="lbInfo" runat="server"></asp:Label>
    </p>

    <table style="width: 600px">
        <tr>
            <td><asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Lagre" /></td>
            <td align="right"><asp:Label ID="lbTotal" runat="server" style="text-align: right" Font-Bold="True"></asp:Label>
                </td>
            <td align="right">
                <a href="printerfriendlyschema.aspx" target="_blank">Utskriftsvennlig versjon</a>
            </td>
        </tr>

    </table>

        <asp:Literal ID="Literal1" runat="server"></asp:Literal>
    <br />
    <asp:Button ID="btnSave2" runat="server" OnClick="btnSave2_Click" Text="Lagre" />
    <br />
    <asp:Label ID="lbStatus" runat="server"></asp:Label>
</asp:Content>
