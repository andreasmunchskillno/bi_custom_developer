﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EP_Driftsrapport.db_driftsrapportTableAdapters;
using System.Text;
using System.Collections;

namespace EP_Driftsrapport
{
    public partial class Default : System.Web.UI.Page
    {
        private void generateSchema()
        {
            if (Session["VisitName"] != null)
            {
                lbInfo.Text = "Besøk hos " + Session["StoreName"] + " den " + Session["VisitName"] + ". Dersom du ønsker å endre butikk og/eller dato kan du gå til <a href=\"default.aspx\">besøkssiden</a>";

                int visitId = Convert.ToInt32(Session["VisitId"]);
                int totalQuestions = 0;
                int totalQuestionsAnswered = 0;

                ArrayList tmp = new ArrayList();

                ArrayList questionIds = new ArrayList();
                StringBuilder str = new StringBuilder();
                StringBuilder strJavaScript = new StringBuilder();

                str.Append("<table border=1>");

                EP_Driftsrapport.db_driftsrapportTableAdapters.CategoryTableAdapter adapter = new CategoryTableAdapter();
                EP_Driftsrapport.db_driftsrapportTableAdapters.QuestionTableAdapter qAdapter = new QuestionTableAdapter();
                EP_Driftsrapport.db_driftsrapportTableAdapters.AnswerTableAdapter aAdapter = new AnswerTableAdapter();

                EP_Driftsrapport.db_driftsrapport.QuestionDataTable questions = qAdapter.GetData();
                EP_Driftsrapport.db_driftsrapport.CategoryDataTable table = adapter.GetData();
                EP_Driftsrapport.db_driftsrapport.AnswerDataTable answers = aAdapter.GetDataBy(visitId);

                foreach (EP_Driftsrapport.db_driftsrapport.CategoryRow row in table)
                {
                    int checkedAnswers = 0;

                    System.Data.DataRow[] questionRow = questions.Select("FK_CategoryId = " + row.PK_CategoryId, "FK_CategoryId");

                    str.Append("<tr class=\"CategoryHeader\"><td align=\"left\" valign=\"top\"><a href=\"JavaScript:doMenu('" + Session["StoreNo"].ToString() + "_" + row.PK_CategoryId + "');\" id=x" + Session["StoreNo"].ToString() + "_" + row.PK_CategoryId + ">");

                    str.Append(row.Description + " (score: $$number$$ / @@number@@)</a></td></td><tr><td>");

                    str.Append("<table border=0 style=\"display:none;\" id=" + Session["StoreNo"].ToString() + "_" + row.PK_CategoryId + ">");

                    strJavaScript.Append("doMenu('" + Session["StoreNo"].ToString() + "_" + row.PK_CategoryId + "', getCookie('" + Session["StoreNo"].ToString() + "_" + row.PK_CategoryId + "'));\n\n");

                    foreach (System.Data.DataRow r in questionRow)
                    {
                        str.Append("<tr onmouseover=\"this.className='trLineWhite';\" onmouseout=\"this.className='trLine';\" class=\"trLine\">");
                        str.Append("<td width=\"180px\">" + r["Description"] + "</td>");
                        str.Append("<td>");

                        if (answers.Select("Value=1 AND FK_Question=" + r["PK_QuestionId"] + " AND FK_VisitId=" + visitId, "FK_Question").Length == 1)
                        {
                            str.Append("<input type=\"checkbox\" checked name=\"cm_" + r["PK_QuestionId"] + "\" value=\"" + r["PK_QuestionId"] + "\">");
                            checkedAnswers++;
                            totalQuestionsAnswered++;
                        }
                        else
                        {
                            str.Append("<input type=\"checkbox\" name=\"cm_" + r["PK_QuestionId"] + "\" value=\"" + r["PK_QuestionId"] + "\">");
                        }

                        totalQuestions++;
                        str.Append("</td>");
                        str.Append("<td>" + r["Long_Description"] + "</td></tr>");

                        //Lagre ID'er i tabell. Som igjen lagres i ViewState
                        questionIds.Add(r["PK_QuestionId"]);
                    }

                    //Legger inn faktisk verdier.
                    str.Replace("@@number@@", questionRow.Length.ToString());
                    str.Replace("$$number$$", checkedAnswers.ToString());
                    str.Append("</table>");
                }

                str.Append("</td></tr></table>");

                str.Append("<script type='text/javascript'>" + strJavaScript.ToString() + "</script>");

                Literal1.Text = str.ToString();

                lbTotal.Text = "Total " + totalQuestionsAnswered.ToString() + " / " + totalQuestions.ToString() + ".";

                ViewState["QuestionList"] = questionIds;
            }
            else
            {
                lbInfo.Text = "Vennligst velg butikk og dato først. Gå til besøkssiden <a href=\"default.aspx\">her</a>";
                btnSave.Visible = false;
                btnSave2.Visible = false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Sjekk av tilgang
            string login = Session["id"].ToString(); 

            if (login == "0")
                Response.Redirect("failure.aspx");

            //Dersom login er noe annet enn -1 har man kun tilgang til en butikk.
            if (login != "-1")
            {
                btnSave.Enabled = false;
                btnSave2.Enabled = false;
            }

            if (!Page.IsPostBack)
            {
                generateSchema();
            }
        }

        private void SaveAnswers()
        {
            ArrayList questions = (ArrayList)ViewState["QuestionList"];
            StringBuilder str = new StringBuilder();

            string shopNumber = Session["StoreNo"].ToString();
            int visit = Convert.ToInt32(Session["VisitId"]);

            str.Append("<answers>");

            foreach (int t in questions)
            {
                if (Request.Form["cm_" + t.ToString()] != null)
                    str.Append("<items><question>" + t + "</question><answer>1</answer><shop>" + shopNumber + "</shop><visit>" + visit + "</visit></items>");
                else
                    str.Append("<items><question>" + t + "</question><answer>0</answer><shop>" + shopNumber + "</shop><visit>" + visit + "</visit></items>");
            }

            str.Append("</answers>");

            EP_Driftsrapport.db_driftsrapportTableAdapters.QueriesTableAdapter q = new QueriesTableAdapter();

            //Fjern radene som finnes fra før
            q.deleteVisitedData(visit);

            //Send inn XML med rader
            if (Convert.ToInt32(q.insertAnswers(str.ToString())) == 0)
            {
                lbStatus.Text = "Lagret";
            }
            else
            {
                lbStatus.Text = "Feil under lagring";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveAnswers();
            generateSchema();
        }

        protected void btnSave2_Click(object sender, EventArgs e)
        {
            SaveAnswers();
            generateSchema();
        }
    }
}