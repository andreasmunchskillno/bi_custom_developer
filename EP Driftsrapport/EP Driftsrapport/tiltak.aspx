﻿<%@ Page Title="" Culture="auto" UICulture="auto" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="tiltak.aspx.cs" Inherits="EP_Driftsrapport.tiltak" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit"%>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

    <asp:ToolkitScriptManager EnableScriptGlobalization="true" EnableScriptLocalization="true" ID="ToolkitScriptManager1" runat="Server" />

    <h2>Tiltak</h2>
    <p>
                <asp:Label ID="lbInfo" runat="server"></asp:Label>
    </p>

            <asp:Panel ID="pnGrid" runat="server">

        <table style="width: 600px">
        <tr>
            <td><h3>Liste over mål og tiltak</h3></td>
           
            <td align="right">
                <a href="printerfriendlytiltak.aspx" target="_blank">Utskriftsvennlig versjon</a>
            </td>
        </tr>
        </table>
            <asp:GridView ID="gdActivities" runat="server" AutoGenerateColumns="False" DataKeyNames="PK_ActivitiesId" DataSourceID="sqlActivities" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" Width="700px" AllowPaging="True" AllowSorting="True" OnRowDataBound="gdActivities_RowDataBound" PageSize="5">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Select" Text="Rediger"></asp:LinkButton>
                            <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Delete" Text="Slett"></asp:LinkButton>
                        </ItemTemplate>
                        <ItemStyle Width="130px" />
                    </asp:TemplateField>
                    <asp:BoundField AccessibleHeaderText="Navn" DataField="Name" HeaderText="Navn" SortExpression="Name" />
                    <asp:BoundField DataField="PK_ActivitiesId" HeaderText="PK_ActivitiesId" InsertVisible="False" ReadOnly="True" SortExpression="PK_ActivitiesId" Visible="False" />
                    <asp:BoundField DataField="FK_ButikkNr" HeaderText="FK_ButikkNr" SortExpression="FK_ButikkNr" Visible="False" />
                    <asp:BoundField DataField="Goal" HeaderText="Mål" SortExpression="Goal" Visible="False" />
                    <asp:BoundField DataField="Description" HeaderText="Beskrivelse" SortExpression="Description" Visible="False" />
                    <asp:BoundField DataField="Responsible" HeaderText="Ansvarlig" SortExpression="Responsible" >
                    <ItemStyle Width="200px" />
                    </asp:BoundField>

                     <asp:BoundField DataField="DeadlineDate" HeaderText="Frist" SortExpression="DeadlineDate" DataFormatString="{0:dd/MM/yyyy}" >
                    <ItemStyle Width="200px" />
                    </asp:BoundField>

                    <asp:CheckBoxField DataField="Completed" HeaderText="Ferdig" SortExpression="Completed" >
                    <ItemStyle Width="50px" />
                    </asp:CheckBoxField>
                    <asp:BoundField DataField="CreatedDate" HeaderText="CreatedDate" SortExpression="CreatedDate" Visible="False" />
                    <asp:BoundField DataField="ModifiedDate" HeaderText="ModifiedDate" SortExpression="ModifiedDate" Visible="False" />
                </Columns>
                <HeaderStyle BackColor="#FFFFE7" ForeColor="Black" />
            </asp:GridView>

                    </asp:Panel>

    <asp:Panel ID="pnForm" runat="server">

    <h3>Rediger</h3>
    
    <table>
        <tr>
            <td valign="top">Kort navn</td>
            <td>
                <asp:TextBox ID="txtName" runat="server" Width="613px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtName" ErrorMessage="Navn må fylles ut" style="color: #FF0000; font-weight: 700"></asp:RequiredFieldValidator>
            </td>
        </tr>

        <tr>
            <td valign="top">Kommentar</td>
            <td>
                <asp:TextBox ID="txtGoal" runat="server" Height="55px" TextMode="MultiLine" Width="613px"></asp:TextBox>
            </td>
        </tr>

         <tr>
            <td valign="top">Tiltak</td>
            <td valign="top">
                <asp:TextBox ID="txtDescription" runat="server" Height="51px" TextMode="MultiLine" Width="613px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtDescription" ErrorMessage="Tiltak må fylles ut" style="color: #FF0000; font-weight: 700"></asp:RequiredFieldValidator>
             </td>
        </tr>

         <tr>
            <td valign="top">Ansvarlig</td>
            <td>
                <asp:TextBox ID="txtResponsible" runat="server" Width="613px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtResponsible" ErrorMessage="Ansvarlig må fylles ut" style="color: #FF0000; font-weight: 700"></asp:RequiredFieldValidator>
             </td>
        </tr>

         <tr>
             <td valign="top">Frist</td>
             <td>
                 <asp:TextBox ID="txtDeadLine" runat="server" Width="580px"></asp:TextBox>

                 <ajaxToolkit:CalendarExtender runat="server"
                    TargetControlID="txtDeadLine"
                    CssClass="Calendar"
                    Format="dd.MM.yyyy"
                     FirstDayOfWeek = "Monday"
                    PopupButtonID="imgCal" />

                 <asp:ImageButton ID="imgCal" runat="server" ImageUrl="~/Images/Calendar_scheduleHS.png" CausesValidation="False" />

                 <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDeadLine" ErrorMessage="Frist må fylles ut" Font-Bold="True" ForeColor="Red"></asp:RequiredFieldValidator>
                 <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtDeadLine" ErrorMessage="Ugyldig format (bruk dd.mm.yyyy)" Font-Bold="True" ForeColor="Red" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
             </td>
        </tr>

         <tr>
            <td>Ferdig</td>
            <td>
                <asp:CheckBox ID="cbCompleted" runat="server" />
             </td>
        </tr>

    </table>

            <br />
    <asp:Button ID="btnUpdate" runat="server" OnClick="Update_Click" Text="Oppdater" />
    <asp:Button ID="btnSaveNew" runat="server" Text="Lagre ny" OnClick="btnSaveNew_Click" />
            <br />
            </asp:Panel>

        <asp:Label ID="lbStatus" runat="server"></asp:Label>
            

            <asp:SqlDataSource ID="sqlActivities" runat="server" ConnectionString="<%$ ConnectionStrings:App_DriftsrapportConnectionString %>" DeleteCommand="DELETE FROM [Activitites] WHERE [PK_ActivitiesId] = @PK_ActivitiesId" InsertCommand="INSERT INTO [Activitites] ([FK_ButikkNr], [Goal], [Description], [Responsible], [Completed], [CreatedDate], [ModifiedDate]) VALUES (@FK_ButikkNr, @Goal, @Description, @Responsible, @Completed, @CreatedDate, @ModifiedDate)" SelectCommand="SELECT PK_ActivitiesId, FK_ButikkNr, Goal, Description, Responsible, Completed, CreatedDate, ModifiedDate, Name, DeadlineDate FROM Activitites WHERE (FK_ButikkNr = @FK_ButikkNr) ORDER BY Completed, CreatedDate" UpdateCommand="UPDATE [Activitites] SET [FK_ButikkNr] = @FK_ButikkNr, [Goal] = @Goal, [Description] = @Description, [Responsible] = @Responsible, [Completed] = @Completed, [CreatedDate] = @CreatedDate, [ModifiedDate] = @ModifiedDate WHERE [PK_ActivitiesId] = @PK_ActivitiesId">
                <DeleteParameters>
                    <asp:Parameter Name="PK_ActivitiesId" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="FK_ButikkNr" Type="String" />
                    <asp:Parameter Name="Goal" Type="String" />
                    <asp:Parameter Name="Description" Type="String" />
                    <asp:Parameter Name="Responsible" Type="String" />
                    <asp:Parameter Name="Completed" Type="Boolean" />
                    <asp:Parameter Name="CreatedDate" Type="DateTime" />
                    <asp:Parameter Name="ModifiedDate" Type="DateTime" />
                </InsertParameters>
                <SelectParameters>
                    <asp:SessionParameter Name="FK_ButikkNr" SessionField="StoreNo" Type="String" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="FK_ButikkNr" Type="String" />
                    <asp:Parameter Name="Goal" Type="String" />
                    <asp:Parameter Name="Description" Type="String" />
                    <asp:Parameter Name="Responsible" Type="String" />
                    <asp:Parameter Name="Completed" Type="Boolean" />
                    <asp:Parameter Name="CreatedDate" Type="DateTime" />
                    <asp:Parameter Name="ModifiedDate" Type="DateTime" />
                    <asp:Parameter Name="PK_ActivitiesId" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>

   
</asp:Content>
