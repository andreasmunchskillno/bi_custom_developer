﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EP_Driftsrapport;

namespace EP_Driftsrapport
{
    public partial class tiltak : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Sjekk av tilgang
            string login = Session["id"].ToString();

            if (login == "0")
                Response.Redirect("failure.aspx");

            lbInfo.Text = "Besøk hos " + Session["StoreName"] + " den " + Session["VisitName"] + ". Dersom du ønsker å endre butikk og/eller dato kan du gå til <a href=\"default.aspx\">besøkssiden</a>";

            //Dersom login er noe annet enn -1 har man kun tilgang til en butikk.
            if (login != "-1")
            {
                btnSaveNew.Enabled = false;
                btnUpdate.Enabled = false;
            }

            if (Session["VisitName"] == null)
            {
                lbStatus.Text = "Vennligst velg butikk og dato først. Gå til besøkssiden <a href=\"default.aspx\">her</a>";

                pnForm.Visible = false;
                pnGrid.Visible = false;
                lbInfo.Visible = false;
            }
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            EP_Driftsrapport.db_driftsrapportTableAdapters.ActivititesTableAdapter dAct = new db_driftsrapportTableAdapters.ActivititesTableAdapter();
            EP_Driftsrapport.db_driftsrapport.ActivititesDataTable activites = new db_driftsrapport.ActivititesDataTable();

            activites = dAct.GetDataByByActivityId(Convert.ToInt32(gdActivities.SelectedDataKey.Value.ToString()));

            txtGoal.Text = activites.Rows[0]["Goal"].ToString();
            txtDescription.Text = activites.Rows[0]["Description"].ToString();
            txtResponsible.Text = activites.Rows[0]["Responsible"].ToString();
            txtName.Text = activites.Rows[0]["Name"].ToString();
            txtDeadLine.Text = activites.Rows[0]["DeadLineDate"].ToString().Substring(0,10);

            if (activites.Rows[0]["Completed"].ToString() == "True")
                cbCompleted.Checked = true;
            else
                cbCompleted.Checked = false;
        }

        protected void btnSaveNew_Click(object sender, EventArgs e)
        {
            try
            {
                EP_Driftsrapport.db_driftsrapportTableAdapters.QueriesTableAdapter q = new db_driftsrapportTableAdapters.QueriesTableAdapter();

                q.insertActivity(Session["StoreNo"].ToString(), txtGoal.Text, txtDescription.Text, txtResponsible.Text, cbCompleted.Checked, txtName.Text, Convert.ToDateTime(txtDeadLine.Text));
                lbStatus.Text = "Lagret nytt tiltak";

                gdActivities.DataBind();

                resetTextBoxes();
            }
            catch(Exception r)
            {
                lbStatus.Text = "Feil under lagring. " + r.Message ;
            }
        }

        protected void Update_Click(object sender, EventArgs e)
        {
            if (gdActivities.SelectedDataKey != null)
            {
                try
                {
                    EP_Driftsrapport.db_driftsrapportTableAdapters.QueriesTableAdapter qAdapt = new db_driftsrapportTableAdapters.QueriesTableAdapter();
                    qAdapt.updateActivity(Convert.ToInt32(gdActivities.SelectedDataKey.Value), txtGoal.Text, txtDescription.Text, txtResponsible.Text, cbCompleted.Checked, txtName.Text, Convert.ToDateTime(txtDeadLine.Text));
                    lbStatus.Text = "Oppdaterte tiltak";

                    gdActivities.DataBind();

                    resetTextBoxes();
                }
                catch
                {
                    lbStatus.Text = "En feil oppstod under oppdatering av tiltak";
                }
            }
            else
            {
                lbStatus.Text = "Vennligst velg et tiltak før du prøver å oppdatere.";
            }
        }

        private void resetTextBoxes()
        {
            txtDeadLine.Text = "";
            txtDescription.Text = "";
            txtGoal.Text = "";
            txtName.Text = "";
            txtResponsible.Text = "";
        }

        protected void gdActivities_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton l = (LinkButton)e.Row.FindControl("LinkButton2");

                if (Session["id"].ToString() != "-1")
                {
                    l.Enabled = false;
                }
                else
                {
                    l.Attributes.Add("onclick", "javascript:return confirm('Er du sikker på at du ønsker å slette denne raden?')");
                }
            }
        }
    }
}