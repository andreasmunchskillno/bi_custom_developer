﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Net.Mail;

namespace EP_Remittering
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        DataTable newTable;

        private void loop()
        {
            StringBuilder htmlMessage = new StringBuilder();

            foreach (DataRow row in newTable.Rows)
            {
                try
                {
                    htmlMessage.Clear();

                    htmlMessage.Append("<h3>Oversikt over remitteringer</h3><br>");
                    htmlMessage.Append("<table>");

                    htmlMessage.Append("<tr style=\"background-color:#D0D0D0\"><td>Butikker EP</td><td>" + row["Butikker EP"] + "</td></tr>");
                    htmlMessage.Append("<tr><td>But.nr.</td><td>" + row["Butnr"] + "</td></tr>");
                    htmlMessage.Append("<tr style=\"background-color:#D0D0D0\"><td>Bank</td><td>" + row["Bank"] + "</td></tr>");
                    htmlMessage.Append("<tr><td>Bankportal</td><td>" + row["Bankportal"] + "</td></tr>");
                    htmlMessage.Append("<tr style=\"background-color:#D0D0D0\"><td>Saldo</td><td>" + row["Saldo"] + "</td></tr>");
                    htmlMessage.Append("<tr><td>Forfalt denne uken</td><td>" + row["Forfalt denne uken"] + "</td></tr>");
                    htmlMessage.Append("<tr style=\"background-color:#D0D0D0\"><td>Fra forrige uke</td><td>" + row["Fra forrige uke"] + "</td></tr>");
                    htmlMessage.Append("<tr><td>EP forrige uke</td><td>" + row["EP forrige uke"] + "</td></tr>");
                    htmlMessage.Append("<tr style=\"background-color:#D0D0D0\"><td>Betalt denne uken</td><td>" + row["Betalt denne uken"] + "</td></tr>");
                    htmlMessage.Append("<tr><td>Totalt betalt</td><td>" + row["Totalt betalt"] + "</td></tr>");
                    htmlMessage.Append("<tr style=\"background-color:#D0D0D0\"><td>Betalt til EP denne uken</td><td>" + row["Betalt til EP denne uken"] + "</td></tr>");
                    htmlMessage.Append("<tr><td>Total betalt til EP</td><td>" + row["Total betalt til EP"] + "</td></tr>");
                    htmlMessage.Append("<tr style=\"background-color:#D0D0D0\"><td>Forfalt ikke bet</td><td>" + row["Forfalt ikke bet"] + "</td></tr>");
                    htmlMessage.Append("<tr><td>Regnskapsfører</td><td>" + row["Regnskapsfører"] + "</td></tr>");
                    htmlMessage.Append("<tr style=\"background-color:#D0D0D0\"><td>Likviditet</td><td>" + row["Likviditet"] + "</td></tr>");
                    htmlMessage.Append("<tr><td>Notater</td><td>" + row["Notater"] + "</td></tr>");

                    htmlMessage.Append("</table>");

                    //Send;
                    //row["E-post franchisetager"]

                    if (row["E-post franchisetager"].ToString() != "")
                    {
                        sendMail(htmlMessage.ToString(), row["E-post franchisetager"].ToString().Trim());

                        lbStatus.Items.Add("Sender mail til " + row["E-post franchisetager"].ToString().Trim() + " .... OK");
                        Application.DoEvents();
                    }
                }
                catch(Exception err)
                {
                    lbStatus.Items.Add("Sender mail til " + row["E-post franchisetager"].ToString().Trim() + " .... FEILET. " + err.Message);
                    Application.DoEvents();
                }
            }
        }

        private void sendMail(string htmlBody, string adress)
        {
            EP_Remittering.Properties.Settings s = new EP_Remittering.Properties.Settings();

            using (var client = new SmtpClient("epex6"))
            {
                //Bruk credentials til den brukeren som kjører programmet
                client.UseDefaultCredentials = true;

                MailMessage newMail = new MailMessage(new MailAddress(s.From_Email, "Europris"), new MailAddress(adress));

                newMail.Subject = "Remittering";
                newMail.IsBodyHtml = true;

                var viewPlain = AlternateView.CreateAlternateViewFromString("Ditt epostprogram støtter ikke å vise bilder. Du må derfor klikke på vedlegget for å få opp rapporten.", null, "text/plain");
                var view = AlternateView.CreateAlternateViewFromString(htmlBody, null, "text/html");

                newMail.AlternateViews.Add(viewPlain);
                newMail.AlternateViews.Add(view);

                client.Send(newMail);
            }
        }

        private void openExcel(string path)
        {
            OleDbConnection con = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path +";Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";");

            StringBuilder stbQuery = new StringBuilder();
            stbQuery.Append("SELECT * FROM [Ark1$A:Q]");
            OleDbDataAdapter adp = new OleDbDataAdapter(stbQuery.ToString(), con);

            DataSet dsXLS = new DataSet();
            adp.Fill(dsXLS);

            DataView dvEmp = new DataView(dsXLS.Tables[0]);

            //Sjekk på at vi KUN tar med de kolonnene som er predefinert
            newTable = dvEmp.ToTable(false, "Butikker EP", "Butnr", "Bank", "Bankportal", "Saldo", "Forfalt denne uken", "Fra forrige uke", 
                "EP forrige uke", "Betalt denne uken", "Totalt betalt", "Betalt til EP denne uken", "Total betalt til EP", "Forfalt ikke bet", "Regnskapsfører", "Likviditet", "Notater", "E-post franchisetager");

            dgPreview.DataSource = newTable;

            dgPreview.Columns["E-post franchisetager"].DefaultCellStyle.BackColor = System.Drawing.Color.Green;
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    openExcel(openFileDialog1.FileName);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }  
        }

        private void btnSendmail_Click(object sender, EventArgs e)
        {
            if (newTable != null)
            {
                loop();
            }
            else
            {
                MessageBox.Show("Vennligst velg Excel-ark først");
            }

            MessageBox.Show("Mailutsendelse ferdig");
        }
    }
}
