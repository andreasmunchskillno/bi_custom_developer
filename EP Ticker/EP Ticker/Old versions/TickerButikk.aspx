﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TickerButikk.aspx.cs" Inherits="EP_Ticker._default2" %>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
		<meta charset="utf-8" />
		<title>Point Counter</title>
		
		<link href="style.css" rel="stylesheet" />
		<style>
			.crappy-plastic-part-made-in-china {
				background-color: #56864e;
				height: 1.2em;
				font-size: 3em;
				overflow: hidden;
                width: 250px;
                text-align: center;
                height: 60px;
			}
			.pad {
				width: 2em;
				margin: 0 auto;
				height: 0.8em;
			}
			ul {
				list-style: none;
				padding: 0;
				margin: -4.25em -10px 0 0;
				width: 0.85em;
				height: 12em;
				float: left;
			}
			li {
				height: 4em;
				width: 0.7em;
				line-height: 2;
				background-color: #56864e;
				color: #fff;
				text-align: center;
				cursor: pointer;
				font-weight: bold;
				font-family: "Helvetica Neue", Arial, sans-serif;
			}
				li.roundabout-in-focus {
					cursor: default;
				}
			li span {
				display: block;
				padding-top: 6em;
			}

			#carbonads-container {
				clear: both;
				margin-top: 1em;
			}
			#carbonads-container .carbonad {
				margin: 0 auto;
			}

		    .header {
                font-size: 18px;
                text-align: left;
                font-family: "Helvetica Neue", Arial, sans-serif;
                margin-left: 10px;
		    }

              .info {
                margin-top: 5px;
                font-size: 12px;
                text-align: left;
                font-family: "Helvetica Neue", Arial, sans-serif;
                margin-left: 0px;
		    }

		    body {
                margin: 10px;
		    }

		    .box {
                background-color: #D0D0D0;
                -webkit-border-radius: 8px;
	            -moz-border-radius: 8px;
	            -ms-border-radius: 8px;
	            -o-border-radius: 8px;
	            border-radius: 8px;
                width: 250px;
                text-align: center;
                padding: 8px 8px;
	            margin: 0 1em;
                border-bottom: 1px solid #A0A0A0;
                border-right: 1px solid #A0A0A0;
		    }
		</style>
	</head>
	<body>
        <div class="box">
		<div class="header">Økning handlekurv din butikk</div>
		
		<div class="crappy-plastic-part-made-in-china">
           
			<div class="pad">
				<ul id="digit-2">
					<li>0</li>
					<li>1</li>
					<li>2</li>
					<li>3</li>
					<li>4</li>
					<li>5</li>
					<li>6</li>
					<li>7</li>
					<li>8</li>
					<li>9</li>
				</ul>
				<ul id="digit-1">
					<li>0</li>
					<li>1</li>
					<li>2</li>
					<li>3</li>
					<li>4</li>
					<li>5</li>
					<li>6</li>
					<li>7</li>
					<li>8</li>
					<li>9</li>
				</ul>
				<ul id="digit-0">
					<li>0</li>
					<li>1</li>
					<li>2</li>
					<li>3</li>
					<li>4</li>
					<li>5</li>
					<li>6</li>
					<li>7</li>
					<li>8</li>
					<li>9</li>
				</ul>
                
			</div>
            
		</div>

            <div class="info">Økning i kroner sammenlignet med sammenlignbar dag i fjor (i dag - 364)</div>

        </div>
        
		<script src="scripts/jquery-1.10.2.min.js"></script>
		<script src="scripts/jquery.roundabout.min.js"></script>
        <script src="scripts/jquery.roundabout-shapes.min.js"></script>
		<script>
		    var count = 0,
			    clickable = true;

		    $(window).load(function () {
		        //applyCount(11);
                //Setter verdien i codebehind
		        <asp:Literal ID="Literal2" runat="server">applyCount(00);</asp:Literal>
		        //applyCount(11);
		    });

		    $(document).ready(function () {
		        $('ul').roundabout({
		            shape: "waterWheel"
		        });

		        $('#contribute')
					.click(function () {
					    if (!clickable) {
					        return false;
					    }

					    $('.interact a').fadeTo(100, 0.5);
					    clickable = false;

					    count += Math.ceil(Math.random() * 10);
					    applyCount(count);
					    return false;
					});

		        $('#how-much')
					.click(function () {
					    alert('The counter is currently at ' + count + '.');
					    return false;
					});
		    });

		    function applyCount(total) {
		        var i, part, child, factor, distance,
				    count = new String(total),
				    parts = count.split("").reverse();

		        if (total > 999) {
		            tilt();
		            return false;
		        }

		        for (i = parts.length - 1; i >= 0; i--) {
		            part = parseInt(parts[i], 10);

		            // get current position
		            child = $('ul#digit-' + i).data('roundabout').childInFocus;
		            factor = (part < child) ? (10 + part) - child : part - child;
		            distance = factor * 36;

		            if (i) {
		                $('ul#digit-' + i).roundabout('animateToDelta', -distance);
		            } else {
		                $('ul#digit-' + i).roundabout('animateToDelta', -distance, function () {
		                    $('.interact a').fadeTo(100, 1);
		                    clickable = true;
		                });
		            }

		        }
		    }

		    function tilt() {
		        for (var i = 0; i < 7; i++) {
		            var amount = (Math.random()) ? 5 + (Math.random() * 20 - 10) : -5 - (Math.random() * 20 - 10);
		            $('ul#digit-' + i).roundabout('animateToDelta', amount).find('li').css('color', '#c00');
		        }
		    }
        </script>
	</body>
</html>