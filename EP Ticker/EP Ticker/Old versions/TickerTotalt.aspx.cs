﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EP_Ticker
{
    public partial class _default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string t = "0";
            EP_Ticker.ReadCube r = new ReadCube();

            if (Application["date_ref"] == null)
            {
                try
                {
                    t = r.readHandlekurvAllDistricts();
                    Application["ref_no"] = t;
                    Application["date_ref"] = System.DateTime.Now;
                }
                catch
                {
                    t = "0";
                }
            }
            else
            {
                System.DateTime test = (System.DateTime)Application["date_ref"];

                if (test.Hour < System.DateTime.Now.Hour)
                {
                    t = r.readHandlekurvAllDistricts();
                    Application["date_ref"] = System.DateTime.Now;
                    Application["ref_no"] = t;
                }
                else
                {
                    t = Application["ref_no"].ToString();
                }
            }
           
            Literal1.Text = "applyCount(" + t + ");";
        }
    }
}