﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AnalysisServices.Hosting;
using Microsoft.AnalysisServices.AdomdClient;

namespace EP_Ticker
{
    public class ReadCube
    {
        public string connString_StoreSales = "Data Source=epsql7;Catalog=StoreSales";
        public string connString_Retail = "Data Source=epsql7;Catalog=RetailData";

        public CellSet printDebugInfoStoreDate()
        {
            try
            {
                AdomdConnection conn = new AdomdConnection(connString_Retail);
                conn.Open();

                string commandText = @"select StrToMember(""[Date].[Year - Month - Date].[Date].&["" + Format(now()-1, ""yyyy-MM-dd"") + ""T00:00:00]"", CONSTRAINED).lag(364) 
                    on 0,
                    [Measures].[Net Amount Ex Tax] on 1
                    from [RetailData]
                    ";

                AdomdCommand cmd = new AdomdCommand(commandText, conn);

                return cmd.ExecuteCellSet();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public CellSet printDebugInfoStore()
        {
            try
            {
                AdomdConnection conn = new AdomdConnection(connString_Retail);
                conn.Open();

                string commandText = @"with member [Handlekurv iaar]
                    as
                    (
	                    [Measures].[Avg Voucher Amount]
                    )

                    member [Handlekurv ifjor]
                    as
                    (
	                    [Date].[Year - Month - Date].currentmember.lag(364),
	                    [Measures].[Avg Voucher Amount]
                    )

                    member Increase as ([Handlekurv iaar] - [Handlekurv ifjor]), FORMAT_STRING = '#.#'

                    select {Increase, [Handlekurv ifjor],  [Handlekurv iaar]} on 0,
                    NON EMPTY [Store].[Store Num].[Store Num] on 1
                    from [RetailData]
                    where (
	                    StrToMember(""[Date].[Year - Month - Date].[Date].&["" + Format(now()-1, ""yyyy-MM-dd"") + ""T00:00:00]"", CONSTRAINED)
                    ) ";

                AdomdCommand cmd = new AdomdCommand(commandText, conn);

                return cmd.ExecuteCellSet();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public string readHandlekurvAllDistricts()
        {
            try
            {
                AdomdConnection conn = new AdomdConnection(connString_StoreSales);
                conn.Open();

                string commandText = @"with member [Hittil i konk]
                as
                (
                [DateFunctions].[Datefunctions].[Name].&[DP],
                 [Measures].[Shopping Cart]
                )

                member [Samme periode ifjor]
                as
                (
                [DateFunctions].[Datefunctions].[Name].&[FP],
                 [Measures].[Shopping Cart]
                )

                member [Økning]
                as
                (
	                iif([Samme periode ifjor] <> null,
	                 [Hittil i konk] - [Samme periode ifjor], 0)
                ), FORMAT_STRING = '#.#'

                select	[Measures].[Økning] on 0
                from [StoreSales]
                where (
                {[DimTime].[Year - Month - Date].[Date].&[2013-09-01T00:00:00] :
	                StrToMember(""[DimTime].[Year - Month - Date].[Date].&["" + Format(now()-1,""yyyy-MM-dd"") + ""T00:00:00]"", CONSTRAINED)
                }
                ) ";

                AdomdCommand cmd = new AdomdCommand(commandText, conn);

                return cmd.ExecuteCellSet().Cells[0].FormattedValue.ToString();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return "0";
            }
        }

        public string readHandlekurvAllButikk()
        {
            try
            {
                AdomdConnection conn = new AdomdConnection(connString_Retail);
                conn.Open();

                string commandText = @"with member [Salg iaar]
                as
                (
	                sum(
		                [Store].[Store Num].Children,
		                [Measures].[Net Amount Ex Tax]
	                )
                )

                member [Bonger iaar]
                as
                (
	                sum(
		                [Store].[Store Num].Children,
		                [Measures].[Voucher Count]
	                )
                )

                member [Handlekurv iaar]
                as
                [Salg iaar] / [Bonger iaar]

                member [Salg ifjor]
                as
                (
	                sum(
		                ([Store].[Store Num].Children, [Date].[Year - Month - Date].currentmember.lag(364)),
		                [Measures].[Net Amount Ex Tax]
	                )
                )

                member [Bonger ifjor]
                as
                (
	                sum(
		                ([Store].[Store Num].Children, [Date].[Year - Month - Date].currentmember.lag(364)),
		                [Measures].[Voucher Count]
	                )
                )

                member [Handlekurv ifjor]
                as
                [Salg ifjor] / [Bonger ifjor]

                member Increase as ([Handlekurv iaar] - [Handlekurv ifjor]), FORMAT_STRING = '#.#'

                select Increase on 0
                from [RetailData]
                where (
                    StrToMember(""[Date].[Year - Month - Date].[Date].&["" + Format(now()-1, ""yyyy-MM-dd"") + ""T00:00:00]"", CONSTRAINED)
                )";

                AdomdCommand cmd = new AdomdCommand(commandText, conn);

                return cmd.ExecuteCellSet().Cells[0].FormattedValue.ToString();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return "0";
            }
        }
    }
}