﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TickerTotalt2.aspx.cs" Inherits="EP_Ticker._default4" %>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
		<meta charset="utf-8" />
		<title>Point Counter</title>
		
		<link href="style.css" rel="stylesheet" />
		<style>
			.crappy-plastic-part-made-in-china {
				background-color: #56864e;
				height: 1.2em;
				font-size: 3em;
				overflow: hidden;
                width: 250px;
                text-align: center;
                height: 60px;
			}
			.pad {
				width: 2em;
				margin: 0 auto;
				height: 0.8em;
			}

		    .header {
                font-size: 18px;
                text-align: left;
                font-family: "Helvetica Neue", Arial, sans-serif;
                margin-left: 10px;
		    }

              .info {
                margin-top: 5px;
                font-size: 12px;
                text-align: left;
                font-family: "Helvetica Neue", Arial, sans-serif;
                margin-left: 0px;
		    }

		    body {
                margin: 10px;
		    }

		    .box {
                background-color: #D0D0D0;
                -webkit-border-radius: 8px;
	            -moz-border-radius: 8px;
	            -ms-border-radius: 8px;
	            -o-border-radius: 8px;
	            border-radius: 8px;
                width: 250px;
                text-align: center;
                padding: 8px 8px;
	            margin: 0 1em;
                border-bottom: 1px solid #A0A0A0;
                border-right: 1px solid #A0A0A0;
		    }
            .timer{
                font-size:40px;
                font-family: "Helvetica Neue", Arial, sans-serif;
                color: white;
            }
		</style>
	</head>
	<body>
        <div class="box">
		<div class="header">Økning handlekurv kjede</div>
		
		<div class="crappy-plastic-part-made-in-china">
           
			<span class="timer"></span>
            
		</div>

            <div class="info">Økning i handlekurv hittil i konk. sammenlignet med sammenlignbar periode ifjor. Tall i kroner</div>

        </div>
        
		<script src="scripts/jquery-1.10.2.min.js"></script>
		<script src="scripts/jquery.roundabout.min.js"></script>
        <script src="scripts/jquery.roundabout-shapes.min.js"></script>
		<script>
		    (function ($) {
		        $.fn.countTo = function (options) {
		            // merge the default plugin settings with the custom options
		            options = $.extend({}, $.fn.countTo.defaults, options || {});

		            // how many times to update the value, and how much to increment the value on each update
		            var loops = Math.ceil(options.speed / options.refreshInterval),
                        increment = (options.to - options.from) / loops;

		            return $(this).each(function () {
		                var _this = this,
                            loopCount = 0,
                            value = options.from,
                            interval = setInterval(updateTimer, options.refreshInterval);

		                function updateTimer() {
		                    value += increment;
		                    loopCount++;
		                    $(_this).html(value.toFixed(options.decimals));

		                    if (typeof (options.onUpdate) == 'function') {
		                        options.onUpdate.call(_this, value);
		                    }

		                    if (loopCount >= loops) {
		                        clearInterval(interval);
		                        value = options.to;

		                        if (typeof (options.onComplete) == 'function') {
		                            options.onComplete.call(_this, value);
		                        }
		                    }
		                }
		            });
		        };

		        $.fn.countTo.defaults = {
		            from: 0,  // the number the element should start at
		            to: 100,  // the number the element should end at
		            speed: 1000,  // how long it should take to count between the target numbers
		            refreshInterval: 100,  // how often the element should be updated
		            decimals: 1,  // the number of decimal places to show
		            onUpdate: null,  // callback method for every time the element is updated,
		            onComplete: null,  // callback method for when the element finishes updating
		        };
		    })(jQuery);

		    jQuery(function ($) {
		        $('.timer').countTo({
		            from: 0,
		            to: <asp:Literal id="Literal2" runat="server"></asp:Literal>,
		            speed: 5000,
		            refreshInterval: 50,
		            onComplete: function (value) {
		                //console.debug(this);
		            }
		        });
		    });
        </script>
	</body>
</html>