﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EP_Ticker
{
    public partial class _default4 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string t;
            EP_Ticker.ReadCube r = new ReadCube();
           
            try
            {
                t = r.readHandlekurvAllDistricts();
            }
            catch
            {
                t = "0";
            }

            Literal2.Text = t;
        }
    }
}