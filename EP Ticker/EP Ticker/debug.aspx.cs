﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AnalysisServices.AdomdClient;

namespace EP_Ticker
{
    public partial class debug : System.Web.UI.Page
    {

        public string getHtmlFromCellSet(CellSet cs)
        {
            System.Text.StringBuilder result = new System.Text.StringBuilder();

            //Output the column captions from the first axis//Note that this procedure assumes a single member exists per column.
            result.Append("\t\t\t");

            TupleCollection tuplesOnColumns = cs.Axes[0].Set.Tuples;

            foreach (Microsoft.AnalysisServices.AdomdClient.Tuple column in tuplesOnColumns)
            {
                result.Append(column.Members[0].Caption + "\t");

            }

            result.AppendLine();

            //Output the row captions from the second axis and cell data//Note that this procedure assumes a two-dimensional cellset
            TupleCollection tuplesOnRows = cs.Axes[1].Set.Tuples;
            for (int row = 0; row < tuplesOnRows.Count; row++)
            {
                for (int members = 0; members < tuplesOnRows[row].Members.Count; members++)
                {
                    result.Append(tuplesOnRows[row].Members[members].Caption + "\t");
                }

                for (int col = 0; col < tuplesOnColumns.Count; col++)
                {
                    result.Append(cs.Cells[col, row].FormattedValue + "\t");
                }
                result.AppendLine();
 
            }

            return result.ToString();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Label1.Text = "<h3>Kjørt: " + DateTime.Now.ToString(); 

            ReadCube t = new ReadCube();
            CellSet cs = t.printDebugInfoStore();

            Literal1.Text = "<pre>" + getHtmlFromCellSet(cs) +"</pre><hr>";

            CellSet cs2 = t.printDebugInfoStoreDate();

            Literal2.Text = "<pre>" + getHtmlFromCellSet(cs2) + "</pre><hr>";

            Literal3.Text = t.readHandlekurvAllButikk();
        }
    }
}