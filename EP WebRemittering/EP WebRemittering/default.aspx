﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="EP_WebRemittering._default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Content/Site.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1>Remitteringer</h1>



    </div>
       <p>Velg fil: <asp:FileUpload ID="FileUpload1" runat="server" /> 
           <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="1: Last opp" />
           <asp:Button ID="btnPreview" runat="server" OnClick="btnPreview_Click" Text="2: Forhåndsvis" />
           <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="3: Send mail" />
           <asp:Label ID="lbInfo" runat="server"></asp:Label>
        </p>
        
        <asp:GridView ID="dgPreview" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>

    </form>
</body>
</html>
