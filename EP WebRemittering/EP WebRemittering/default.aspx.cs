﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OleDb;
using System.Net.Mail;
using System.Data;
using System.Text;

namespace EP_WebRemittering
{
    public partial class _default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private void loop(DataTable newTable)
        {
            StringBuilder htmlMessage = new StringBuilder();

            foreach (DataRow row in newTable.Rows)
            {
                htmlMessage.Append("<h3>Oversikt over remitteringer</h3><br>");
                htmlMessage.Append("<table>");

                htmlMessage.Append("<tr style=\"background-color:#D0D0D0\"><td>Butikker EP</td><td>" + row["Butikker EP"] + "</td></tr>");
                htmlMessage.Append("<tr><td>But.nr.</td><td>" + row["Butnr"] + "</td></tr>");
                htmlMessage.Append("<tr style=\"background-color:#D0D0D0\"><td>Bank</td><td>" + row["Bank"] + "</td></tr>");
                htmlMessage.Append("<tr><td>Bankportal</td><td>" + row["Bankportal"] + "</td></tr>");
                htmlMessage.Append("<tr style=\"background-color:#D0D0D0\"><td>Saldo</td><td>" + row["Saldo"] + "</td></tr>");
                htmlMessage.Append("<tr><td>Forfalt denne uken</td><td>" + row["Forfalt denne uken"] + "</td></tr>");
                htmlMessage.Append("<tr style=\"background-color:#D0D0D0\"><td>Fra forrige uke</td><td>" + row["Fra forrige uke"] + "</td></tr>");
                htmlMessage.Append("<tr><td>EP forrige uke</td><td>" + row["EP forrige uke"] + "</td></tr>");
                htmlMessage.Append("<tr style=\"background-color:#D0D0D0\"><td>Betalt denne uken</td><td>" + row["Betalt denne uken"] + "</td></tr>");
                htmlMessage.Append("<tr><td>Totalt betalt</td><td>" + row["Totalt betalt"] + "</td></tr>");
                htmlMessage.Append("<tr style=\"background-color:#D0D0D0\"><td>Betalt til EP denne uken</td><td>" + row["Betalt til EP denne uken"] + "</td></tr>");
                htmlMessage.Append("<tr><td>Total betalt til EP</td><td>" + row["Total betalt til EP"] + "</td></tr>");
                htmlMessage.Append("<tr style=\"background-color:#D0D0D0\"><td>Forfalt ikke bet</td><td>" + row["Forfalt ikke bet"] + "</td></tr>");
                htmlMessage.Append("<tr><td>Regnskapsfører</td><td>" + row["Regnskapsfører"] + "</td></tr>");
                htmlMessage.Append("<tr style=\"background-color:#D0D0D0\"><td>Likviditet</td><td>" + row["Likviditet"] + "</td></tr>");
                htmlMessage.Append("<tr><td>Notater</td><td>" + row["Notater"] + "</td></tr>");

                htmlMessage.Append("</table>");

                //Send;
                //row["E-post franchisetager"]

                if (row["E-post franchisetager"].ToString() != "")
                {
                    sendMail(htmlMessage.ToString(), row["E-post franchisetager"].ToString());
                }

                htmlMessage.Clear();
            }
        }

        private void sendMail(string htmlBody, string adress)
        {
            using (var client = new SmtpClient("192.168.5.7"))
            {
                MailMessage newMail = new MailMessage(new MailAddress("no-reply@europris.no", "Europris"), new MailAddress(adress));

                newMail.Subject = "Remittering";
                newMail.IsBodyHtml = true;

                var viewPlain = AlternateView.CreateAlternateViewFromString("Ditt epostprogram støtter ikke å vise bilder. Du må derfor klikke på vedlegget for å få opp rapporten.", null, "text/plain");
                var view = AlternateView.CreateAlternateViewFromString(htmlBody, null, "text/html");

                newMail.AlternateViews.Add(viewPlain);
                newMail.AlternateViews.Add(view);

                client.Send(newMail);
            }
        }

        private void openExcel(string path)
        {
            OleDbConnection con = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";");

            StringBuilder stbQuery = new StringBuilder();
            stbQuery.Append("SELECT * FROM [Ark1$A:Q]");
            OleDbDataAdapter adp = new OleDbDataAdapter(stbQuery.ToString(), con);

            DataSet dsXLS = new DataSet();
            adp.Fill(dsXLS);

            DataView dvEmp = new DataView(dsXLS.Tables[0]);

            //Sjekk på at vi KUN tar med de kolonnene som er predefinert
            DataTable newTable = dvEmp.ToTable(false, "Butikker EP", "Butnr", "Bank", "Bankportal", "Saldo", "Forfalt denne uken", "Fra forrige uke",
                "EP forrige uke", "Betalt denne uken", "Totalt betalt", "Betalt til EP denne uken", "Total betalt til EP", "Forfalt ikke bet", "Regnskapsfører", "Likviditet", "Notater", "E-post franchisetager");

            dgPreview.DataSource = newTable;
            dgPreview.DataBind();

            Session["table"] = newTable;
            //dgPreview.Columns["E-post franchisetager"].DefaultCellStyle.BackColor = System.Drawing.Color.Green;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            String savePath = @"c:\temp\uploads\";

            if (FileUpload1.HasFile)
            {
                String fileName = FileUpload1.FileName;
                savePath += fileName;

                FileUpload1.SaveAs(savePath);

                Session["excelFile"] = savePath;

                // Notify the user of the name of the file
                // was saved under.
                lbInfo.Text = "Fil lagret";
            }
            else
            {
                // Notify the user that a file was not uploaded.
                lbInfo.Text = "Vennligst velg fil";
            }

        }

        protected void btnPreview_Click(object sender, EventArgs e)
        {
            if (Session["excelFile"] != null)
                openExcel(Session["excelFile"].ToString());
            else
                lbInfo.Text = "Vennligst last opp og velg fil først";
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["table"] != null)
                {
                    loop((DataTable)Session["table"]);
                    lbInfo.Text = "Mailutsendelse ferdig";
                }
                else
                {
                    lbInfo.Text = "Vennligst velg Excel-ark først";
                }
            }
            catch(Exception err)
            {
                lbInfo.Text = "En feil oppstod: " + err.Message;
            }
        }
    }
}