﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AnalysisServices;
using ADOMD;
using System.Net.Mail;
using System.Net;
using Microsoft.AnalysisServices.AdomdClient;

namespace GetSSASObjects
{
    class Program
    {
        private void lookObjects()
        {
            Server server = new Server();
            server.Connect("Data Source=EPSQLBITEST;Initial Catalog=Purchase;Provider=MSOLAP.5;Impersonation Level=Impersonate;");
            Database database = server.Databases.FindByName("Purchase");
            Cube storeSales = database.Cubes.FindByName("Purchase");

            for (int i = 0; i < storeSales.MeasureGroups.Count; i++)
            {
                MeasureGroup ItemSales = storeSales.MeasureGroups[i];
                Console.WriteLine(ItemSales.Name);
            }

            Console.ReadKey();
        }

        public void processLastPartitions(string partitionName)
        {
            Server server = new Server();
            server.Connect("Data Source=EPSQLBITEST;Initial Catalog=Purchase;Provider=MSOLAP.5;Impersonation Level=Impersonate;");
            Database database = server.Databases.FindByName("Purchase");
            Cube storeSales = database.Cubes.FindByName("Purchase");
            MeasureGroup ItemSales = storeSales.MeasureGroups.FindByName(partitionName);

            //Get partitions
            Partition oldPartition = ItemSales.Partitions[0];
            //Partition oldPartition = ItemSales.Partitions.FindByName(partitionName);

            try
            {
                //Fors�k � prosessere partisjonen vanlig.
                oldPartition.Process(ProcessType.ProcessFull);
                Console.WriteLine("Prosesserer " + oldPartition.Name);
            }
            catch (Exception err)
            {
                Console.WriteLine(err.Message);
            }

            server.Disconnect();
            Console.ReadKey();
        }

        static void Main(string[] args)
        {
            Program p = new Program();
            //p.lookObjects();
            p.processLastPartitions("Tid");
        }
    }
}
