﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Services.Protocols;
using RSScheduler.ReportExecution2005;
using System.IO;
using Microsoft.AnalysisServices.AdomdClient;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;

namespace RSScheduler
{
    class Program
    {

        string connString = "Data Source=epsql7;Catalog=StoreSales";

        string mdx1 = @"WITH  MEMBER [Measures].[Net Amount Ex Tax Rank] AS 'IIF ( ISEMPTY([Net Amount Ex Tax]), NULL, RANK ( [Store.Store].CurrentMember, 
                ORDER ( AddCalculatedMembers( [Store.Store].CurrentMember.Siblings ), [Net Amount Ex Tax], DESC )) ) 
                ' MEMBER [Measures].[Snittbeløp pr. bong] AS 'IIF(ISEMPTY([Measures].[Net Amount Ex Tax]), NULL, [Measures].[Net Amount Ex Tax] / [Measures].[Customer Count])', 
                SOLVE_ORDER=10 SELECT  NON EMPTY {  { { {  { AddCalculatedMembers([Store.Store].[Store].Members), [Store.Store].[(All)] }  } } }  }  ON ROWS ,  
                { [Measures].[Net Amount Ex Tax Rank], [Measures].[Net Amount Ex Tax], [Measures].[Budget], [Measures].[Profit], [Measures].[Margin], [Measures].[Customer Count], [Measures].[Snittbeløp pr. bong]}  
                ON COLUMNS FROM  ( SELECT ( { [Prev Week]} ) ON 0 
                FROM [StoreSales] ) WHERE ( [Prev Week] )  CELL PROPERTIES VALUE, FORMATTED_VALUE";

        string mdx2 = @"SELECT {
	            [Measures].[Net Amount Ex Tax], 
	            Measures.Budget, 
	            [Measures].[Budget index]
            } ON 0, 
            {
	            Order(
		            BottomCount(
				            Filter(
					            [Store].[Store].[Store].Members,
					            [Measures].[Net Amount Ex Tax] <> NULL AND Measures.Budget <> 0),10,
				            [Measures].[Budget index])
		            ,[Measures].[Budget index],BASC)
	            } On Rows
            from StoreSales
            where ([Date], [Store].[Active].&[1])";

        string mdx3 = @"with member [Hittil i konk]
            as
            (
            [DateFunctions].[Datefunctions].[Name].&[DP],
             [Measures].[Shopping Cart]
            )

             set Stores
             as
             {
             [Store].[OrginalStore - Store].[OrginalStore].[EP ALTA]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP ASKIM (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP ASKØY (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP BAMBLE]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP BARDUFOSS]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP BILLINGSTAD]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP BJUGN]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP BJØRKELANGEN]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP BODØ (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP BOGAFJELL]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP BRANDBU (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP BRENNÅSEN]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP BROKELANDSHEIA (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP BRUMUNDDAL]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP BRYN]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP BRYNE (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP BRØNNØYSUND]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP BØ]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP BØMLO]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP DOKKA]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP DRØBAK]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP EGERSUND]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP EIDSVOLL]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP EIDSVÅG (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP EIKER]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP ELNESVÅGEN (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP ELVERUM]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP EVJE]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP FARSUND]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP FAUSKE (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP FINNSNES]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP FLATÅSEN (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP FLEKKEFJORD]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP FLISA]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP FLORØ]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP FLÅ]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP FREDRIKSTAD]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP FYLLINGSDALEN]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP FØRDE]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP GANDDAL]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP GAUSDAL]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP GJØVIK (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP GOL (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP GRENLAND GS Senteret]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP GRIMSTAD]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP GRINI PLUSS]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP HALDEN]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP HAMMERFEST]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP HARSTAD]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP HAUGENSTUA]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP HITRA]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP HOLMESTRAND]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP HORTEN (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP HOV]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP HUNSTAD (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP HUSNES]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP HØNEFOSS]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP INDRE ARNA]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP ISEVEIEN]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP JØRPELAND/STRAND]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP KARMØY]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP KILEN TØNSBERG]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP KIRKENES (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP KLEPP]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP KLØFTA]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP KNARVIK]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP KOLVEREID]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP KONGSBERG]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP KONGSVINGER]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP KONNERUD (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP KRAGERØ]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP KRISTIANSUND (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP KRØGENES]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP KVALA]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP KVINESDAL (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP KYRKSÆTERØRA]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP LADE]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP LAKSELV]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP LAMBERTSETER]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP LANGNES]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP LEIRA]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP LEKNES]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP LENA]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP LEVANGER (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP LIERSTRANDA]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP LIERTOPPEN]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP LILLEHAMMER PLUSS]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP LILLESAND]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP LILLESTRØM (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP LINDEBERG]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP LOM]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP LUND]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP LYNGDAL]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP MADLA]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP MANDAL]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP MAURA]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP MELHUS]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP MIDTTUN]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP MJØNDALEN (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP MO I RANA]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP MOELV]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP MOLDE (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP MOSJØEN]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP MOSS PLUSS (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP MYSEN (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP MÅLØY]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP NAMSOS (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP NANSET]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP NARVIK]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP NORDFJORDEID]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP NORHEIMSUND (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP NORÅS FANA (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP NOTODDEN]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP NÆRBØ (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP NØTTERØY (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP ODDA]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP OPPDAL (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP ORKANGER]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP OS]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP OTTA]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP PORSGRUNN]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP PRINSDAL]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP RAGLAMYR PLUSS]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP RAKKESTAD]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP RANDABERG]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP RAUFOSS]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP RENA]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP REVETAL]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP RINGEBU (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP RISSA]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP ROA]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP RØROS]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP RÅHOLT]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP SANDANE]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP SANDE I VESTFOLD]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP SANDEFJORD]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP SANDNES]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP SANDNESSJØEN]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP SANDVIKA]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP SARPSBORG]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP SAUDA]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP SELJORD]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP SELLEBAKK]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP SEM (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP SKARNES (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP SKEDSMO (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP SKIEN]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP SLEMMESTAD]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP SOGNDAL (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP SOLA (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP SOLHEIMSVIKEN]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP SORTLAND]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP SOTRA]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP STANGE]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP STEINKJER (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP STJØRDAL]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP STOA (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP STORD]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP STORHAMAR (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP STRYN]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP STØPERIGATEN]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP STØREN]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP SURNADALSØRA (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP SVEBERG]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP SVINESUNDSPARKEN]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP SVOLVÆR (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP SYKKYLVEN]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP SØRKJOSEN]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP SØRLANDSPARKEN PLUSS]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP SØRUMSAND]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP TILLER PLUSS]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP TRYSIL]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP TVEDESTRAND (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP TYNSET]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP ULSTEINVIK BRATTØRA (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP VADSØ]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP VATNE]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP VENNESLA (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP VERDAL (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP VESTBY]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP VESTKANTEN]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP VIKERSUND]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP VOLDA (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP VOLLEBEKK]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP VOSS]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP VÅGSBYGD]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP ØLENSVÅG (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP ØREBEKK]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP ØRSTA]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP ÅLESUND PLUSS]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP ÅLGÅRD]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP ÅNDALSNES (*)]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP ÅRNES]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP ÅS]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP ÅSANE PLUSS]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP ÅSEN]
             ,[Store].[OrginalStore - Store].[OrginalStore].[EP ÅSSIDEN PLUSS (*)]
             }

            member [Samme periode ifjor]
            as
            (
            [DateFunctions].[Datefunctions].[Name].&[FP],
             [Measures].[Shopping Cart]
            )

            member [Goal]
            as
            (
	            13
            )

            member [Økning]
            as
            (
	            iif([Samme periode ifjor] <> null,
	             [Hittil i konk] - [Samme periode ifjor], 0)
            )

            set [Top butikk] as
            generate
            (
	            NonEmpty(
		            [Store].[District - Store].[District].members, [Measures].[Customer Count]),
		            { [Store].[District].CurrentMember } *
	            TopCount(
		            Stores, 1, [Measures].[Økning]
	            )
            )

            SELECT [Top butikk]  on 1,
            {
	            [Measures].[Økning],
	            [Goal]
            } on 0
            from [StoreSales]
            where (
	            {
	            StrToMember(""[DimTime].[Year - Month - Date].[Date].&["" +Format(now()-1, ""yyyy-MM"") + ""-01"" + ""T00:00:00]"", CONSTRAINED) :
		            StrToMember(""[DimTime].[Year - Month - Date].[Date].&["" + Format(now() - 1, ""yyyy-MM-dd"") + ""T00:00:00]"", CONSTRAINED)
	            }
            ) 
            ";

        string mdx4 = @"WITH  MEMBER [Measures].[Net Amount Ex Tax Rank] 
                AS 'IIF ( ISEMPTY([Net Amount Ex Tax]), NULL, RANK ( [Store.OrginalStore - Store].CurrentMember, 
                ORDER ( AddCalculatedMembers( [Store.OrginalStore - Store].CurrentMember.Siblings ), 
                [Net Amount Ex Tax], DESC )) ) ' 
                MEMBER [Measures].[Snittbeløp pr. bong] 
                AS 'IIF(ISEMPTY([Measures].[Net Amount Ex Tax]), NULL, [Measures].[Net Amount Ex Tax] / [Measures].[Customer Count])', 
                SOLVE_ORDER=10 
                SELECT  NON EMPTY {  { { {  { AddCalculatedMembers([Store.OrginalStore - Store].[OrginalStore].Members), 
                [Store.OrginalStore - Store].[(All)] }  } } }  }  ON ROWS , 
                 { [Measures].[Net Amount Ex Tax Rank], [Measures].[Net Amount Ex Tax], [Measures].[Budget], [Measures].[Profit], [Measures].[Margin], [Measures].[Customer Count], [Measures].[Snittbeløp pr. bong]}  ON COLUMNS 
                 FROM  ( SELECT FILTER([DimTime].[DimTime].LEVELS(1).MEMBERS, 
                 ([DimTime].[DimTime].CURRENTMEMBER.MEMBERVALUE =CDATE(Format(now()-1, ""yyyy-MM-dd"")))) ON 0 
                 FROM [StoreSales]) CELL PROPERTIES VALUE, FORMATTED_VALUE";

        string mdx5 = @"<ClearCache xmlns=""http://schemas.microsoft.com/analysisservices/2003/engine"">
              <Object>
                <DatabaseID>StoreSales</DatabaseID>
              </Object>
            </ClearCache>
            ";

        private void runMdx(string mdx)
        {
            try
            {
                AdomdConnection conn = new AdomdConnection(connString);
                conn.Open();

                string commandText = mdx;

                AdomdCommand cmd = new AdomdCommand(commandText, conn);
                Console.WriteLine("MDX traff på " + cmd.ExecuteReader().FieldCount + " rader");

                conn.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        static void Main(string[] args)
        {
            Program P = new Program();
            RSScheduler.Properties.Settings s = new Properties.Settings();

            try
            {

                for (int i = 0; i <= 5; i++)
                {
                    P.runMdx(P.mdx1);
                    P.runMdx(P.mdx2);
                    P.runMdx(P.mdx3);
                    P.runMdx(P.mdx4);
                    P.runMdx(P.mdx5);
                }

                Console.WriteLine("Kjøring OK");

                Environment.Exit(0);
            }
            catch (Exception err)
            {
                Console.WriteLine("Kjøring feilet. " + err.Message);
                Environment.Exit(1);
            }
            finally
            {
                Console.ReadKey();
            }
        }
    }
}
