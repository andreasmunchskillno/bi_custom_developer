﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Services.Protocols;
using RSScheduler.ReportExecution2005;
using System.IO;
using Microsoft.AnalysisServices.AdomdClient;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;

namespace RSScheduler
{
    class Program
    {
        public static int addDays = -1;

        string MhtmlFilename = @"D:\ScheduledApps\report.mht";

        string PDFFileName = @"D:\ScheduledApps\Key_figures_store_sales_" + DateTime.Now.AddDays(addDays).ToShortDateString().Replace('.','_') + ".pdf";
        string PNGFileName = @"D:\ScheduledApps\Key_figures_store_sales_" + DateTime.Now.AddDays(addDays).ToShortDateString().Replace('.', '_') + ".tif";
        string errorFile = @"D:\ScheduledApps\RSScheduler\error.txt";

        string PDFFileNamePrevious = @"D:\ScheduledApps\Key_figures_store_sales_" + DateTime.Now.AddDays(-2).ToShortDateString().Replace('.', '_') + ".pdf";
        string PNGFileNamePrevious = @"D:\ScheduledApps\Key_figures_store_sales_" + DateTime.Now.AddDays(-2).ToShortDateString().Replace('.', '_') + ".tif";

        string PNGName = "Key_figures_store_sales_" + DateTime.Now.AddDays(addDays).ToShortDateString().Replace('.', '_') + ".tif";

        string RSMailReport = "/LED/LED_12_101_Key_figures_Index";
        string PDFMailReport = "/LED/LED_12_101_Key_figures_Index_detailed";

        private void runMdx()
        {
            try
            {
                RSScheduler.Properties.Settings s = new Properties.Settings();
                AdomdConnection conn = new AdomdConnection(s.SSAS_Connection);
                conn.Open();

                string commandText = @"with member [Measures].[Index]
            as
            (
	            iif(Measures.Budget = null 
		            or [Measures].[Net Amount Ex Tax] = null, 0, 
		            ([Measures].[Net Amount Ex Tax] / Measures.Budget) * 100
	            )
            )

            member [Measures].[Budget2]
            as
            (
	            iif(Measures.Budget = null, 0, Measures.Budget)
            )

            SELECT {[Measures].[Net Amount Ex Tax], Measures.Budget2, [Measures].[Index]} ON 0, 

            {Order(BottomCount(Filter([Store].[Store].[Store].Members,
            [Measures].[Net Amount Ex Tax] <> Null),10,
            [Measures].[Net Amount Ex Tax]),[Measures].[Net Amount Ex Tax],BASC)} On Rows

            from StoreSales
            where ([Date], [Store].[Active].&[1])";

                AdomdCommand cmd = new AdomdCommand(commandText, conn);
                Console.WriteLine("MDX traff på " + cmd.ExecuteReader().FieldCount + " rader");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Funksjon for å sjekke om salgstall er mer enn 20% under budsjett.
        /// Returnerer 0 eller 1 avhengig av dette.
        /// 0 = feil.
        /// 1 = OK
        /// </summary>
        private string checkNumbers()
        {
            try
            {
                RSScheduler.Properties.Settings s = new Properties.Settings();
                AdomdConnection conn = new AdomdConnection(s.SSAS_Connection);

                conn.Open();

                string date = System.DateTime.Now.AddDays(addDays).ToString("yyyy-MM-dd");

                string commandText = @"
                    with member RunReport
                    as
                    iif(isEmpty([Measures].[Net Amount Ex Tax]) or isEmpty([Measures].[Budget]), 0,
                    iif([Measures].[Net Amount Ex Tax] 
	                    / [Measures].[Budget] < 0.80, 0, 1))

                    select {[Measures].[RunReport], [Measures].[Net Amount Ex Tax], [Measures].[Budget]} on 0
                    from [StoreSales]
                    where [DimTime].[Year - Month - Date].[Date].&[" + date + "T00:00:00]";

                AdomdCommand cmd = new AdomdCommand(commandText, conn);
                //Console.WriteLine("Sjekk mot budsjett returnerte verdien " + cmd.ExecuteCellSet().Cells[0].Value);

                return cmd.ExecuteCellSet().Cells[0].Value.ToString();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return "0";
            }
        }

        private void makeReportFile(string fileName, string format, string devInfo, string reportPath)
        {
            ReportExecution2005.ReportExecutionService rs = new ReportExecution2005.ReportExecutionService();

            rs.Credentials = System.Net.CredentialCache.DefaultCredentials;

            rs.Timeout = System.Threading.Timeout.Infinite;
            
            byte[] result = null;
            string historyID = null;

            string encoding;
            string mimeType;
            string extension;
            ReportExecution2005.Warning[] warnings = null;
            string[] streamIDs = null;

            ReportExecution2005.ExecutionInfo execInfo = new ReportExecution2005.ExecutionInfo();
            ReportExecution2005.ExecutionHeader execHeader = new ReportExecution2005.ExecutionHeader();

            rs.ExecutionHeaderValue = execHeader;

            ReportExecution2005.ParameterValue[] parameter = new ParameterValue[1];

            //string date = String.Format("{0:yyyy/mm/dd}",System.DateTime.Now.ToShortDateString());

            parameter[0] = new ParameterValue();
            parameter[0].Label = "Date2";
            parameter[0].Name = "Date2";
            parameter[0].Value = System.DateTime.Now.AddDays(addDays).ToString();

            execInfo = rs.LoadReport(reportPath, historyID);

            rs.SetExecutionParameters(parameter, "nb-NO"); 
            String SessionId = rs.ExecutionHeaderValue.ExecutionID;

            Console.WriteLine("SessionID: {0}", rs.ExecutionHeaderValue.ExecutionID);

            try
            {
                result = rs.Render(format, devInfo, out extension, out encoding, out mimeType, out warnings, out streamIDs);

                execInfo = rs.GetExecutionInfo();

                Console.WriteLine("Execution date and time: {0}", execInfo.ExecutionDateTime);
            }
            catch (SoapException e)
            {
                Console.WriteLine(e.Detail.OuterXml);
            }
            // Write the contents of the report to an MHTML file.
            try
            {
                FileStream stream = File.Create(fileName, result.Length);
                Console.WriteLine("File created.");
                stream.Write(result, 0, result.Length);
                Console.WriteLine("Result written to the file.");
                stream.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                Console.WriteLine("Ferdig med generering av rapport " + format);
            }
        }
        
        public void sendMail2(string emailFrom, string emailTo, string subject)
        {
            using (var client = new SmtpClient("192.168.7.172"))
            {
                MailMessage newMail = new MailMessage(new MailAddress(emailFrom, "Europris"), new MailAddress(emailTo));
          
                newMail.Subject = subject;
                newMail.IsBodyHtml = true;

                var inlineLogo = new LinkedResource(PNGFileName, MediaTypeNames.Image.Tiff);
                inlineLogo.ContentId = Guid.NewGuid().ToString();

                string body = string.Format(@"<img src=""cid:{0}"" />", inlineLogo.ContentId);

                var viewPlain = AlternateView.CreateAlternateViewFromString("Ditt epostprogram støtter ikke å vise bilder. Du må derfor klikke på vedlegget for å få opp rapporten.", null, "text/plain");

                var view = AlternateView.CreateAlternateViewFromString(body, null, "text/html");

                view.LinkedResources.Add(inlineLogo);
                //view.TransferEncoding = TransferEncoding.Base64;
                
                view.LinkedResources[0].TransferEncoding = TransferEncoding.Base64;
                view.LinkedResources[0].ContentType.Name = PNGName;

                newMail.AlternateViews.Add(viewPlain);
                newMail.AlternateViews.Add(view);
                newMail.Attachments.Add(new Attachment(PDFFileName));
                
                client.Send(newMail);
            }
        }
        
        /*
        public void sendMail3(string emailFrom, string emailTo, string subject)
        {
            using (var client = new SmtpClient("192.168.5.7"))
            {
                MailMessage newMail = new MailMessage(new MailAddress(emailTo), new MailAddress(emailTo));

                newMail.Subject = subject;
                newMail.IsBodyHtml = true;

                byte[] imageBytes = File.ReadAllBytes(PNGFileName);

                string base64String = Convert.ToBase64String(imageBytes);

                string body = "<img src=\"data:image/png;base64," + base64String + "\" />";

                var view = AlternateView.CreateAlternateViewFromString(body, null, "text/html");
                newMail.AlternateViews.Add(view);

                client.Send(newMail);
            }
        }
        */
        /// <summary>
        /// Denne funksjonen er for å sende MHTML-mail
        /// </summary>
        /// <param name="emailFrom"></param>
        /// <param name="emailTo"></param>
        /// <param name="subject"></param>
        /// <param name="MHTmessage"></param>
        /// <param name="PDFReport"></param>
        public void SendEmail(string emailFrom, string emailTo, string subject, string MHTmessage, string PDFReport)
        {
            RSScheduler.Properties.Settings s = new Properties.Settings();

            string smtpAddress = s.SMTP;

            try
            {
                CDO.Message oMessage = new CDO.Message();
                CDO.Message msg = new CDO.Message();

                // set message 
                ADODB.Stream oStream = new ADODB.Stream();
                oStream.Charset = "ascii";
                oStream.Open();
                oStream.WriteText(MHTmessage);
                oMessage.DataSource.OpenObject(oStream, "_Stream");

                // set configuration 
                ADODB.Fields oFields = oMessage.Configuration.Fields;

                oFields["http://schemas.microsoft.com/cdo/configuration/sendusing"].Value = CDO.CdoSendUsing.cdoSendUsingPort;
                oFields["http://schemas.microsoft.com/cdo/configuration/smtpserver"].Value = smtpAddress;
                
                oFields.Update();

                // set other values 
                oMessage.MimeFormatted = true;
                oMessage.Subject = subject;
                oMessage.Sender = emailFrom;
                oMessage.To = emailTo;
                oMessage.From = "Europris";

                oMessage.AddAttachment(PDFReport);
     
                oMessage.Send();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void sendMailToList()
        {
            RSScheduler.Properties.Settings s = new Properties.Settings();

            string connetionString = null;
            SqlConnection cnn;
            SqlCommand cmd;
            string sql = null;
            SqlDataReader reader;

            connetionString = s.DWH_Meta;

            if (s.SEND_DEBUG_MAIL)
            {
                //sql = "select 'sindre.mathisen@europris.no' As Alias";
                sql = "select 'LarsOve.Davidsen@europris.no' As Alias";
            }
            else
            {
                sql = "select Alias from dbo.DataDrivenSubscription_RS where RapportFilter = 'KeyFigures' and aktiv = 'True'";
            }
            
            //string f = File.ReadAllText(MhtmlFilename); // For lesing av MHTML

            cnn = new SqlConnection(connetionString);
            try
            {
                cnn.Open();
                cmd = new SqlCommand(sql, cnn);
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    //SendEmail("no-reply@europris.no", reader.GetString(0), "Key figures", f, PDFFileName);
                    sendMail2("no-reply@europris.no", reader.GetString(0), "Key figures store sales " + DateTime.Now.AddDays(addDays).ToShortDateString().Replace(':', '_'));
                    //sendMail3("no-reply@europris.no", reader.GetString(0), "Key figures", PDFFileName);
                    Console.WriteLine("Epost sendt. " + reader.GetString(0));
                }

                reader.Close();
                cmd.Dispose();
                cnn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Can not open connection ! " + ex.Message);
            }
        }

        static void Main(string[] args)
        {
            Program P = new Program();
            RSScheduler.Properties.Settings s = new Properties.Settings();

            try
            {
                string day = System.DateTime.Now.DayOfWeek.ToString();
               
                Console.Write(day);

                //Bestemme om rapporten skal kjøres på mandag eller ikke.
                if (day != "Monday" || s.RunMonday)
                {
                    //Hvis det er salgs 20 % under budsjett og kjøres på søndag.
                    //Ikke vis rapport.
                    if (P.checkNumbers() == "0" && day == "Sunday")
                    {

                    }
                    else
                    {
                        if (File.Exists(P.MhtmlFilename))
                            File.Delete(P.MhtmlFilename);

                        if (File.Exists(P.PDFFileName))
                            File.Delete(P.PDFFileName);

                        if (File.Exists(P.PNGFileName))
                            File.Delete(P.PNGFileName);

                        //P.makeReportFile(P.MhtmlFilename, "MHTML", @"<DeviceInfo><Toolbar>False</Toolbar></DeviceInfo>", P.RSMailReport);

                        P.runMdx();

                        P.checkNumbers();

                        //Pga bug i AS får man av og til en feilmelding første gang rapporten kjøres dersom den inneholder bottomcount.
                        //Et herrrrrrrlig problem
                        P.makeReportFile(P.PDFFileName, "PDF", null, P.PDFMailReport);

                        P.makeReportFile(P.PNGFileName, "Image", @"<DeviceInfo><OutputFormat>TIFF</OutputFormat></DeviceInfo>", P.RSMailReport);

                        P.sendMailToList();

                        Thread.Sleep(2000);

                        if (File.Exists(P.PDFFileNamePrevious))
                            File.Delete(P.PDFFileNamePrevious);

                        if (File.Exists(P.PNGFileNamePrevious))
                            File.Delete(P.PNGFileNamePrevious);
                    }
                }

                File.AppendAllText(P.errorFile, "\r\n\n " + DateTime.Now.ToLongDateString() + " ---> Rapport sendt. Debug: " + s.SEND_DEBUG_MAIL);
                Environment.Exit(0);
            }
            catch(Exception err)
            {
                //Console.WriteLine(err.Message);
                File.AppendAllText(P.errorFile, "\r\n\n " + DateTime.Now.ToLongDateString() + " ---> " + err.Message + ". Debug: " + s.SEND_DEBUG_MAIL);
                Environment.Exit(1);
            }
        }
    }
}
