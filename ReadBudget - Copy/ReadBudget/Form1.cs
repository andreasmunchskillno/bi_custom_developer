﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Globalization;

using Excel = Microsoft.Office.Interop.Excel;

namespace ReadBudget
{
    public partial class Form1 : Form
    {
        SqlConnection conn;
        string budgetType = "";

        public Form1()
        {
            InitializeComponent();
        }

        public void openConn()
        {
            conn = new SqlConnection("Data Source=epsqlbitest;Initial Catalog=DWH_STG1;Integrated Security=SSPI");

            conn.Open();
        }

        public void closeConn()
        {
            conn.Close();
        }

        public void Insertdata(string Store, DateTime budgetDate, double budget)
        {
            CultureInfo culture = new CultureInfo("en-US");      

            try
            {
                string insertString = "";

                if (budgetType == "SalgButikk")
                {
                    // prepare command string
                    insertString = @"
                 exec DWH_STG2.dbo.[spInsertBudgetSales] '" + Store + "', '" + budgetDate.ToString("d", culture) + "'," + budget.ToString("#0.0000", culture);
                }
                else if (budgetType == "KunderButikk")
                {
                    // prepare command string
                    insertString = @"
                  exec DWH_STG2.dbo.[spInsertBudgetCustomer] '" + Store + "', '" + budgetDate.ToString("d", culture) + "'," + budget.ToString("#0.0000", culture);
                }
                else if (budgetType == "BudsjettForretningsomradeGrossistVarekost")
                {
                    // prepare command string
                    insertString = @"
                  insert into [STG1_FactBudgetItemCostOperations]
                  ([ItemNo],[BudgetDate],[DayBudget])
                  values ('" + Store + "', '" + budgetDate.ToString("d", culture) + "'," + budget.ToString("#0.0000", culture) + ")";
                }
                else if (budgetType == "BudsjettForretningsområdeGrossistSalg")
                {
                    // prepare command string
                    insertString = @"
                  insert into [STG1_FactSalesBudgetOperations]
                  ([ItemNo],[BudgetDate],[DayBudget])
                  values ('" + Store + "', '" + budgetDate.ToString("d", culture) + "'," + budget.ToString("#0.0000", culture) + ")";
                }
                else if (budgetType == "VarekostButikk")
                {
                    // prepare command string
                    insertString = @"
                  exec DWH_STG2.dbo.[spInsertBudgetItemCost] '" + Store + "', '" + budgetDate.ToString("d", culture) + "'," + budget.ToString("#0.0000", culture);
                }
                else if (budgetType == "KunderButikkTOTAL")
                {
                     // prepare command string
                    insertString = @"exec DWH_STG2.dbo.spInsertBudget 2, " + budget.ToString("#0.0000", culture) + ", '" +  budgetDate.ToString("d", culture)+ "'";
                }
                else if (budgetType == "VarekostButikkTOTAL")
                {
                     // prepare command string
                    insertString = @"exec DWH_STG2.dbo.spInsertBudget 3, " + budget.ToString("#0.0000", culture) + ", '" +  budgetDate.ToString("d", culture)+ "'";
                }
                else if (budgetType == "SalgButikkTOTAL")
                {
                     // prepare command string
                    insertString = @"exec DWH_STG2.dbo.spInsertBudget 1, " + budget.ToString("#0.0000", culture) + ", '" + budgetDate.ToString("d", culture) + "'";
                }

                // 1. Instantiate a new command with a query and connection
                SqlCommand cmd = new SqlCommand(insertString, conn);

                // 2. Call ExecuteNonQuery to send command
                cmd.ExecuteNonQuery();
            }
            catch (Exception er)
            {
                MessageBox.Show(er.Message);
            }
        }

        private void loopData(bool useFormattedValue)
        {
            CultureInfo culture = new CultureInfo("en-US");      

            openConn();

            DateTime timeStamp;

            int numberOfStores = Convert.ToInt32(txtNumberOfStores.Text);

            Excel.Application xlApp = new Excel.Application();
            string path = txtFileName.Text;

            Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(path);

            Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
            Excel.Range xlRange = xlWorksheet.UsedRange;

            int rowCount = xlRange.Rows.Count;
            int colCount = xlRange.Columns.Count;

            pgImport.Minimum = 0;
            pgImport.Maximum = numberOfStores;

            pgImport.Value = 0;

            string StoreNo = "";

            double dymmy;

            for (int b = 2; b < numberOfStores + 2; b++)
            {
#warning Endre denne dersom annet år

                //Starter dag siste dag i året før
                timeStamp = Convert.ToDateTime("2017/12/30");

                for (int i = 1; i <= rowCount; i++)
                {
                    if (i == 1)
                    {
                        StoreNo = xlRange.Cells[i, b].Value2.ToString();
                        lbStatus.Items.Add("Butikknr: " + StoreNo);
                        lblStatus.Text = "Butikk " + (b - 1) + " av " + numberOfStores;
                        lbNumber.Text = (b - 1) + " / " + numberOfStores;
                        pgImport.Value = (b-1);
                    }
                    else
                    {
                        if (Double.TryParse(xlRange.Cells[i, b].Text.ToString().Trim(), out dymmy))
                        {
                            //(xlRange.Cells[i, b].Text) gir formattert verdi
                            //(xlRange.Cells[i, b].Value2 gir orginal verdi
                            if(!useFormattedValue)
                                Insertdata(StoreNo.Trim(), timeStamp.AddDays(i), Convert.ToDouble(xlRange.Cells[i, b].Value2.ToString().Trim()));
                            else
                                Insertdata(StoreNo.Trim(), timeStamp.AddDays(i), Convert.ToDouble(xlRange.Cells[i, b].Text.ToString().Trim()));
                        }
                        else
                        {
                            Insertdata(StoreNo.Trim(), timeStamp.AddDays(i), 0);
                        }
                        //MessageBox.Show(StoreNo + " -> " + timeStamp.AddDays(i) + " -> " + xlRange.Cells[i, b].Value2.ToString());
                    }

#warning HUSK Å ENDRE ÅRET
                    if (timeStamp.AddDays(i).Equals(new DateTime(2018, 12, 31)))
                    {
                        break;
                    }

                    Application.DoEvents();
                }
            }
            closeConn();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            lbStatus.Items.Clear();

            budgetType = checkedListBox1.SelectedItem.ToString();

            try
            {
                loopData(false);

                MessageBox.Show("Import ferdig!");
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }

        private void btnChooseFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtFileName.Text = openFileDialog1.FileName;
            }
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
