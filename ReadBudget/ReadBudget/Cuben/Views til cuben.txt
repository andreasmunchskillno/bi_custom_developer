﻿select a.BudgetDate, a.DayBudget As Budget, a.FK_StoreId ,b.CustomerBudget, c.ItemCost 
from DWH.dbo.FactSalesBudget_ as a
left outer join DWH.dbo.FactBudgetCustomerStore as b
on a.BudgetDate = b.BudgetDate
and a.StoreId = b.StoreId
left outer join FactBudgetItemCostStore as c
on a.BudgetDate = c.BudgetDate
and a.StoreId = c.StoreId