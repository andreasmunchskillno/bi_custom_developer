﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.AnalysisServices;
using System.Net.Mail;
using System.Net;
using Microsoft.AnalysisServices.AdomdClient;

namespace SSAS_LoopObjects
{
    public partial class Form1 : Form
    {
        bool dummyBoolean = false;
        string connectionString;
        string databaseName = "StoreSales";
        string cubeName = "StoreSales";                                 //Navnet p� cuben
        Server server;

        private void connect()
        {
            connectionString = "Data Source=epsqlbitest";
            server = new Server();
            server.Connect(connectionString);
        }

        private void close()
        {
            server.Disconnect();
        }

        /// <summary>
        /// Faktaer
        /// </summary>
        /// 
        public void processLastPartitions()
        {
            Database database = server.Databases.FindByName(databaseName);
            Cube storeSales = database.Cubes.FindByName(cubeName);
    
            foreach (MeasureGroup g in storeSales.MeasureGroups)
            {
                listView1.Items.Add(g.Name);
            }
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            connect();
            processLastPartitions();
            close();
        }
    }
}
