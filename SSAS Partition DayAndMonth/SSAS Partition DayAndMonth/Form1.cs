﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.AnalysisServices;
using System.Collections;
using System.Net.Mail;
using System.Net;
using Microsoft.AnalysisServices.AdomdClient;

namespace SSAS_Partition_DayAndMonth
{
    public partial class Form1 : Form
    {
        bool dummyBoolean = false;
        string connectionString;
        string databaseName;
        string sekkePostPartition;
        string checkStartDate = "2015.01.01";                                       //Hvor mange måneder den skal sjekke bakover.
        int numberOfPartitionsToProcess = -1;                                       //Hvor mange partisjoner som skal prosesseres
        string cubeName = "Purchase";                                               //Navnet på cuben
        string measureGroup = "Varer i beholdning";                                 //Navnet på partisjonen
        string defaultMeasureGroup = "Fact Store Inventory Empty Stock";
        string partitionToCopyAggregationFrom = "Fact Store Inventory Empty Stock Sekkepost"; //Navnet på den partisjonen vi skal kopiere aggregationID fra
        ArrayList processedPartitions = new ArrayList();

        /// <summary>
        /// Forsøker å prosessere partisjonen med ignore error
        /// </summary>
        /// <param name="partitionName"></param>
        public void executeXmla(string partitionName)
        {
            try
            {
                string XMLA = @"<Batch xmlns=""http://schemas.microsoft.com/analysisservices/2003/engine"">
              <ErrorConfiguration xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:ddl2=""http://schemas.microsoft.com/analysisservices/2003/engine/2"" xmlns:ddl2_2=""http://schemas.microsoft.com/analysisservices/2003/engine/2/2"" xmlns:ddl100_100=""http://schemas.microsoft.com/analysisservices/2008/engine/100/100"" xmlns:ddl200=""http://schemas.microsoft.com/analysisservices/2010/engine/200"" xmlns:ddl200_200=""http://schemas.microsoft.com/analysisservices/2010/engine/200/200"">
                <KeyErrorLimit>-1</KeyErrorLimit>
                <KeyErrorLogFile>c:\" + measureGroup + @".log</KeyErrorLogFile>
                <KeyNotFound>IgnoreError</KeyNotFound>
                <NullKeyNotAllowed>IgnoreError</NullKeyNotAllowed>
              </ErrorConfiguration>
              <Parallel>
                <Process xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:ddl2=""http://schemas.microsoft.com/analysisservices/2003/engine/2"" xmlns:ddl2_2=""http://schemas.microsoft.com/analysisservices/2003/engine/2/2"" xmlns:ddl100_100=""http://schemas.microsoft.com/analysisservices/2008/engine/100/100"" xmlns:ddl200=""http://schemas.microsoft.com/analysisservices/2010/engine/200"" xmlns:ddl200_200=""http://schemas.microsoft.com/analysisservices/2010/engine/200/200"">
                  <Object>
                    <DatabaseID>" + databaseName + @"</DatabaseID>
                    <CubeID>DWH</CubeID>
                    <MeasureGroupID>" + measureGroup + @"</MeasureGroupID>
                    <PartitionID>" + partitionName + @"</PartitionID>
                  </Object>
                  <Type>ProcessFull</Type>
                  <WriteBackTableCreation>UseExisting</WriteBackTableCreation>
                </Process>
              </Parallel>
            </Batch>";

                //Open a connection to the local server
                AdomdConnection conn = new AdomdConnection(connectionString);
                conn.Open();

                //Create a command, and assign it an XMLA command to process the cube.
                AdomdCommand cmd = conn.CreateCommand();
                cmd.CommandText = XMLA;

                //Execute the command
                int result = cmd.ExecuteNonQuery();

                //Close the connection
                conn.Close();
            }
            catch (Exception err)
            {
                //Dts.Events.FireInformation(1, "Prosesserer fakta", "Error XMLA " + err.Message + "...", "", 0, ref dummyBoolean);
                MessageBox.Show(err.Message);
            }
        }

        public static void sendMail(string error, string partisjon)
        {
            MailMessage myHtmlMessage = new MailMessage();
            SmtpClient mySmtpClient = new SmtpClient();

            myHtmlMessage = new MailMessage(@"sindre.mathisen@europris.no",
                                 "sindre.mathisen@europris.no", @"Feil under prosessering", @"Feil under prosessering av StoreSales salgsfakta (partisjon " + partisjon + @"). 
                                    Prøver ignore error. <br><br>" + error);
            mySmtpClient = new SmtpClient("192.168.5.7");
            myHtmlMessage.IsBodyHtml = true;
            mySmtpClient.Credentials = CredentialCache.DefaultNetworkCredentials;
            mySmtpClient.Send(myHtmlMessage);
        }

        public static string getSql(string WhereStatement)
        {
            return @"select * from [dbo].[v_FactStoreInventoryEmptyStock] " + WhereStatement;
        }
        
        public void changeSekkepost()
        {
            Server server = new Server();
            server.Connect(connectionString);

            Database database = server.Databases.FindByName(databaseName);

            Cube storeSales = database.Cubes.FindByName(cubeName);

            MeasureGroup ItemSales = storeSales.MeasureGroups.FindByName(measureGroup);

            //if (!ItemSales.Partitions.Contains(sekkePostPartition))
            {
                Partition oldPartition = ItemSales.Partitions.GetByName(sekkePostPartition);

                //oldPartition.AggregationDesignID = server.Databases.FindByName(Dts.Variables["Purchase_DataBaseID"].Value.ToString()).Cubes.FindByName(cubeName).MeasureGroups.FindByName(measureGroup).Partitions.FindByName(partitionToCopyAggregationFrom).AggregationDesignID;
                oldPartition.AggregationDesignID = server.Databases.FindByName("Purchase").Cubes.FindByName(cubeName).MeasureGroups.FindByName(measureGroup).Partitions.FindByName(partitionToCopyAggregationFrom).AggregationDesignID;

                oldPartition.EstimatedRows = 1000000;
                oldPartition.StorageMode = StorageMode.Molap;

                oldPartition.Source = new QueryBinding(
                            "DM Purchase", // the ID of the DataSource for the partition
                                @getSql(" WHERE FK_Date2 > '" + DateTime.Now.ToString("yyyy-MM-dd") + "'"));
                oldPartition.Update();

                //Dts.Events.FireInformation(1, "Dynamic partitions", "Endre partisjon for sekkepost. Ny fradato er : " + getCurrentToDate(), "", 0, ref dummyBoolean);

                //Dts.Events.FireInformation(1, "Dynamic partitions", "PROSESSERER sekkepost...", "", 0, ref dummyBoolean);

                try
                {
                    //Forsøk å prosessere partisjonen vanlig.
                    oldPartition.Process(ProcessType.ProcessData);
                }
                catch (Exception err)
                {
                    executeXmla(sekkePostPartition);
                    sendMail(err.ToString(), sekkePostPartition);
                }

                processedPartitions.Add(sekkePostPartition);
            }

            server.Disconnect();
        }
       
        /// <summary>
        /// Prosesserer siste 2 mnd
        /// </summary>
        /// 
        public void processLastNPartitions()
        {
            Server server = new Server();
            server.Connect(connectionString);
            server.CaptureXml = true;

            DateTime startDate = DateTime.Now.AddDays(numberOfPartitionsToProcess);

            string partitionName = "";

            while (startDate <= DateTime.Now)
            {
                partitionName = defaultMeasureGroup + " " + startDate.ToString("yyyy-MM-dd");

                Database database = server.Databases.FindByName(databaseName);

                Cube storeSales = database.Cubes.FindByName(cubeName);

                MeasureGroup ItemSales = storeSales.MeasureGroups.FindByName(measureGroup);

                if (ItemSales.Partitions.Contains(partitionName))
                {
                    //Sjekker opp partisjonen har blitt prosessert tidligere
                    if (processedPartitions.BinarySearch(partitionName) < 0)
                    {
                        Partition oldPartition = ItemSales.Partitions.GetByName(partitionName);

                        try
                        {
                            //Forsøk å prosessere partisjonen vanlig.
                            oldPartition.Process(ProcessType.ProcessData);

                            listBox1.Items.Add("Process " + oldPartition.Name);
                            Application.DoEvents();
                        }
                        catch (Exception err)
                        {
                            executeXmla(partitionName);
                            sendMail(err.ToString(), partitionName);
                        }

                        //Dts.Events.FireInformation(1, "Dynamic partitions", "PROSESSERER 2 siste mnd: " + partitionName + "...", "", 0, ref dummyBoolean);
                    }
                    else
                    {
                        //Dts.Events.FireInformation(1, "Dynamic partitions", "SKIPPET PROSESSERER 2 siste mnd (har vært prosessert tidligere): " + partitionName + "...", "", 0, ref dummyBoolean);
                    }
                }
                else
                {
                    //Dts.Events.FireInformation(1, "Dynamic partitions", "FEIL: Denne burde ligge her..." + partitionName, "", 0, ref dummyBoolean);
                }

                startDate = startDate.AddDays(1);

                server.CaptureXml = false;
            }

            XmlaResultCollection res = server.ExecuteCaptureLog(true, true);
            printXmla(res);
            server.Disconnect();
        }

        private void printXmla(XmlaResultCollection results)
        {
            string allErrors = "";

            foreach (XmlaResult result in results)
            {
                foreach (XmlaMessage message in result.Messages)
                {
                    if (message is XmlaError)
                    {
                        allErrors = allErrors + message.Description + " " + message.Source + " " + " " + message.Location + "<br>";
                        //Dts.Events.FireError(0, "Feil under prosessering av fakta. Feil:  " + message.Description + " " + message.Source, message.Location + "Feil under prosessering av fakta. Feil: " + message.Description + " " + message.Source, String.Empty, 0);
                    }
                }
            }

            //Dts.Variables["ErrMsg"].Value = "<br>Feil under vanlig prosessering av cuben. Vennligst gjør en manuell sjekk av cuben på epsqlbiprod før den eventuelt flyttes til epsql7. Sjekk errorlog på c:\\error_processing_purchase.log <br><br>" + allErrors;
        }

        /// <summary>
        /// Oppretter partisjoner for en tidsintervall.
        /// </summary>
        /// 
        public void createPartitionsForTimeRange()
        {
            DateTime startDate = Convert.ToDateTime(checkStartDate);

            string whereStatement = "";
            string partitionName = "";

            while (startDate <= DateTime.Now)
            {
                partitionName = defaultMeasureGroup + " " + startDate.ToString("yyyy-MM-dd");
                whereStatement = " WHERE FK_Date2 = '" + startDate.ToString("yyyy-MM-dd") + "'"; //'2015/03/28'

                createPartition(defaultMeasureGroup + " " + startDate.ToString("yyyy-MM-dd"), whereStatement);

                listBox1.Items.Add(whereStatement + " - " + partitionName);
                label1.Text = listBox1.Items.Count.ToString();
                Application.DoEvents();

                startDate = startDate.AddDays(1);

                //Dts.Events.FireInformation(1, "Dynamic partitions", "Sjekker etter hull i partisjoneringen. Sjekk fradato : " + whereStatement, "", 0, ref dummyBoolean);
            }
        }
        /*
        /// <summary>
        /// Denne funksjonen lager partisjonsnavn for måneden man er i
        /// </summary>
        public void createPartitionForCurrentMonth()
        {
            DateTime current = DateTime.Now;

            string sMonth = current.Month < 10 ? "0" + current.Month.ToString() : current.Month.ToString();
            int year = current.Year;

            string whereStatement = " WHERE FK_Date2 >= " + getCurrentFromDate() + " AND FK_Date2 < " + getCurrentToDate();

            createPartition(defaultMeasureGroup + " " + year + " - " + sMonth, whereStatement);
        }
        */
        /// <summary>
        /// Denne funksjonen oppretter selve partisjonen
        /// </summary>
        /// <param name="PartitionName"></param>
        /// <param name="whereStatement"></param>
        /// <returns></returns>
        public bool createPartition(string PartitionName, string whereStatement)
        {
            /*
             * Kjent feil. Man m� legge til referanse til Analysis Managment Objects hver gang man editerer koden.
             * 
            */

            Server server = new Server();
            server.Connect(connectionString);

            Database database = server.Databases.FindByName(databaseName);

            Cube storeSales = database.Cubes.FindByName(cubeName);

            MeasureGroup ItemSales = storeSales.MeasureGroups.FindByName(measureGroup);

            if (!ItemSales.Partitions.Contains(PartitionName))
            {
                Partition newPartition = ItemSales.Partitions.Add(PartitionName);

                //newPartition.AggregationDesignID = server.Databases.FindByName(Dts.Variables["Purchase_DataBaseID"].Value.ToString()).Cubes.FindByName(cubeName).MeasureGroups.FindByName(measureGroup).Partitions.FindByName(partitionToCopyAggregationFrom).AggregationDesignID;
                newPartition.AggregationDesignID = server.Databases.FindByName("Purchase").Cubes.FindByName(cubeName).MeasureGroups.FindByName(measureGroup).Partitions.FindByName(partitionToCopyAggregationFrom).AggregationDesignID;

                newPartition.EstimatedRows = 1000000;
                newPartition.StorageMode = StorageMode.Molap;

                newPartition.Source = new QueryBinding(
                            "DM Purchase", // the ID of the DataSource for the partition
                                @getSql(whereStatement)
                            );

                newPartition.Update();

                try
                {
                    //Forsøk å prosessere partisjonen vanlig.
                    newPartition.Process(ProcessType.ProcessData);
                }
                catch (Exception err)
                {
                    executeXmla(PartitionName);
                    sendMail(err.ToString(), PartitionName);
                }

                //Lagrer partisjonen som vi har prosessert
                processedPartitions.Add(PartitionName);

                //Dts.Events.FireInformation(1, "Dynamic partitions", "OPPRETTET. La til ny partisjon med navn: " + PartitionName, "", 0, ref dummyBoolean);

                //Dts.Events.FireInformation(1, "Dynamic partitions", "PROSESSERER " + PartitionName + "...", "", 0, ref dummyBoolean);
            }
            else
            {
                Partition newPartition = ItemSales.Partitions.GetByName(PartitionName);

                if (newPartition.State == AnalysisState.Unprocessed)
                {
                    //Lagrer partisjonen som vi har prosessert
                    processedPartitions.Add(PartitionName);

                    newPartition.Process(ProcessType.ProcessData);
                    //Dts.Events.FireInformation(1, "Dynamic partitions", "IGNORERTE (MEN PROSESSERTE). Lagde ikke partisjonen siden den fantes fra før", "", 0, ref dummyBoolean);
                }
                else
                {
                    //Dts.Events.FireInformation(1, "Dynamic partitions", "IGNORERTE. Lagde ikke partisjonen siden den fantes fra før", "", 0, ref dummyBoolean);
                }
            }

            server.Disconnect();

            return true;
        }

        /// <summary>
        /// This method is called when this script task executes in the control flow.
        /// Before returning from this method, set the value of Dts.TaskResult to indicate success or failure.
        /// To open Help, press F1.
        /// </summary>
        //public void Main()
        //{
        //    try
        //    {
        //        connectionString = "Data Source=" + Dts.Variables["Purchase_Server"].Value.ToString();
        //        databaseName = Dts.Variables["Purchase_Database"].Value.ToString();
        //        sekkePostPartition = defaultMeasureGroup + " Sekkepost";
        //        numberOfPartitionsToProcess = (Convert.ToInt32(Dts.Variables["NumberOfMonthsToProcess"].Value.ToString()) * -1) + 1;

        //        //Sjekker partisjoner og prosesserer. Prosesserer også sekkepost
        //        createPartitionsForTimeRange();

        //        //Prosesserer partisjoner for de siste 3 måneder.
        //        processLastNPartitions();

        //        //Prosesserer sekkepost
        //        changeSekkepost();

        //        Dts.TaskResult = (int)ScriptResults.Success;
        //    }
        //    catch (Exception err)
        //    {
        //        //En feil oppstod
        //        Dts.Events.FireError(-1, "Error", err.Message + "\r" + err.StackTrace, String.Empty, 0);

        //        Dts.TaskResult = (int)ScriptResults.Failure;
        //    }
        //}

       
        public void Main()
        {
            try
            {
                connectionString = "Data Source=epsqlbitest\\build";
                databaseName = "Purchase";
                sekkePostPartition = defaultMeasureGroup + " Sekkepost";
                numberOfPartitionsToProcess = -2;

                //Sjekker partisjoner og prosesserer. Prosesserer også sekkepost
                //createPartitionsForTimeRange();

                //Prosesserer partisjoner for de siste 3 måneder.
                //processLastNPartitions();

                //Prosesserer sekkepost
                changeSekkepost();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Main();
            MessageBox.Show("Ferdig!");
        }
    }
}
