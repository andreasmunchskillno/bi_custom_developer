﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.AnalysisServices.AdomdClient;
using Microsoft.AnalysisServices;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Threading;

namespace SSAS_Performance_Test
{
    public partial class Form1 : Form
    {
        public delegate void loopDelegate();

        public Form1()
        {
            InitializeComponent();
        }

        private void runXMLA(string MDX, string database)
        {
            try
            {
                //Open a connection to the local server
                //AdomdConnection conn = new AdomdConnection("Data Source=epsqlbitest; Catalog=" + database);
                AdomdConnection conn = new AdomdConnection("Data Source=epsqlbiprod2; Catalog=" + database);
                conn.Open();

                //Create a command, and assign it an XMLA command to process the cube.
                AdomdCommand cmd = conn.CreateCommand();
                cmd.CommandText = MDX;

                //Execute the command
                cmd.Execute();

                //Close the connection
                conn.Close();
            }
            catch 
            {
                throw new Exception("Inneholder parameter");
            }
        }

        private void getMDXQueries()
        {
            Stopwatch stopwatch = new Stopwatch();

            // Begin timing
            stopwatch.Start();

            SqlConnection sqlConnection1 = new SqlConnection("Server=epsqlbitest;Database=Sindre;Trusted_Connection=True;");
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;
            int iCounter = 0;

            //            cmd.CommandText = @"SELECT top 10 [RowNumber]
            //                                  ,[TextData]
            //                                  ,[DatabaseName]
            //                              FROM [Sindre].[dbo].[OlapQueriesFromEPSQL7]
            //                              where [DatabaseName] is not null
            //                              order by RowNumber ";

            //            cmd.CommandText = @"SELECT [RowNumber]
            //                                  ,[TextData]
            //                                  ,[DatabaseName]
            //                              FROM [Sindre].[dbo].[Queries epsql7 monday]
            //                              where [DatabaseName] is not null
            //                              order by RowNumber ";

            cmd.CommandText = @"SELECT TOP 500 [RowNumber]
    ,[TextData]
    ,[DatabaseName]
FROM [Sindre].[dbo].[Aktivitet epsql7 17042015]
where [DatabaseName] = 'StoreSales'
order by RowNumber
 ";


            cmd.CommandType = CommandType.Text;
            cmd.Connection = sqlConnection1;

            sqlConnection1.Open();

            reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    if (reader.GetString(1) != null)
                    {
                        try
                        {
                            runXMLA(reader.GetString(1), reader.GetString(2));
                            listBox1.Items.Add("Spørring nr: " + iCounter);
                        }
                        catch (Exception err)
                        {
                            listBox1.Items.Add("Spørring nr: " + iCounter + ". Feil: " + err.Message);
                        }

                        label2.Text = stopwatch.Elapsed.ToString();
                        label1.Text = iCounter.ToString();

                        Application.DoEvents();

                        iCounter++;
                    }
                }
            }
            else
            {
                Console.WriteLine("No rows found.");
            }

            sqlConnection1.Close();

            stopwatch.Stop();
        }

        private void getThreadMDXQueries()
        {
            SqlConnection sqlConnection1 = new SqlConnection("Server=epsqlbitest;Database=Sindre;Trusted_Connection=True;");
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;
            int iCounter = 0;

            cmd.CommandText = @"SELECT top 400 [RowNumber]
                                  ,[TextData]
                                  ,[DatabaseName]
                              FROM [Sindre].[dbo].[OlapQueriesFromEPSQL7]
                              where [DatabaseName] is not null
                              order by RowNumber ";

            cmd.CommandType = CommandType.Text;
            cmd.Connection = sqlConnection1;

            sqlConnection1.Open();

            reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    if (reader.GetString(1) != null)
                    {
                        try
                        {
                            runXMLA(reader.GetString(1), reader.GetString(2));
                            //listBox1.Items.Add("Spørring nr: " + iCounter);
                        }
                        catch (Exception err)
                        {
                            //listBox1.Items.Add("Spørring nr: " + iCounter + ". Feil: " + err.Message);
                        }

                        iCounter++;
                    }
                }
            }
            else
            {
                Console.WriteLine("No rows found.");
            }

            MessageBox.Show("Ferdig. " + Thread.CurrentThread.Name);

            sqlConnection1.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            listBox1.Items.Add("Starter");

            getMDXQueries();

            listBox1.Items.Add("Ferdig");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Stopwatch stopwatch = new Stopwatch();
            listBox1.Items.Clear();
            listBox1.Items.Add("Starter");

            // Begin timing
            stopwatch.Start();

            Thread[] theThreads = new Thread[5];

            for (int counter = 0; counter < 5; counter++)
            {
                listBox1.Items.Add("Thread " + counter);

                ThreadStart st = new ThreadStart(getThreadMDXQueries);
                theThreads[counter] = new Thread(st);
                theThreads[counter].Name = "Thread " + counter;
                theThreads[counter].Start();
            }

            for (int counter = 0; counter < 5; counter++)
            {
                theThreads[counter].Join();
            }

            stopwatch.Stop();
            label2.Text = stopwatch.Elapsed.ToString();

            listBox1.Items.Add("Ferdig");
        }
    }
}
